<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partner extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'product_id', 'name', 'image', 'description'
    ];

    protected $dates = ['deleted_at'];

    public function product() {
        $p_ids = explode(';', $this->product_id);
        return Product::find($p_ids);
    }

    public function product2()
    {
        return $this->belongsTo('App\Product');
    }
}
