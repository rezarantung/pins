<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'location'
    ];

    protected $dates = ['deleted_at'];

    public function scopeFeatured($query) {
        return $query->where('featured', 'true');
    }
}
