<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enquiry extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'name', 'email', 'phone', 'message'
    ];

    protected $dates = ['deleted_at'];
    protected $table = 'enquiries';
}
