<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'parent_id', 'name_id', 'name_en', 'ref_id', 'type', 'link', 'featured', 'order', 'icon'
    ];

    protected $dates = ['deleted_at'];

    public function child() {
        return $this->hasMany('App\Menu', 'parent_id');
    }

    public function product() {
        return $this->belongsTo('App\Product', 'ref_id');
    }

    public function parent() {
        return $this->belongsTo('App\Menu');
    }

    public function scopeFeatured($query) {
        return $query
            ->where('type', Helpers::MENU_TYPE_PRODUCT)
            ->whereNotNull('parent_id')
            ->join('products', function($join) {
                $join->on('menus.ref_id', '=', 'products.id')
                     ->where('featured', 'true');
            })->orderBy('order', 'asc')
            ->select('menus.*');
    }

    public function scopeFeaturedSaved($query) {
        return $query
            ->where('type', Helpers::MENU_TYPE_PRODUCT)
            ->whereNotNull('parent_id')
            ->where('parent_id', 2)
            ->orderBy('order', 'asc');
    }

    public function scopeFooter($query) {
        return $query
            ->where('type', Helpers::MENU_TYPE_ABOUT)
            ->whereNull('parent_id')
            ->orderBy('order', 'asc');
    }

    public static function getFooter() {
        $out = [];

        foreach (Menu::featuredSaved()->get() as $value) {
            array_push($out, $value);
        }

        foreach (Menu::footer()->get() as $value) {
            array_push($out, $value);
        }

        return $out;
    }
}
