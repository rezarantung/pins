<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Emailtemplate extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'type', 'name', 'subject', 'content'
    ];

    protected $dates = ['deleted_at'];
    protected $table = 'email_templates';

    public function scopeContactUsAdmin($query) {
        return $query
            ->where('type', '1')
            ->where('name', 'like', '%admin%');
    }

    public function scopeContactUsUser($query) {
        return $query
            ->where('type', '1')
            ->where('name', 'like', '%user%');
    }

    public function scopeCommentAdmin($query) {
        return $query
            ->where('type', '2')
            ->where('name', 'like', '%admin%');
    }

    public function scopeCommentUser($query) {
        return $query
            ->where('type', '2')
            ->where('name', 'like', '%user%');
    }
}
