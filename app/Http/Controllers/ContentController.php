<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Content;

class ContentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
	}
	
    public function index()
    {
        return view('pages/content');
    }

    public function list(Request $request) {
		if( $request->draw != FALSE )   {$draw   = $request->draw;}   else{$draw   = 1;}; 
		if( $request->length != FALSE ) {$length = $request->length;} else{$length = 10;}; 
		if( $request->start != FALSE )  {$start  = $request->start;}  else{$start  = 0;}; 		
		
		$order = $request->order;
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 
		$order_fields = array('name', 'created_at', '');
		
		$search = $request->search;
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		$limit 			= (int) $length;
		$offset			= (int) $start;
		$order_column 	= $order_fields[$order_column];
		$order_dir		= $order_dir;
		$filter 		= $search_value;

    	$list_total = Content::where('name', 'LIKE', '%'.$filter.'%')->get();
    	$list_filtered = Content::where('name', 'LIKE', '%'.$filter.'%')
    							->take($limit)
    							->offset($offset)
    							->orderBy($order_column, $order_dir)
    							->get();

    	$result["recordsTotal"] = $list_total->count();
		$result["recordsFiltered"] = $list_total->count();
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list_filtered as $value ) {
            $disabled = $value['type'] == 7 ? 'disabled' : '';
            
            $action = '
                        <center>
                            <button class="btn btn-sm btn-primary" onclick="_detail(\''.$value['id'].'\');" title="Detail"><i class="fa fa-eye"></i></button>
                            <button class="btn btn-sm btn-warning" onclick="_edit(\''.$value['id'].'\');" title="Edit"><i class="fa fa-edit"></i></button>
                            <button class="btn btn-sm btn-danger" onclick="_delete(\''.$value['id'].'\');" title="Delete" ' . $disabled . '><i class="fa fa-trash"></i></button>
                        </center>
                    ';


		    array_push($data, 
				array(
                    $value['name'],
                    $value['created_at']->format('d M Y'),
					$action
				)
			);
		}
		
		$result["data"] = $data;

		return response()->json($result);
    }
    
    public function store(Request $request) {
    	//rules
        $rules=array(
        	'name' => 'required',
            'layout' => 'required',
            'type' => 'required'
        );
          
        //message error 
        $messages=array(
        	// 'product.required' => 'Product required.',
        );

        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
            $exist = Content::where('name', $request->name)->first();

            if($exist) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'The name has already exist.'
                ]);
            } else {
                $table = new Content;

                $table->name = $request->name;
                $table->layout = $request->layout;
                $table->type = $request->type;
                $table->text_1_id = $request->text_1_id;
                $table->text_2_id = $request->text_2_id;
                $table->text_3_id = $request->text_3_id;
                $table->text_1_en = $request->text_1_en;
                $table->text_2_en = $request->text_2_en;
                $table->text_3_en = $request->text_3_en;
                $table->video_1 = $request->video_1;

                //set image unique name
                if($request->hasFile('image_1')) {
                    $image = $request->image_1;
                    $fileNameWithExt = $image->getClientOriginalName();
                    $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extension = $image->getClientOriginalExtension();
                    $filenameToStore = $filename.'_'.time().'.'.$extension;
                    $path = $image->storeAs('public/content', $filenameToStore);
                    $table->image_1 = $filenameToStore;
                }

                if($request->hasFile('image_2')){
                    $image = $request->image_2;
                    $fileNameWithExt = $image->getClientOriginalName();
                    $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extension = $image->getClientOriginalExtension();
                    $filenameToStore = $filename.'_'.time().'.'.$extension;
                    $path = $image->storeAs('public/content', $filenameToStore);
                    $table->image_2 = $filenameToStore;
                }

                if($request->hasFile('image_3')){
                    $image = $request->image_3;
                    $fileNameWithExt = $image->getClientOriginalName();
                    $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extension = $image->getClientOriginalExtension();
                    $filenameToStore = $filename.'_'.time().'.'.$extension;
                    $path = $image->storeAs('public/content', $filenameToStore);
                    $table->image_3 = $filenameToStore;
                }
               
                if($table->save()) {
                    return response()->json([
                        'status' => 'success',
                        'message' => 'Form Submited.'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Submit Failed.'
                    ]);
                }  
            }
        }
    }

	public function destroy(Request $request) {
    	$deleted = Content::where('id', $request->id)->delete();

        if($deleted) {
        	return response()->json([
	            'status' => 'success',
	            'message' => 'Record Deleted.'
	        ]);
        } else {
        	return response()->json([
                'status' => 'error',
                'message' => 'Delete Failed.'
            ]);
        }
    }

    public function detail(Request $request) {
		$record = Content::where('id', $request->id)->first();
    	return response()->json($record);
    }
	
	public function update(Request $request) {
    	$updated = 0;

    	//rules
        $rules=array(
        	'id' => 'required',
            'name' => 'required',
            'layout' => 'required',
            'type' => 'required'
        );
          
        //title error 
        $titles=array(
        	//'id.required' => 'ID required.'
        );

        $validator = Validator::make($request->all(),$rules,$titles);
        if($validator->fails()) {
            $titles=$validator->messages();
            $errors=$titles->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
			//check name existing
	    	$exist = Content::where('name', $request->name)->first();

	    	if($exist) {
	    		if($exist['name'] == $request->name_old) {
                    $filenameToStore1 = $request->image_1_old;
                    $filenameToStore2 = $request->image_2_old;
                    $filenameToStore3 = $request->image_3_old;

                    //set image unique name
                    if($request->hasFile('image_1')) {
                        // delete old file
                        $image = Content::select ('image_1')->where('id', '=', $request->id)->first();
                        if($image) {
                            $file_name = $image['image_1'];

                            if(Storage::exists('public/content/'.$file_name)) {
                                $delete_file = Storage::delete('public/content/'.$file_name);
                            }
                        }

                        //save new file
                        $image = $request->image_1;
                        $fileNameWithExt = $image->getClientOriginalName();
                        $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                        $extension = $image->getClientOriginalExtension();
                        $filenameToStore1 = $filename.'_'.time().'.'.$extension;
                        $path = $image->storeAs('public/content', $filenameToStore1);
                    }

                    if($request->hasFile('image_2')){
                        // delete old file
                        $image = Content::select ('image_2')->where('id', '=', $request->id)->first();
                        if($image) {
                            $file_name = $image['image_2'];

                            if(Storage::exists('public/content/'.$file_name)) {
                                $delete_file = Storage::delete('public/content/'.$file_name);
                            }
                        }

                        //save new file
                        $image = $request->image_2;
                        $fileNameWithExt = $image->getClientOriginalName();
                        $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                        $extension = $image->getClientOriginalExtension();
                        $filenameToStore2 = $filename.'_'.time().'.'.$extension;
                        $path = $image->storeAs('public/content', $filenameToStore2);
                    }

                    if($request->hasFile('image_3')){
                        // delete old file
                        $image = Content::select ('image_3')->where('id', '=', $request->id)->first();
                        if($image) {
                            $file_name = $image['image_3'];

                            if(Storage::exists('public/content/'.$file_name)) {
                                $delete_file = Storage::delete('public/content/'.$file_name);
                            }
                        }

                        //save new file
                        $image = $request->image_3;
                        $fileNameWithExt = $image->getClientOriginalName();
                        $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                        $extension = $image->getClientOriginalExtension();
                        $filenameToStore3 = $filename.'_'.time().'.'.$extension;
                        $path = $image->storeAs('public/content', $filenameToStore3);
                    }


                    //for default content
                    if($request->type_old == 7) {
                        $updated = Content::where('id', $request->id)
                                      ->update([
                                            'name' => $request->name,
                                            'layout' => $request->layout,
                                            'text_1_id' => $request->text_1_id,
                                            'text_2_id' => $request->text_2_id,
                                            'text_3_id' => $request->text_3_id,
                                            'text_1_en' => $request->text_1_en,
                                            'text_2_en' => $request->text_2_en,
                                            'text_3_en' => $request->text_3_en,
                                            'image_1' => $filenameToStore1,
                                            'image_2' => $filenameToStore2,
                                            'image_3' => $filenameToStore3,
                                            'video_1' => $request->video_1
                                        ]);
                    } else {
    	    			$updated = Content::where('id', $request->id)
    		                              ->update([
                                                'name' => $request->name,
                                                'layout' => $request->layout,
                                                'type' => $request->type,
                                                'text_1_id' => $request->text_1_id,
                                                'text_2_id' => $request->text_2_id,
                                                'text_3_id' => $request->text_3_id,
                                                'text_1_en' => $request->text_1_en,
                                                'text_2_en' => $request->text_2_en,
                                                'text_3_en' => $request->text_3_en,
                                                'image_1' => $filenameToStore1,
                                                'image_2' => $filenameToStore2,
                                                'image_3' => $filenameToStore3,
                                                'video_1' => $request->video_1
    		                                ]);
                    }
	    		} else {
	    			return response()->json([
		                'status' => 'error',
		                'message' => 'The name has already exist.'
		            ]);
	    		}
	    	} else {
                $filenameToStore1 = $request->image_1_old;
                $filenameToStore2 = $request->image_2_old;
                $filenameToStore3 = $request->image_3_old;

                //set image unique name
                if($request->hasFile('image_1')) {
                    // delete old file
                    $image = Content::select ('image_1')->where('id', '=', $request->id)->first();
                    if($image) {
                        $file_name = $image['image_1'];

                        if(Storage::exists('public/content/'.$file_name)) {
                            $delete_file = Storage::delete('public/content/'.$file_name);
                        }
                    }

                    //save new file
                    $image = $request->image_1;
                    $fileNameWithExt = $image->getClientOriginalName();
                    $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extension = $image->getClientOriginalExtension();
                    $filenameToStore1 = $filename.'_'.time().'.'.$extension;
                    $path = $image->storeAs('public/content', $filenameToStore1);
                }

                if($request->hasFile('image_2')){
                    // delete old file
                    $image = Content::select ('image_2')->where('id', '=', $request->id)->first();
                    if($image) {
                        $file_name = $image['image_2'];

                        if(Storage::exists('public/content/'.$file_name)) {
                            $delete_file = Storage::delete('public/content/'.$file_name);
                        }
                    }

                    //save new file
                    $image = $request->image_2;
                    $fileNameWithExt = $image->getClientOriginalName();
                    $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extension = $image->getClientOriginalExtension();
                    $filenameToStore2 = $filename.'_'.time().'.'.$extension;
                    $path = $image->storeAs('public/content', $filenameToStore2);
                }

                if($request->hasFile('image_3')){
                    // delete old file
                    $image = Content::select ('image_3')->where('id', '=', $request->id)->first();
                    if($image) {
                        $file_name = $image['image_3'];

                        if(Storage::exists('public/content/'.$file_name)) {
                            $delete_file = Storage::delete('public/content/'.$file_name);
                        }
                    }

                    //save new file
                    $image = $request->image_3;
                    $fileNameWithExt = $image->getClientOriginalName();
                    $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extension = $image->getClientOriginalExtension();
                    $filenameToStore3 = $filename.'_'.time().'.'.$extension;
                    $path = $image->storeAs('public/content', $filenameToStore3);
                }

                //for default content
                if($request->type_old == 7) {
                    $updated = Content::where('id', $request->id)
                                    ->update([
                                            'name' => $request->name,
                                            'layout' => $request->layout,
                                            'text_1_id' => $request->text_1_id,
                                            'text_2_id' => $request->text_2_id,
                                            'text_3_id' => $request->text_3_id,
                                            'text_1_en' => $request->text_1_en,
                                            'text_2_en' => $request->text_2_en,
                                            'text_3_en' => $request->text_3_en,
                                            'image_1' => $filenameToStore1,
                                            'image_2' => $filenameToStore2,
                                            'image_3' => $filenameToStore3,
                                            'video_1' => $request->video_1
                                        ]);
                } else {
    	    		$updated = Content::where('id', $request->id)
    		                            ->update([
                                                'name' => $request->name,
                                                'layout' => $request->layout,
                                                'type' => $request->type,
                                                'text_1_id' => $request->text_1_id,
                                                'text_2_id' => $request->text_2_id,
                                                'text_3_id' => $request->text_3_id,
                                                'text_1_en' => $request->text_1_en,
                                                'text_2_en' => $request->text_2_en,
                                                'text_3_en' => $request->text_3_en,
                                                'image_1' => $filenameToStore1,
                                                'image_2' => $filenameToStore2,
                                                'image_3' => $filenameToStore3,
                                                'video_1' => $request->video_1
    		                                ]);
                }
	    	}
        }

        if($updated) {
        	return response()->json([
	            'status' => 'success',
	            'message' => 'Form Updated.'
	        ]);
        } else {
        	return response()->json([
                'status' => 'error',
                'message' => 'Update Failed.'
            ]);
        }
    }
}
