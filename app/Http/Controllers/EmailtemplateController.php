<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Emailtemplate;

class EmailtemplateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/emailtemplate');
    }

    public function list(Request $request) {
		if( $request->draw != FALSE )   {$draw   = $request->draw;}   else{$draw   = 1;}; 
		
		$order = $request->order;
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 4;}; 
		$order_fields = array('name', '', '', '', 'type');
		
		$search = $request->search;
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		$order_column 	= $order_fields[$order_column];
		$order_dir		= $order_dir;
		$filter 		= $search_value;

    	$list_total = Emailtemplate::where('name', 'LIKE', '%'.$filter.'%')->get();
    	$list_filtered = Emailtemplate::where('name', 'LIKE', '%'.$filter.'%')
                                ->orderBy($order_column, $order_dir)
    							->get();

    	$result["recordsTotal"] = 0;
		$result["recordsFiltered"] = 0;
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list_filtered as $value ) {
			$action = '
						<center>
							<button class="btn btn-sm btn-warning" onclick="_edit(\''.$value['id'].'\');" title="Edit"><i class="fa fa-edit"></i></button>
						</center>
					';

			//$type_ = $value['type'] == '1' ? 'Contact Us' : 'Comment' ;

            array_push($data, 
				array(
                    $value['name'],
                    '<b>Subject</b> ' . $value['subject'] . '<br/>' . '<b>Content</b> '. '<br/><div style="border: 1px solid lightgray; padding: 15px;">' . $value['content'] . '</div>',
                    $action
				)
			);
		}
		
		$result["data"] = $data;

		return response()->json($result);
    }

    public function detail(Request $request) {
		$record = Emailtemplate::where('id', $request->id)->first();
    	return response()->json($record);
    }
	
	public function update(Request $request) {
    	$updated = 0;

    	//rules
        $rules=array(
        	'id' => 'required',
            'type' => 'required',
            'subject' => 'required',
            'content' => 'required'
        );
          
        //message error 
        $messages=array(
        	// 'description_short.required' => 'Excerpt is required.'
        );

        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
            $updated = Emailtemplate::where('id', $request->id)
                          ->update([
                                'type' => $request->type,
                                'name' => $request->name,
                                'subject' => $request->subject,
                                'content' => $request->content
                            ]); 
        }

        if($updated) {
        	return response()->json([
	            'status' => 'success',
	            'message' => 'Form Updated.'
	        ]);
        } else {
        	return response()->json([
                'status' => 'error',
                'message' => 'Update Failed.'
            ]);
        }
    }
}
