<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Article;
use App\User;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
	}
	
    public function index()
    {
        return view('pages/article');
    }

    public function list(Request $request) {
		if( $request->draw != FALSE )   {$draw   = $request->draw;}   else{$draw   = 1;}; 
		if( $request->length != FALSE ) {$length = $request->length;} else{$length = 10;}; 
		if( $request->start != FALSE )  {$start  = $request->start;}  else{$start  = 0;}; 		
		
		$order = $request->order;
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 
		$order_fields = array('articles.title_id', 'articles.title_en', '', 'users.name', '');
		
		$search = $request->search;
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		$limit 			= (int) $length;
		$offset			= (int) $start;
		$order_column 	= $order_fields[$order_column];
		$order_dir		= $order_dir;
		$filter 		= $search_value;

    	$list_total = Article::select(['articles.*', 'users.name'])
                                ->where('articles.title_id', 'LIKE', '%'.$filter.'%')
                                ->orWhere('articles.title_en', 'LIKE', '%'.$filter.'%')
                                ->orWhere('users.name', 'LIKE', '%'.$filter.'%')
                                ->leftJoin('users', 'articles.user_id', '=', 'users.id')
                                ->get();
    	$list_filtered = Article::select(['articles.*', 'users.name'])
                                ->where('articles.title_id', 'LIKE', '%'.$filter.'%')
                                ->orWhere('articles.title_en', 'LIKE', '%'.$filter.'%')
                                ->orWhere('users.name', 'LIKE', '%'.$filter.'%')
                                ->leftJoin('users', 'articles.user_id', '=', 'users.id')
    							->take($limit)
    							->offset($offset)
    							->orderBy($order_column, $order_dir)
    							->get();

    	$result["recordsTotal"] = $list_total->count();
		$result["recordsFiltered"] = $list_total->count();
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list_filtered as $value ) {
			$action = '
						<center>
							<button class="btn btn-sm btn-primary" onclick="_detail(\''.$value['id'].'\');" title="Detail"><i class="fa fa-eye"></i></button>
							<button class="btn btn-sm btn-warning" onclick="_edit(\''.$value['id'].'\');" title="Edit"><i class="fa fa-edit"></i></button>
							<button class="btn btn-sm btn-danger" onclick="_delete(\''.$value['id'].'\');" title="Delete"><i class="fa fa-trash"></i></button>
						</center>
					';

            $featured = $value['is_featured'] == 'true' ? '<br><span class="label label-primary">Featured</span>' : '';
            $video = $value['video'] != null ? '<br/>' . '<b>Video</b> ' . '<br/>' . $value['video'] : '';

		    array_push($data, 
				array(
                    '<b>Title ID</b> ' . $value['title_id'] . '<br/>' . '<b>Title EN</b> '. $value['title_en'],
                    '<b>Image</b> ' . '<br/>' . '<img id="image_detail" alt="Image" style="width: 75px; height: 75px;" src="'.asset('storage/article/'.$value['image']).'"/>' . $video,
                    '<b>Author</b> ' . $value['name'] . $featured,
					$action
				)
			);
		}
		
		$result["data"] = $data;

		return response()->json($result);
    }

    public function store(Request $request) {
    	//rules
        $rules=array(
            'title_id' => 'required',
            'title_en' => 'required',
            'description_short_id' => 'required',
            'description_short_en' => 'required',
            'description_long_id' => 'required',
            'description_long_en' => 'required',
            'image' => 'required|image|max:2000',
            'is_featured' => 'required'
        );
          
        //message error 
        $messages=array(
            'description_short_id.required' => 'Excerpt Id is required.',
            'description_short_en.required' => 'Excerpt En is required.',
            'description_long_id.required' => 'Description Id is required.',
            'description_long_en.required' => 'Description En is required.'
            
        );

        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
        	$table = new Article;
            $table->title_id = $request->title_id;
            $table->title_en = $request->title_en;
            $table->description_short_id = $request->description_short_id;
            $table->description_short_en = $request->description_short_en;
            $table->description_long_id = $request->description_long_id;
            $table->description_long_en = $request->description_long_en;
            $table->video = $request->video;
            $table->user_id = $request->session()->get('userid');
            $table->is_featured = $request->is_featured == "true" ? "true" : "false";

            //set image unique name
            if($request->hasFile('image')) {
                $image = $request->image;
                
                $fileNameWithExt = $image->getClientOriginalName();

                $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

                $extension = $image->getClientOriginalExtension();

                $filenameToStore = $filename.'_'.time().'.'.$extension;
                
                $path = $image->storeAs('public/article', $filenameToStore);

                $table->image = $filenameToStore;
            } else {
            	$table->image = "";
            }
           
        	if($table->save()) {
	        	return response()->json([
		            'status' => 'success',
		            'message' => 'Form Submited.'
		        ]);
	        } else {
	        	return response()->json([
                    'status' => 'error',
                    'message' => 'Submit Failed.'
                ]);
	        }  
        }
    }

	public function destroy(Request $request) {
    	$deleted = Article::where('id', $request->id)->delete();

        if($deleted) {
        	return response()->json([
	            'status' => 'success',
	            'message' => 'Record Deleted.'
	        ]);
        } else {
        	return response()->json([
                'status' => 'error',
                'message' => 'Delete Failed.'
            ]);
        }
    }

    public function detail(Request $request) {
		$record = Article::where('id', $request->id)->first();
    	return response()->json($record);
    }
	
	public function update(Request $request) {
    	$updated = 0;

    	//rules
        $rules=array(
        	'id' => 'required',
            'title_id' => 'required',
            'title_en' => 'required',
            'description_short_id' => 'required',
            'description_short_en' => 'required',
            'description_long_id' => 'required',
            'description_long_en' => 'required',
            'image' => $request->hasFile('image') ? 'required|image|max:2000' : '',
            'is_featured' => 'required'
        );
          
        //message error 
        $messages=array(
            'description_short_id.required' => 'Excerpt Id is required.',
            'description_short_en.required' => 'Excerpt En is required.',
            'description_long_id.required' => 'Description Id is required.',
            'description_long_en.required' => 'Description En is required.'
        );

        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
			$featured = $request->is_featured == "true" ? "true" : "false";
            
            //set image unique name
            if($request->hasFile('image')) {
                // delete old file
                $image = Article::select ('image')->where('id', '=', $request->id)->first();
                $file_name = $image['image'];

                if(Storage::exists('public/article/'.$file_name)) {
                    $delete_file = Storage::delete('public/article/'.$file_name);
                }

                //save new file
                $image = $request->image;
                
                $fileNameWithExt = $image->getClientOriginalName();

                $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

                $extension = $image->getClientOriginalExtension();

                $filenameToStore = $filename.'_'.time().'.'.$extension;
                
                $path = $image->storeAs('public/article', $filenameToStore);

                $updated = Article::where('id', $request->id)
                              ->update([
                                    'title_id' => $request->title_id,
                                    'title_en' => $request->title_en,
                                    'description_short_id' => $request->description_short_id,
                                    'description_short_en' => $request->description_short_en,
                                    'description_long_id' => $request->description_long_id,
                                    'description_long_en' => $request->description_long_en,
                                    'image' => $filenameToStore,
                                    'video' => $request->video,
                                    'user_id' => $request->session()->get('userid'),
                                    'is_featured' => $featured
                                ]);
            } else {
                $updated = Article::where('id', $request->id)
                              ->update([
                                    'title_id' => $request->title_id,
                                    'title_en' => $request->title_en,
                                    'description_short_id' => $request->description_short_id,
                                    'description_short_en' => $request->description_short_en,
                                    'description_long_id' => $request->description_long_id,
                                    'description_long_en' => $request->description_long_en,
                                    'video' => $request->video,
                                    'user_id' => $request->session()->get('userid'),
                                    'is_featured' => $featured
                                ]);
            }    
        }

        if($updated) {
        	return response()->json([
	            'status' => 'success',
	            'message' => 'Form Updated.'
	        ]);
        } else {
        	return response()->json([
                'status' => 'error',
                'message' => 'Update Failed.'
            ]);
        }
    }
}
