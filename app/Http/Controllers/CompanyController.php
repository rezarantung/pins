<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/company');
    }

    public function detail(Request $request)
    {
        $record = Company::first();
        return response()->json($record);
    }

    public function update(Request $request) {
        $updated = 0;

        //rules
        $rules = array(
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'email_notif' => 'required',
            'address' => 'required',
            'image' => $request->hasFile('image') ? 'required|image|max:2000' : '',
        );
          
        //message error 
        $messages=array(
            //'id.required' => 'ID required.',
        );

        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
            //set image unique name
            if($request->hasFile('image')) {
                // delete old file
                $image = Company::select ('image')->where('id', '=', $request->id)->first();
                $file_name = $image['image'];

                if($file_name != null) {
                    if(Storage::exists('public/company/'.$file_name)) {
                        $delete_file = Storage::delete('public/company/'.$file_name);
                    }
                }

                //save new file
                $image = $request->image;
                
                $fileNameWithExt = $image->getClientOriginalName();

                $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

                $extension = $image->getClientOriginalExtension();

                $filenameToStore = $filename.'_'.time().'.'.$extension;
                
                $path = $image->storeAs('public/company', $filenameToStore);

                $updated = Company::where('id', $request->id)
                              ->update([
                                    'name' => $request->name,
                                    'email' => $request->email,
                                    'phone' => $request->phone,
                                    'email_notif' => $request->email_notif,
                                    'address' => $request->address,
                                    'image' => $filenameToStore,
                                    'description' => $request->description,
                                ]);
            } else {
                $updated = Company::where('id', $request->id)
                              ->update([
                                    'name' => $request->name,
                                    'email' => $request->email,
                                    'phone' => $request->phone,
                                    'email_notif' => $request->email_notif,
                                    'address' => $request->address,
                                    'description' => $request->description,
                                ]);
            }            
        }

        if($updated) {
            return response()->json([
                'status' => 'success',
                'message' => 'Form Updated.'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Update Failed.'
            ]);
        }
    }
}
