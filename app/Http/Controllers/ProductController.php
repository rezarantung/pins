<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Content;
use App\Product;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
	}
	
    public function index()
    {
        return view('pages/product');
    }

    public function list(Request $request) {
		if( $request->draw != FALSE )   {$draw   = $request->draw;}   else{$draw   = 1;}; 
		if( $request->length != FALSE ) {$length = $request->length;} else{$length = 10;}; 
		if( $request->start != FALSE )  {$start  = $request->start;}  else{$start  = 0;}; 		
		
		$order = $request->order;
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 
		$order_fields = array('title', '', '', '');
		
		$search = $request->search;
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		$limit 			= (int) $length;
		$offset			= (int) $start;
		$order_column 	= $order_fields[$order_column];
		$order_dir		= $order_dir;
		$filter 		= $search_value;

        $list_total = Product::where('title', 'LIKE', '%'.$filter.'%')->get();
    	$list_filtered = Product::where('title', 'LIKE', '%'.$filter.'%')
                                ->with("whyUs")->with("overview")->with("product")->with("cased")
    							->take($limit)
    							->offset($offset)
    							->orderBy($order_column, $order_dir)
    							->get();

    	$result["recordsTotal"] = $list_total->count();
		$result["recordsFiltered"] = $list_total->count();
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list_filtered as $value ) {
			$action = '
						<center>
							<button class="btn btn-sm btn-warning" onclick="_edit(\''.$value['id'].'\');" title="Edit"><i class="fa fa-edit"></i></button>
							<button class="btn btn-sm btn-danger" onclick="_delete(\''.$value['id'].'\');" title="Delete"><i class="fa fa-trash"></i></button>
						</center>
					';

            $overview_ = $value['overview_id'] == '' || $value['overview_id'] == null || $value['overview_id'] == 'null' ? '-' : $value['overview']->name;
            $product_ = $value['product_id'] == '' || $value['product_id'] == null || $value['product_id'] == 'null' ? '-' : $value['product']->name;
            $whyus_ = $value['why_us_id'] == '' || $value['why_us_id'] == null || $value['why_us_id'] == 'null' ? '-' : $value['whyUs']->name;
            $cased_ = $value['cased_id'] == '' || $value['cased_id'] == null || $value['cased_id'] == 'null' ? '-' : $value['cased']->name;

            $featured = $value['featured'] == 'true' ? '<br><span class="label label-primary">Featured</span>' : '';
            $content = $value['featured'] == 'true' ? '<b>Overview</b> '. $overview_ : '<b>Product</b> ' . $product_ . '<br/>' . '<b>Overview</b> '. $overview_ . '<br/>' . '<b>Why Us</b> '. $whyus_ . '<br/>' . '<b>Case Study</b> '. $cased_ ;

		    array_push($data, 
				array(
                    '<b>Title</b> ' . $value['title'] . '<br/>' . '<b>Excerpt</b> '. $value['excerpt'] . $featured,
                    $content,
					'<img alt="Image" style="width: 75px; height: 75px;" src="'.asset('storage/product/'.$value['image']).'"/>',
					$action
				)
			);
		}
		
		$result["data"] = $data;
		return response()->json($result);
	}
	
	public function content_list(Request $request) {
		$list = Content::where('type', 2)->get();
    	
    	$data = array();			
		foreach ( $list as $value ) {
			array_push($data, 
				array(
					$value['id'],
					$value['name']
				)
			);
		}
		
		$result["data"] = $data;

		return response()->json($result);
    }
	
	public function store(Request $request) {
    	//rules
        $rules=array(
        	'title' => 'required',
            'image' => 'required|image|max:2000',
            'excerpt' => 'required',
            'featured' => 'required'
        );
          
        //message error 
        $messages=array(
        	// 'product.required' => 'Product required.',
        );

        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
        	$table = new Product;

			$table->title = $request->title;
            $table->excerpt = $request->excerpt;
            $table->overview_id = $request->overview_id;
            $table->why_us_id = $request->why_us_id;
            $table->product_id = $request->product_id;
            $table->cased_id = $request->cased_id;
            $table->featured = $request->featured == "true" ? "true" : "false";

            //set image unique name
            if($request->hasFile('image')) {
                $image = $request->image;
                
                $fileNameWithExt = $image->getClientOriginalName();

                $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

                $extension = $image->getClientOriginalExtension();

                $filenameToStore = $filename.'_'.time().'.'.$extension;
                
                $path = $image->storeAs('public/product', $filenameToStore);

                $table->image = $filenameToStore;
            } else {
            	$table->image = "";
            }
           
        	if($table->save()) {
	        	return response()->json([
		            'status' => 'success',
		            'message' => 'Form Submited.'
		        ]);
	        } else {
	        	return response()->json([
                    'status' => 'error',
                    'message' => 'Submit Failed.'
                ]);
	        }  
        }
    }

	public function destroy(Request $request) {
    	$deleted = Product::where('id', $request->id)->delete();

        if($deleted) {
        	return response()->json([
	            'status' => 'success',
	            'message' => 'Record Deleted.'
	        ]);
        } else {
        	return response()->json([
                'status' => 'error',
                'message' => 'Delete Failed.'
            ]);
        }
    }

    public function detail(Request $request) {
		$record = Product::where('id', $request->id)->first();
    	return response()->json($record);
    }
	
	public function update(Request $request) {
        $updated = 0;

        //rules
        $rules=array(
            'id' => 'required',
            'title' => 'required',
			'image' => $request->hasFile('image') ? 'required|image|max:2000' : '',
            'excerpt' => 'required',
            'featured' => 'required'
        );
          
        //message error 
        $messages=array(
            //'id.required' => 'ID required.',
        );

        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
            $featured = $request->featured == "true" ? "true" : "false";
            
            //set image unique name
            if($request->hasFile('image')) {
                // delete old file
                $image = Product::select ('image')->where('id', '=', $request->id)->first();
                $file_name = $image['image'];

                if(Storage::exists('public/product/'.$file_name)) {
                    $delete_file = Storage::delete('public/product/'.$file_name);
                }

                //save new file
                $image = $request->image;
                
                $fileNameWithExt = $image->getClientOriginalName();

                $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

                $extension = $image->getClientOriginalExtension();

                $filenameToStore = $filename.'_'.time().'.'.$extension;
                
                $path = $image->storeAs('public/product', $filenameToStore);

                $updated = Product::where('id', $request->id)
                              ->update([
                                    'title' => $request->title,
                                    'excerpt' => $request->excerpt,
									'product_id' => $request->product_id,
                                    'overview_id' => $request->overview_id,
                                    'why_us_id' => $request->why_us_id,
                                    'cased_id' => $request->cased_id,
									'image' => $filenameToStore,
                                    'featured' => $featured
                                ]);
            } else {
                $updated = Product::where('id', $request->id)
                              ->update([
                                    'title' => $request->title,
                                    'excerpt' => $request->excerpt,
                                    'product_id' => $request->product_id,
                                    'overview_id' => $request->overview_id,
                                    'why_us_id' => $request->why_us_id,
                                    'cased_id' => $request->cased_id,
                                    'featured' => $featured
                                ]);
            }            
        }

        if($updated) {
            return response()->json([
                'status' => 'success',
                'message' => 'Form Updated.'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Update Failed.'
            ]);
        }
    }
}
