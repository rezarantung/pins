<aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Menus</li>
        <li class="{{Request::is('admin') ? 'active' : ''}}"><a href="{{url('/admin')}}"><i class="fa fa-home"></i> <span>Home</span></a></li>
        <li class="{{Request::is('admin/company') ? 'active' : ''}}"><a href="{{url('/admin/company')}}"><i class="fa fa-institution"></i> <span>Company</span></a></li>
        <li class="{{Request::is('admin/user') ? 'active' : ''}}"><a href="{{url('/admin/user')}}"><i class="fa fa-user"></i> <span>Users</span></a></li>
        <li class="{{Request::is('admin/menu') ? 'active' : ''}}"><a href="{{url('/admin/menu')}}"><i class="fa fa-bars"></i> <span>Menus</span></a></li>
        <li class="{{Request::is('admin/content') ? 'active' : ''}}"><a href="{{url('/admin/content')}}"><i class="fa fa-file-image-o"></i> <span>Content</span></a></li>
        <li class="{{Request::is('admin/product') ? 'active' : ''}}"><a href="{{url('/admin/product')}}"><i class="fa fa-cube"></i> <span>Products</span></a></li>
        <li class="{{Request::is('admin/article') ? 'active' : ''}}"><a href="{{url('/admin/article')}}"><i class="fa fa-newspaper-o"></i> <span>Articles</span></a></li>
        <li class="{{Request::is('admin/partner') ? 'active' : ''}}"><a href="{{url('/admin/partner')}}"><i class="fa fa-handshake-o"></i> <span>Partners</span></a></li>
        <li class="{{Request::is('admin/socialmedia') ? 'active' : ''}}"><a href="{{url('/admin/socialmedia')}}"><i class="fa fa-wechat"></i> <span>Socialmedia</span></a></li>
        <li class="{{Request::is('admin/area') ? 'active' : ''}}"><a href="{{url('/admin/area')}}"><i class="fa fa-globe"></i> <span>Area</span></a></li>
        <li class="{{Request::is('admin/team') ? 'active' : ''}}"><a href="{{url('/admin/team')}}"><i class="fa fa-group"></i> <span>Team</span></a></li>
        <li class="{{Request::is('admin/enquiry') ? 'active' : ''}}"><a href="{{url('/admin/enquiry')}}"><i class="fa fa-send-o"></i> <span>Enquiry</span></a></li>
        <li class="{{Request::is('admin/comment') ? 'active' : ''}}"><a href="{{url('/admin/comment')}}"><i class="fa fa-comments-o"></i> <span>Comment</span></a></li>
        <li class="{{Request::is('admin/emailtemplate') ? 'active' : ''}}"><a href="{{url('/admin/emailtemplate')}}"><i class="fa fa-columns"></i> <span>Email</span></a></li>
      </ul>
    </section>
</aside>