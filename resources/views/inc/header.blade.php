<header class="main-header">
    <!-- Logo -->
    <a href="{{url('/admin')}}" class="logo" style="background: white;">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <!-- <span class="logo-mini"><b>A</b>LT</span> -->
      <img src="{{asset('img/icon.jpeg')}}" class="logo-mini" alt="Pin's">
      <!-- logo for regular state and mobile devices -->
      <!-- <span class="logo-lg"><b>Admin</b>LTE</span> -->
      <img src="{{asset('img/logo2.png')}}" class="logo-lg" alt="Pin's">
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span>{{Request::session()->get('username')}}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <p>
                  {{Request::session()->get('username')}}
                  <small>Member since {{Request::session()->get('userdate')}}</small>
                </p>
              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                    <a class="btn btn-default btn-flat" data-toggle="modal" href="#modal_change_password" onclick="reset_change_password_form();">{{ __('Change Password') }}</a>
                </div>
                <div class="pull-right">
                    <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Sign out') }}</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>
    </nav>
</header>

<!-- /////////////////change password modal/////////////////////// -->
<div class="modal fade" id="modal_change_password" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body">
                <div id='modal_change_password_message_danger' class="alert alert-danger" style="display: none;"></div>
                <div id='modal_change_password_message_success' class="alert alert-success" style="display: none;"></div>
                <form action="#" id="form_change_password" class="form-horizontal">
                    <div class="form-body">
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Old Password <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="password_change_old" name="password_change_old" type="password" data-required="1" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">New Password <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="password_change_new" name="password_change_new" type="password" data-required="1" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Confirm New Password <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="password_change_confirmation" name="password_change_confirmation" type="password" data-required="1" class="form-control"/>
                            </div>
                        </div>

                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- /////////////////submit confirmation/////////////////////// -->
<div class="modal fade bs-modal-sm" id="confirm_modal_change_password" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                 Are you sure want to change your password?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="change_password({{Request::session()->get('userid')}});">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////// -->

@section('assets_header')
<script>
    var validator_change_password;

    function reset_change_password_form() {
        validator_change_password.resetForm();

        $("#modal_change_password_message_danger").hide();
        $("#modal_change_password_message_success").hide();

        $('#modal_change_password #password_change_old').val('');
        $('#modal_change_password #password_change_new').val('');
        $('#modal_change_password #password_change_confirmation').val('');
    }

    function change_password(id) {
        $password_old           = $('#modal_change_password #password_change_old').val();
        $password_new           = $('#modal_change_password #password_change_new').val();
        $password_new_confirm   = $('#modal_change_password #password_change_confirmation').val();
        
        if($password_new != $password_new_confirm) {
            $('#confirm_modal_change_password').modal('toggle');

            $('#modal_change_password_message').show();

            $('#password_change_new').val('');
            $('#password_change_confirmation').val('');
        } else {
            $(".loading").show();

            $url = 'user/change_password';
            @if(Request::is('admin'))
                $url = 'admin/user/change_password';
            @endif

            $.ajax({
                type: 'PUT',
                url: $url, 
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id' : id,
                    'password_old' : $password_old,
                    'password_new' : $password_new
                }, 
                success: function(response){
                    if(response.status == 'success') {
                        reset_change_password_form()

                        $('#confirm_modal_change_password').modal('toggle');
                        
                        $("#modal_change_password_message_success").html(response.message);
                        $("#modal_change_password_message_danger").hide();
                        $("#modal_change_password_message_success").show();
                    } else {
                        $('#confirm_modal_change_password').modal('toggle');

                        $("#modal_change_password_message_danger").html(response.message);
                        $("#modal_change_password_message_success").hide();
                        $("#modal_change_password_message_danger").show();
                    }
                    $(".loading").hide();
                }
            });
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////form validation

    $().ready(function() {
        // validate signup form on keyup and submit
        validator_change_password = $("#form_change_password").validate({
            rules: {
                password_change_old: {
                    required: true,
                    minlength: 6
                },
                password_change_new: {
                    required: true,
                    minlength: 6
                },
                password_change_confirmation: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password_change_new"
                }
            },
            messages: {
                password_change_old: {
                    required: "Please provide an old password",
                    minlength: "Your password must be at least 6 characters long"
                },
                password_change_new: {
                    required: "Please provide a new password",
                    minlength: "Your password must be at least 6 characters long"
                },
                password_change_confirmation: {
                    required: "Please provide a confirmation password",
                    minlength: "Your password must be at least 6 characters long",
                    equalTo: "Please enter the same password as above"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_change_password').modal('toggle');
            }
        });
    });
</script>
@endsection