@extends('layouts.auth')

@section('auth')
<div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form class="login-form" method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
        @csrf

      <div class="form-group has-feedback">
        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-solid placeholder-no-fix" name="email" placeholder="{{ __('E-Mail Address') }}" value="{{ old('email') }}" required autofocus>

        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong> {{ $errors->first('email') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group has-feedback">
        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-solid placeholder-no-fix" name="password" placeholder="{{ __('Password') }}" required>

        <span class="glyphicon glyphicon-lock form-control-feedback"></span>

        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong> {{ $errors->first('password') }}</strong>
            </span>
        @endif   
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label class="rememberme check" for="remember">
            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}/> {{ __('Remember Me') }}</label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('Sign In') }}</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a id="forget-password" class="forget-password" href="{{ route('password.request') }}">{{ __('I forgot my password') }}</a>
    <br>
    {{-- <a href="/admin/register" id="register-btn">Register a new membership</a> --}}

</div>
@endsection