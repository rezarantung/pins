@extends('layouts.app')

@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Menu List</h3>
                </div>
                <div id="submit_alert_success" class="alert alert-success" style="display: none; text-align: center;"></div>
                <div id="submit_alert_failed" class="alert alert-danger" style="display: none; text-align: center;"></div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6" style="margin-left: 25px;">
                            <a id="sample_editable_1_new" class="btn btn-success" data-toggle="modal" href="#modal_add" onclick="resetFormAdd();">Add Menu <i class="fa fa-plus"></i></a>
                            <a href="#" title="Refresh" class="btn btn-default" onclick="getData();"><i class="fa fa-refresh"></i></a>
                        </div>
                    </div>
                    <br>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div id="tree"></div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div id="tree-output">
                            <div id='modal_message_edit' class="alert alert-danger" style="display: none;"></div>
                            <form action="#" id="form_edit" class="form-horizontal" enctype="multipart/form-data" style="display: none;">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Parent
                                        </label>
                                        <div class="col-md-8">
                                            <input id="parent_edit_old" name="parent_edit_old" type="hidden"/>
                                            <select class="form-control" id="parent_edit" name="parent_edit">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Name ID <span class="required">
                                        * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input id="name_id_edit_old" name="name_id_edit_old" type="hidden"/>
                                            <input id="name_id_edit" name="name_id_edit" type="text" data-required="1" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Name EN <span class="required">
                                        * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input id="name_en_edit_old" name="name_en_edit_old" type="hidden"/>
                                            <input id="name_en_edit" name="name_en_edit" type="text" data-required="1" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Type <span class="required">
                                        * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input id="type_edit_old" name="type_edit_old" type="hidden"/>
                                            <select class="form-control" id="type_edit" name="type_edit" onchange="getRef('edit', this.value);">
                                                <option value="">-- choose type --</option>
                                                <option value="0">Other</option>
                                                <option value="2">Product</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Ref.</label>
                                        <div class="col-md-8">
                                            <input id="ref_edit_old" name="ref_edit_old" type="hidden"/>
                                            <select class="form-control" id="ref_edit" name="ref_edit">
                                                <option value="">-- choose ref --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Order <span class="required">
                                        * </span>
                                        </label>
                                        <div class="col-md-2">
                                            <input id="order_edit_old" name="order_edit_old" type="hidden"/>
                                            <input id="order_edit" name="order_edit" type="number" data-required="1" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Icon</label>
                                        <div class="col-md-8">
                                            <img id="uploadPreview_edit" alt="Icon" style="width: 100px; height: 100px; background: lightgray; padding: 5px;" />
                                            <input id="icon_edit" name="icon_edit" type="file" data-required="1" class="form-control" onchange="PreviewImage_edit();"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-offset-4" id="action_edit">
                                    <div>
                                        <input id="update_id" name="update_id" type="hidden"/>
                                        <button type="submit" class="btn btn-warning">Update</button>
                                        <button type="button" class="btn btn-danger" onclick="_delete();">Delete</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
</section>

<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////modal -->

<!-- /////////////////create/////////////////////// -->
<div class="modal fade" id="modal_add" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Create Menu</h4>
            </div>
            <div class="modal-body">
                <div id='modal_message_add' class="alert alert-danger" style="display: none;"></div>
                <form action="#" id="form_add" class="form-horizontal" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Parent
                            </label>
                            <div class="col-md-8">
                                <select class="form-control" id="parent_add" name="parent_add">
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Name ID <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="name_id_add" name="name_id_add" type="text" data-required="1" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Name EN <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="name_en_add" name="name_en_add" type="text" data-required="1" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Type <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-8">
                                <select class="form-control" id="type_add" name="type_add" onchange="getRef('add', this.value);">
                                    <option value="">-- choose type --</option>
                                    <option value="0">Other</option>
                                    <option value="2">Product</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Ref.</label>
                            <div class="col-md-8">
                                <select class="form-control" id="ref_add" name="ref_add">
                                    <option value="">-- choose ref --</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Order <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-2">
                                <input id="order_add" name="order_add" type="number" data-required="1" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Icon</label>
                            <div class="col-md-8">
                                <img id="uploadPreview_add" alt="Icon" style="width: 100px; height: 100px; background: lightgray; padding: 5px;" />
                                <input id="icon_add" name="icon_add" type="file" data-required="1" class="form-control" onchange="PreviewImage_add();"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
    
        </div>
        
    </div>
    
</div>

<!--submit confirmation -->
<div class="modal fade bs-modal-sm" id="confirm_modal_add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                 Are you sure want to submit?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitMenu('create');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- update confirmation -->
<div class="modal fade bs-modal-sm" id="confirm_modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                 Are you sure want to update?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitMenu('update');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!--delete confirmation-->
<div class="modal fade bs-modal-sm" id="delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                 Are you sure want to delete?
            </div>
            <div class="modal-footer">
                <input id="delete_id" name="delete_id" type="hidden"/>
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="_destroy();">Delete</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('assets')
<script>
    var validator_add;
    var validator_edit;

    jQuery(document).ready(function () {
        getData();
    });

    function resetFormAdd() {
        validator_add.resetForm();  

        $('#modal_add #parent_add').val('');
        $('#modal_add #name_id_add').val('');
        $('#modal_add #name_en_add').val('');
        $('#modal_add #type_add').val('');
        $('#modal_add #ref_add').val('');
        $('#modal_add #order_add').val('');
        $('#modal_add #icon_add').val('');
        $('#modal_add #uploadPreview_add').attr("src", "");
    }

    function PreviewImage_add() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("icon_add").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_add").src = oFREvent.target.result;
        };
    };

    function PreviewImage_edit() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("icon_edit").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_edit").src = oFREvent.target.result;
        };
    };

    //get menu tree view
    function getData() {
        getParent();
        $('#form_edit').hide();

        $(".loading").show();

        var menus;

        //get menu list
        $.ajax({
            type: 'GET',
            url: "menu/list", 
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
                menus = response.data;
                $(".loading").hide();
            },
            async: false
        });

        if(menus.length > 0) {
            $('#tree').treeview({
                data: menus,
                levels: 1,
                onNodeSelected: function(event, node) {
                    $('#form_edit').show();
                    $('#tree-output #form_edit #update_id').val(node.id);
                    $("select[name='parent_edit'] option[value='"+node._self+"']").hide();

                    getRef("edit", node._type);
                    _edit(node.id)
                },
                onNodeUnselected: function (event, node) {
                    $('#form_edit').hide();
                    $("select[name='parent_edit'] option[value='"+node._self+"']").show();
                }
            });
        } else {
            $('#tree').text("Menu tidak ditemukan");
        }
    }

    //menu list in form
    function getParent() {
        $.ajax({
            type: 'GET',
            url: "menu/parent_list", 
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
                $("#modal_add #parent_add").html("");
                $("#parent_edit").html("");
                $("#modal_add #parent_add").append("<option value=\"\">-- root --</option>");
                $("#parent_edit").append("<option value=\"\">-- root --</option>");
                for(var item of response.data) {
                    $("#modal_add #parent_add").append("<option value=\"" + item[0] + "\">" + item[1] + "</option>");
                    $("#parent_edit").append("<option value=\"" + item[0] + "\">" + item[1] + "</option>");
                }
            }
        });
    }

    //get content ref
    function getRef(kind, type) {
        //content list in form
        $.ajax({
            kind: 'GET',
            url: "menu/content_list", 
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'type': type
            }, 
            success: function(response) {
                if(kind == "add") {
                    $("#modal_add #ref_add").html("");
                    $("#modal_add #ref_add").append("<option value=\"\">-- choose ref --</option>");
                    for(var item of response.data) {
                        $("#modal_add #ref_add").append("<option value=\"" + item[0] + "\">" + item[1] + "</option>");
                    }
                } else if(kind == "edit") {
                    $("#ref_edit").html("");
                    $("#ref_edit").append("<option value=\"\">-- choose ref --</option>");
                    for(var item of response.data) {
                        $("#ref_edit").append("<option value=\"" + item[0] + "\">" + item[1] + "</option>");
                    }
                }
            },
            async: false
        });
    }

    //create
    function submitMenu(action) {
        if(action == 'create') {
            $parent           = $('#modal_add #parent_add').val();
            $name_id          = $('#modal_add #name_id_add').val();
            $name_en          = $('#modal_add #name_en_add').val();
            $type             = $('#modal_add #type_add').val();
            $ref              = $('#modal_add #ref_add').val();
            $order            = $('#modal_add #order_add').val();
            $icon             = $('#modal_add #icon_add');

            $(".loading").show();

            var form = $('#form_add')[0];
            var fdata = new FormData(form);

            fdata.append("parent", $parent);
            fdata.append("name_id", $name_id);
            fdata.append("name_en", $name_en);
            fdata.append("type", $type);
            fdata.append("ref", $ref);
            fdata.append("order", $order);
            fdata.append("icon", $icon[0].files[0]);

            $.ajax({
                type: 'POST',
                url: "menu/store", 
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: fdata, 
                contentType: false,
                processData: false,
                success: function(response){
                    if(response.status == 'success') {
                        $('#confirm_modal_add').modal('toggle');
                        $('#modal_add').modal('toggle');

                        resetFormAdd();

                        getData();
                        $("#submit_alert_success").html(response.message);
                        $("#submit_alert_success").show();
                        setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                    } else {
                        $('#confirm_modal_add').modal('toggle');

                        $("#modal_message_add").text(response.message);
                        $("#modal_message_add").show();
                        setTimeout(function() { $("#modal_message_add").slideUp(); }, 6000);
                    }
                    $(".loading").hide();
                }
            });
        } else if(action == 'update') {
            $id               = $('#tree-output #form_edit #update_id').val();
            $parent           = $('#parent_edit').val();
            $name_id          = $('#name_id_edit').val();
            $name_en          = $('#name_en_edit').val();
            $type             = $('#type_edit').val();
            $ref              = $('#ref_edit').val();
            $order            = $('#order_edit').val();
            $icon             = $('#icon_edit');

            $parent_old           = $('#parent_edit_old').val();
            $name_id_old          = $('#name_id_edit_old').val();
            $name_en_old          = $('#name_en_edit_old').val();
            $type_old             = $('#type_edit_old').val();
            $ref_old              = $('#ref_edit_old').val();
            $order_old            = $('#order_edit_old').val();

            if($parent == $parent_old && $name_id == $name_id_old && $name_en == $name_en_old && $type == $type_old && $ref == $ref_old && $order == $order_old && $icon[0].files.length == 0) {
                $('#confirm_modal_edit').modal('toggle');
            } else {
                $(".loading").show();

                var form = $('#form_edit')[0];
                var fdata = new FormData(form);

                fdata.append("id", $id);
                fdata.append("parent", $parent);
                fdata.append("name_id", $name_id);
                fdata.append("name_en", $name_en);
                fdata.append("type", $type);
                fdata.append("ref", $ref);
                fdata.append("order", $order);
                fdata.append("icon", $icon[0].files[0]); 

                $.ajax({
                    type: 'POST',
                    url: "menu/update", 
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: fdata, 
                    contentType: false,
                    processData: false,
                    success: function(response){
                        if(response.status == 'success') {
                            $('#confirm_modal_edit').modal('toggle');

                            getData();
                            $('#form_edit').hide();

                            $("#submit_alert_success").html(response.message);
                            $("#submit_alert_success").show();
                            setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                        } else {
                            $('#confirm_modal_edit').modal('toggle');

                            $("#submit_alert_failed").html(response.message);
                            $("#submit_alert_failed").show();
                            setTimeout(function() { $("#submit_alert_failed").slideUp(); }, 6000);
                        }
                        $(".loading").hide();
                    }
                });
            }
        }
    }

    //delete
    function _delete() {
        $('#delete_modal').modal('toggle');   
    }

    function _destroy() {
        $('#delete_modal').modal('toggle');

        $(".loading").show();

        $id = $('#tree-output #form_edit #update_id').val();

        $.ajax({
            type: 'DELETE',
            url: "menu/destroy", 
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $id
            }, 
            success: function(response){
                if(response.status == 'success') {
                    getData();
                    $('#form_edit').hide();
                    $("#submit_alert_success").html(response.message);
                    $("#submit_alert_success").show();
                    setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                } else {
                    $("#submit_alert_failed").html(response.message);
                    $("#submit_alert_failed").show();
                    setTimeout(function() { $("#submit_alert_failed").slideUp(); }, 6000);
                }
                $(".loading").hide();
            }
        });
    }

    //update
    function _edit(id) {
        $(".loading").show();
        $.ajax({
            type: 'GET',
            url: "menu/detail", 
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            }, 
            success: function(response){

                $('#action_edit').show();
                if(response.type == 1 || response.type == 3 || response.type == 4 || response.type == 5 || response.type == 6 || response.type == 7 || (response.parent_id == 4 && response.type == 0)) {
                    $('#action_edit').hide();
                }

                $('#parent_edit').val(response.parent_id);
                $('#name_id_edit').val(response.name_id);
                $('#name_en_edit').val(response.name_en);
                $('#type_edit').val(response.type);
                $('#ref_edit').val(response.ref_id);
                $('#order_edit').val(response.order);

                $('#uploadPreview_edit').attr("src", "{{asset('storage/menu_icon/image')}}".replace("image", response.icon));

                $('#parent_edit_old').val(response.parent_id);
                $('#name_id_edit_old').val(response.name_id);
                $('#name_en_edit_old').val(response.name_en);
                $('#type_edit_old').val(response.type);
                $('#ref_edit_old').val(response.ref_id);
                $('#order_edit_old').val(response.order);
                
                $(".loading").hide();
            }
        });  
    }

    //form validation
    $().ready(function() {
        // validate signup form on keyup and submit
        validator_add = $("#form_add").validate({
            rules: {
                name_id_add: {
                    required: true
                },
                name_en_add: {
                    required: true
                },
                type_add: {
                    required: true
                },
                order_add: {
                    required: true
                }
            },
            messages: {
                name_id_add: {
                    required: "Please enter name id"
                },
                name_en_add: {
                    required: "Please enter name en"
                },
                type_add: {
                    required: "Please choose a type"
                },
                order_add: {
                    required: "Please enter order"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_add').modal('toggle');
            }
        });
    });

    $().ready(function() {
        // validate signup form on keyup and submit
        validator_edit = $("#form_edit").validate({
            rules: {
                name_id_edit: {
                    required: true
                },
                name_en_edit: {
                    required: true
                },
                type_edit: {
                    required: true
                },
                order_edit: {
                    required: true
                }
            },
            messages: {
                name_id_edit: {
                    required: "Please enter name id"
                },
                name_en_edit: {
                    required: "Please enter name en"
                },
                type_edit: {
                    required: "Please choose a type"
                },
                order_edit: {
                    required: "Please enter order"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_edit').modal('toggle');
            }
        });
    });
</script>

<style>
    input[type=number] {
        -moz-appearance:textfield;
    }
    
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
</style>
@endsection
