@extends('layouts.app')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Enquiry Table</h3>
                </div>
                <div id="submit_alert_success" class="alert alert-success" style="display: none; text-align: center;"></div>
                <div id="submit_alert_failed" class="alert alert-danger" style="display: none; text-align: center;"></div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" title="Refresh" class="btn btn-default" onclick="ajaxReload();"><i class="fa fa-refresh"></i></a>
                        </div>
                    </div>
                    <br>
                    <table class="table table-striped table-bordered table-hover" id="table_list">
                        <thead>
                            <tr>
                                <th>
                                    Identity
                                </th>
                                <th>
                                    Message
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Modal Add !-->
<div class="modal fade" id="modal_add" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Create Enquiry</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form_add" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="name_add" name="name_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="email_add" name="email_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Phone <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="phone_add" name="phone_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Message <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <textarea id="message_add" name="message_add" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Update!-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Enquiry</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="name_edit_old" name="name_edit_old" type="hidden" />
                                <input id="name_edit" name="name_edit" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="email_edit_old" name="email_edit_old" type="hidden" />
                                <input id="email_edit" name="email_edit" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Phone <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="phone_edit_old" name="phone_edit_old" type="hidden" />
                                <input id="phone_edit" name="phone_edit" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Message <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <textarea id="message_edit_old" name="message_edit_old" type="hidden"></textarea>
                                <textarea id="message_edit" name="message_edit" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <input id="update_id" name="update_id" type="hidden" />
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Detail!-->
<div class="modal fade" id="modal_detail" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Detail Enquiry</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-4 col-md-4">
                        <label class="label-control"><strong>Name</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <label id="name_detail" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-4 col-md-4">
                        <label class="label-control"><strong>Email</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <label id="email_detail" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-4 col-md-4">
                        <label class="label-control"><strong>Phone</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <label id="phone_detail" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>
                    <div class="clearfix"></div>
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-4 col-md-4">
                        <label class="label-control"><strong>Message</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <label id="message_detail" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                </div>     
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Add Confirmation!-->
<div class="modal fade bs-modal-sm" id="confirm_modal_add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to submit?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('create');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- Update Confirmation!-->
<div class="modal fade bs-modal-sm" id="confirm_modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to update?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('update');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- Update Confirmation!-->
<div class="modal fade bs-modal-sm" id="delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete?
            </div>
            <div class="modal-footer">
                <input id="delete_id" name="delete_id" type="hidden" />
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="_destroy();">Submit</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('assets')
<script>
    var table;
    var validator_add;
    var validator_edit;

    jQuery(document).ready(function () {
        table = $('#table_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                'type': 'GET',
                'url': 'enquiry/list'
            },
            "columnDefs": [{
                "targets": 2,
                "className": "dt-right"
            }, {
                "targets": [1,2],
                "orderable": false
            }],
            "order": [],
            "bAutoWidth": false,
            "aoColumns": [{"sWidth": "25%"}, {"sWidth": "66%"}, {"sWidth": "9%"}]
        });
    });

    function ajaxReload() {
        table.ajax.reload();
    }

    function resetFormAdd() {
        validator_add.resetForm();

        $('#modal_add #name_add').val('');
        $('#modal_add #email_add').val('');
        $('#modal_add #phone_add').val('');
        $('#modal_add #message_add').val('');
    }

    function submitForm(action) {
        if (action == 'create') {
            $name = $('#modal_add #name_add').val();
            $email = $('#modal_add #email_add').val();
            $phone = $('#modal_add #phone_add').val();
            $message = $('#modal_add #message_add').val();

            $('#confirm_modal_add').modal('toggle');
            $('#modal_add').modal('toggle');

            resetFormAdd();
            $(".loading").show();

            $.ajax({
                type: 'POST',
                url: "enquiry/store",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'name': $name,
                    'email': $email,
                    'phone': $phone,
                    'message': $message
                },
                success: function (response) {
                    if (response.status == 'success') {
                        table.ajax.reload();
                        $("#submit_alert_success").html(response.message);
                        $("#submit_alert_success").show();
                        setTimeout(function () {
                            $("#submit_alert_success").slideUp();
                        }, 6000);
                    } else {
                        table.ajax.reload();
                        $("#submit_alert_failed").html(response.message);
                        $("#submit_alert_failed").show();
                        setTimeout(function () {
                            $("#submit_alert_failed").slideUp();
                        }, 6000);
                    }
                    $(".loading").hide();
                }
            });
        } else if (action == 'update') {
            $id = $('#update_id').val();
            $name = $('#modal_edit #name_edit').val();
            $email = $('#modal_edit #email_edit').val();
            $phone = $('#modal_edit #phone_edit').val();
            $message = $('#modal_edit #message_edit').val();

            $name_old = $('#modal_edit #name_edit_old').val();
            $email_old = $('#modal_edit #email_edit_old').val();
            $phone_old = $('#modal_edit #phone_edit_old').val();
            $message_old = $('#modal_edit #message_edit_old').val();

            if ($name == $name_old && $email == $email_old && $phone == $phone_old && $message == $message_old) {
                $('#confirm_modal_edit').modal('toggle');
                $('#modal_edit').modal('toggle');
            } else {
                $('#confirm_modal_edit').modal('toggle');
                $('#modal_edit').modal('toggle');

                $(".loading").show();

                $.ajax({
                    type: 'PUT',
                    url: "enquiry/update",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        'id': $id,
                        'name': $name,
                        'number': $email,
                        'phone': $phone,
                        'message': $message
                    },
                    success: function (response) {
                        if (response.status == 'success') {
                            table.ajax.reload();
                            $("#submit_alert_success").html(response.message);
                            $("#submit_alert_success").show();
                            setTimeout(function () {
                                $("#submit_alert_success").slideUp();
                            }, 6000);
                        } else {
                            table.ajax.reload();
                            $("#submit_alert_failed").html(response.message);
                            $("#submit_alert_failed").show();
                            setTimeout(function () {
                                $("#submit_alert_failed").slideUp();
                            }, 6000);
                        }
                        $(".loading").hide();
                    }
                });
            }
        }
    }

    function _delete(id) {
        $('#delete_modal').modal('toggle');
        $('#delete_id').val(id);
    }

    function _destroy(id) {
        $('#delete_modal').modal('toggle');
        $(".loading").show();
        $id = $('#delete_id').val();

        $.ajax({
            type: 'DELETE',
            url: "enquiry/destroy",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $id
            },
            success: function (response) {
                if (response.status == 'success') {
                    table.ajax.reload();
                    $("#submit_alert_success").html(response.message);
                    $("#submit_alert_success").show();
                    setTimeout(function () {
                        $("#submit_alert_success").slideUp();
                    }, 6000);
                } else {
                    table.ajax.reload();
                    $("#submit_alert_failed").html(response.message);
                    $("#submit_alert_failed").show();
                    setTimeout(function () {
                        $("#submit_alert_failed").slideUp();
                    }, 6000);
                }
                $(".loading").hide();
            }
        });
    }

    function _edit(id) {
        $(".loading").show();
        $.ajax({
            type: 'GET',
            url: "enquiry/detail",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            },
            success: function (response) {
                $('#update_id').val(response.id);
                $('#modal_edit #name_edit').val(response.name);
                $('#modal_edit #email_edit').val(response.email);
                $('#modal_edit #phone_edit').val(response.phone);
                $('#modal_edit #message_edit').val(response.message);

                $('#modal_edit #name_edit_old').val(response.name);
                $('#modal_edit #email_edit_old').val(response.email);
                $('#modal_edit #phone_edit_old').val(response.phone);
                $('#modal_edit #message_edit_old').val(response.message);

                $('#modal_edit').modal('toggle');

                $(".loading").hide();
            }
        });
    }

    function _detail(id) {
        $(".loading").show();
        $.ajax({
            type: 'GET',
            url: "enquiry/detail",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            },
            success: function (response) {
                $('#modal_detail #name_detail').text(response.name);
                $('#modal_detail #phone_detail').text(response.phone);
                $('#modal_detail #email_detail').text(response.email);
                $('#modal_detail #message_detail').text(response.message);

                $status = '';
                switch (response.status) {
                    case "Active":
                        $status = '<span class="label label-sm label-success">Active</span>';
                        break;
                    case "Inactive":
                        $status = '<span class="label label-sm label-warning">Inactive</span>';
                        break;
                    case "Locked":
                        $status = '<span class="label label-sm label-danger">Locked</span>';
                        break;
                    default:
                        $status = '-';
                }

                $('#modal_detail #status_detail').html($status);

                $('#modal_detail').modal('toggle');

                $(".loading").hide();
            }
        });
    }

    //Add Validation
    $().ready(function() {
        validator_add = $("#form_add").validate({
            rules: {
                name_add: {
                    required: true,
                    minlength: 2
                },
                email_add: {
                    required: true,
                    email: true
                },
                phone_add: {
                    required: true,
                    minlength: 10,
                    number: true
                },
                message_add: {
                    required: true,
                    minlength: 10
                },
            },
            messages: {
                name_add: {
                    required: "Please enter a name",
                    minlength: "Your name must consist of at least 2 characters"
                },
                email_add: {
                    required: "Please enter an email",
                    email: "Please enter a valid email"
                },
                phone_add: {
                    required: "Please enter a phone",
                    minlength: "Your phone must consist of at least 10 characters"
                },
                message_add: {
                    required: "Please enter a message",
                    minlength: "Your message must consist of at least 10 characters"
                },
            },
            submitHandler: function(form) {
                $('#confirm_modal_add').modal('toggle');
            }
        });
    });

    //Edit Validation
    $().ready(function() {
        validator_edit = $("#form_edit").validate({
            rules: {
                name_edit: {
                    required: true,
                    minlength: 2
                },
                email_edit: {
                    required: true,
                    email: true
                },
                phone_edit: {
                    required: true,
                    minlength: 10,
                    number: true
                },
                message_edit: {
                    required: true,
                    minlength: 10
                },
            },
            messages: {
                name_edit: {
                    required: "Please enter a name",
                    minlength: "Your name must consist of at least 2 characters"
                },
                email_edit: {
                    required: "Please enter an email",
                    email: "Please enter a valid email"
                },
                phone_edit: {
                    required: "Please enter a phone",
                    minlength: "Your phone must consist of at least 10 characters"
                },
                message_edit: {
                    required: "Please enter a message",
                    minlength: "Your message must consist of at least 10 characters"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_edit').modal('toggle');
            }
        });
    });

</script>
@endsection
