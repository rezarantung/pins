@extends('layouts.app')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Comment Table</h3>
                </div>
                <div id="submit_alert_success" class="alert alert-success" style="display: none; text-align: center;"></div>
                <div id="submit_alert_failed" class="alert alert-danger" style="display: none; text-align: center;"></div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" title="Refresh" class="btn btn-default" onclick="ajaxReload();"><i class="fa fa-refresh"></i></a>
                        </div>
                    </div>
                    <br>
                    <table class="table table-striped table-bordered table-hover" id="table_list">
                        <thead>
                            <tr>
                                <th>
                                    Article
                                </th>
                                <th>
                                    Content
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Update Confirmation!-->
<div class="modal fade bs-modal-sm" id="delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete?
            </div>
            <div class="modal-footer">
                <input id="delete_id" name="delete_id" type="hidden" />
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="_destroy();">Submit</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('assets')
<script>
    var table;
    
    jQuery(document).ready(function () {
        table = $('#table_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                'type': 'GET',
                'url': 'comment/list'
            },
            "columnDefs": [{
                "targets": 2,
                "className": "dt-center",
                "orderable": false
            }],
            "order": [],
            "bAutoWidth": false,
            "aoColumns": [{"sWidth": "38%"}, {"sWidth": "50%"}, {"sWidth": "12%"}]
        });
    });

    function ajaxReload() {
        table.ajax.reload();
    }

    function _delete(id) {
        $('#delete_modal').modal('toggle');
        $('#delete_id').val(id);
    }

    function _destroy(id) {
        $('#delete_modal').modal('toggle');
        $(".loading").show();
        $id = $('#delete_id').val();

        $.ajax({
            type: 'DELETE',
            url: "comment/destroy",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $id
            },
            success: function (response) {
                if (response.status == 'success') {
                    table.ajax.reload();
                    $("#submit_alert_success").html(response.message);
                    $("#submit_alert_success").show();
                    setTimeout(function () {
                        $("#submit_alert_success").slideUp();
                    }, 6000);
                } else {
                    table.ajax.reload();
                    $("#submit_alert_failed").html(response.message);
                    $("#submit_alert_failed").show();
                    setTimeout(function () {
                        $("#submit_alert_failed").slideUp();
                    }, 6000);
                }
                $(".loading").hide();
            }
        });
    }

    function toogle_showed(id, checked) {
        $(".loading").show();
        $.ajax({
            type: 'PUT',
            url: "comment/update", 
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id,
                'showed': checked
            }, 
            success: function(response){
                if(response.status == 'success') {
                    table.ajax.reload();
                    $("#submit_alert_success").html(response.message);
                    $("#submit_alert_success").show();
                    setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                } else {
                    table.ajax.reload();
                    $("#submit_alert_failed").html(response.message);
                    $("#submit_alert_failed").show();
                    setTimeout(function() { $("#submit_alert_failed").slideUp(); }, 6000);
                }
                $(".loading").hide();
            }
        });
    }
</script>
@endsection
