@extends('layouts.app')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Content Table</h3>
                </div>
                <div id="submit_alert_success" class="alert alert-success" style="display: none; text-align: center;"></div>
                <div id="submit_alert_failed" class="alert alert-danger" style="display: none; text-align: center;"></div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <a id="sample_editable_1_new" class="btn btn-success" data-toggle="modal" href="#modal_add"
                                onclick="resetFormAdd();">Add New <i class="fa fa-plus"></i></a>
                            <a href="#" title="Refresh" class="btn btn-default" onclick="ajaxReload();"><i class="fa fa-refresh"></i></a>
                        </div>
                    </div>
                    <br>
                    <table class="table table-striped table-bordered table-hover" id="table_list">
                        <thead>
                            <tr>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Created at
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Modal Add !-->
<div class="modal fade" id="modal_add" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Create Content</h4>
            </div>
            <div class="modal-body">
                <div id='modal_message_add' class="alert alert-danger" style="display: none;"></div>
                <form action="#" id="form_add" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-2">Name <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="name_add" name="name_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Type <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <select class="form-control" id="type_add" name="type_add">
                                    <option value="">-- choose type --</option>
                                    <option value="0">Other</option>
                                    <option value="2">Product</option>
                                </select>
                                    {{-- <option value="1">Home</option>
                                    <option value="3">News</option>
                                    <option value="4">Team</option>
                                    <option value="5">Partner</option>
                                    <option value="6">Contact</option> --}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Layout <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-4">
                                <select class="form-control" id="layout_add" name="layout_add" onchange="LayoutPreviewImage_add();">
                                    <option value="">-- choose layout --</option>
                                    <option value="1">Text Only</option>
                                    <option value="2">Image Only</option>
                                    <option value="3">Video Only</option>
                                    <option value="4">Text With Image On Top</option>
                                    <option value="5">Text With Image On Bottom</option>
                                    <option value="6">Text With Image On Left</option>
                                    <option value="7">Text With Image On Right</option>
                                    <option value="8">Image Between Text</option>
                                    <option value="9">2 Image 2 Text</option>
                                    <option value="10">Double Image+Text</option>
                                    <option value="11">Triple Image+Text</option>
                                    <option value="12">Video With Text</option>
                                    <option value="13">Video Image+Text</option>
                                </select>
                            </div>
                            <div class="col-md-5">
                                <img id="LayoutPreview_add" alt="Image"/>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_text_1_id_add" style="display: none;">
                            <label class="control-label col-md-2">Text ID 1</label>
                            <div class="col-md-9">
                                <textarea id="text_1_id_add" name="text_1_id_add" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_text_2_id_add" style="display: none;">
                            <label class="control-label col-md-2">Text ID 2</label>
                            <div class="col-md-9">
                                <textarea id="text_2_id_add" name="text_2_id_add" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_text_3_id_add" style="display: none;">
                            <label class="control-label col-md-2">Text ID 3</label>
                            <div class="col-md-9">
                                <textarea id="text_3_id_add" name="text_3_id_add" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_text_1_en_add" style="display: none;">
                            <label class="control-label col-md-2">Text EN 1</label>
                            <div class="col-md-9">
                                <textarea id="text_1_en_add" name="text_1_en_add" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_text_2_en_add" style="display: none;">
                            <label class="control-label col-md-2">Text EN 2</label>
                            <div class="col-md-9">
                                <textarea id="text_2_en_add" name="text_2_en_add" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_text_3_en_add" style="display: none;">
                            <label class="control-label col-md-2">Text EN 3</label>
                            <div class="col-md-9">
                                <textarea id="text_3_en_add" name="text_3_en_add" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_image_1_add" style="display: none;">
                            <label class="control-label col-md-2">Image 1</label>
                            <div class="col-md-9">
                                <img id="uploadPreview_1_add" alt="Image" style="width: 100px; height: 100px;" />
                                <input id="image_1_add" name="image_1_add" type="file" data-required="1" class="form-control" onchange="PreviewImage_1_add();"/>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_image_2_add" style="display: none;">
                            <label class="control-label col-md-2">Image 2</label>
                            <div class="col-md-9">
                                <img id="uploadPreview_2_add" alt="Image" style="width: 100px; height: 100px;" />
                                <input id="image_2_add" name="image_2_add" type="file" data-required="1" class="form-control" onchange="PreviewImage_2_add();"/>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_image_3_add" style="display: none;">
                            <label class="control-label col-md-2">Image 3</label>
                            <div class="col-md-9">
                                <img id="uploadPreview_3_add" alt="Image" style="width: 100px; height: 100px;" />
                                <input id="image_3_add" name="image_3_add" type="file" data-required="1" class="form-control" onchange="PreviewImage_3_add();"/>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_video_1_add" style="display: none;">
                            <label class="control-label col-md-2">Video 1</label>
                            <div class="col-md-9">
                                <input id="video_1_add" name="video_1_add" type="text" data-required="1" class="form-control" placeholder="Enter Youtube Link"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Update!-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Content</h4>
            </div>
            <div class="modal-body">
                <div id='modal_message_edit' class="alert alert-danger" style="display: none;"></div>
                <form action="#" id="form_edit" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-2">Name <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="name_edit_old" name="name_edit_old" type="hidden"/>
                                <input id="name_edit" name="name_edit" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Type <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="type_edit_old" name="type_edit_old" type="hidden"/>
                                <select class="form-control" id="type_edit" name="type_edit">
                                    <option value="">-- choose type --</option>
                                    <option value="0">Other</option>
                                    <option value="2">Product</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Layout <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-4">
                                <input id="layout_edit_old" name="layout_edit_old" type="hidden"/>
                                <select class="form-control" id="layout_edit" name="layout_edit" onchange="LayoutPreviewImage_edit(this.value);">
                                    <option value="">-- choose layout --</option>
                                    <option value="1">Text Only</option>
                                    <option value="2">Image Only</option>
                                    <option value="3">Video Only</option>
                                    <option value="4">Text With Image On Top</option>
                                    <option value="5">Text With Image On Bottom</option>
                                    <option value="6">Text With Image On Left</option>
                                    <option value="7">Text With Image On Right</option>
                                    <option value="8">Image Between Text</option>
                                    <option value="9">2 Image 2 Text</option>
                                    <option value="10">Double Image+Text</option>
                                    <option value="11">Triple Image+Text</option>
                                    <option value="12">Video With Text</option>
                                    <option value="13">Video Image+Text</option>
                                </select>
                            </div>
                            <div class="col-md-5">
                                <img id="LayoutPreview_edit" alt="Image"/>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_text_1_id_edit" style="display: none;">
                            <label class="control-label col-md-2">Text ID 1</label>
                            <div class="col-md-9">
                                <input id="text_1_id_edit_old" name="text_1_id_edit_old" type="hidden"/>
                                <textarea id="text_1_id_edit" name="text_1_id_edit" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_text_2_id_edit" style="display: none;">
                            <label class="control-label col-md-2">Text ID 2</label>
                            <div class="col-md-9">
                                <input id="text_2_id_edit_old" name="text_2_id_edit_old" type="hidden"/>
                                <textarea id="text_2_id_edit" name="text_2_id_edit" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_text_3_id_edit" style="display: none;">
                            <label class="control-label col-md-2">Text ID 3</label>
                            <div class="col-md-9">
                                <input id="text_3_id_edit_old" name="text_3_id_edit_old" type="hidden"/>
                                <textarea id="text_3_id_edit" name="text_3_id_edit" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_text_1_en_edit" style="display: none;">
                            <label class="control-label col-md-2">Text EN 1</label>
                            <div class="col-md-9">
                                <input id="text_1_en_edit_old" name="text_1_en_edit_old" type="hidden"/>
                                <textarea id="text_1_en_edit" name="text_1_en_edit" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_text_2_en_edit" style="display: none;">
                            <label class="control-label col-md-2">Text EN 2</label>
                            <div class="col-md-9">
                                <input id="text_2_en_edit_old" name="text_2_en_edit_old" type="hidden"/>
                                <textarea id="text_2_en_edit" name="text_2_en_edit" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_text_3_en_edit" style="display: none;">
                            <label class="control-label col-md-2">Text EN 3</label>
                            <div class="col-md-9">
                                <input id="text_3_en_edit_old" name="text_3_en_edit_old" type="hidden"/>
                                <textarea id="text_3_en_edit" name="text_3_en_edit"edit type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_image_1_edit" style="display: none;">
                            <label class="control-label col-md-2">Image 1</label>
                            <div class="col-md-9">
                                <img id="uploadPreview_1_edit" alt="Image" style="width: 100px; height: 100px;" />
                                <input id="image_1_edit_old" name="image_1_edit_old" type="hidden"/>
                                <input id="image_1_edit" name="image_1_edit" type="file" data-required="1" class="form-control" onchange="PreviewImage_1_edit();"/>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_image_2_edit" style="display: none;">
                            <label class="control-label col-md-2">Image 2</label>
                            <div class="col-md-9">
                                <img id="uploadPreview_2_edit" alt="Image" style="width: 100px; height: 100px;" />
                                <input id="image_2_edit_old" name="image_2_edit_old" type="hidden"/>
                                <input id="image_2_edit" name="image_2_edit" type="file" data-required="1" class="form-control" onchange="PreviewImage_2_edit();"/>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_image_3_edit" style="display: none;">
                            <label class="control-label col-md-2">Image 3</label>
                            <div class="col-md-9">
                                <img id="uploadPreview_3_edit" alt="Image" style="width: 100px; height: 100px;" />
                                <input id="image_3_edit_old" name="image_3_edit_old" type="hidden"/>
                                <input id="image_3_edit" name="image_3_edit" type="file" data-required="1" class="form-control" onchange="PreviewImage_3_edit();"/>
                            </div>
                        </div>
                        <div class="form-group" id="form_group_video_1_edit" style="display: none;">
                            <label class="control-label col-md-2">Video 1</label>
                            <div class="col-md-9">
                                <input id="video_1_edit_old" name="video_1_edit_old" type="hidden"/>
                                <input id="video_1_edit" name="video_1_edit" type="text" data-required="1" class="form-control" placeholder="Enter Youtube Link"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <input id="update_id" name="update_id" type="hidden" />
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Detail!-->
<div class="modal fade" id="modal_detail" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Detail Content</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                        <label class="label-control"><strong>Name</strong></label>
                    </div>
                    <div class="col-lg-9 col-md-9">
                        <label id="name_detail" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>

                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                        <label class="label-control"><strong>Type</strong></label>
                    </div>
                    <div class="col-lg-9 col-md-9">
                        <label id="type_detail" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>

                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                        <label class="label-control"><strong>Layout</strong></label>
                    </div>
                    <div class="col-lg-9 col-md-9">
                        <img id="layout_detail" alt="Image"/>
                    </div>
                    <div class="clearfix"></div>
                    <br/>

                    <div id="group_detail_text_1_id" style="display: none;">
                        <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                            <label class="label-control"><strong>Text ID 1</strong></label>
                        </div>
                        <div class="col-lg-8 col-md-8" style="border: 1px solid lightgray; padding: 15px">
                            <label id="text_1_id_detail" class="label-control"></label>
                        </div>
                        <div class="clearfix"></div>
                        <br/>
                    </div>

                    <div id="group_detail_text_2_id" style="display: none;">
                        <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                            <label class="label-control"><strong>Text ID 2</strong></label>
                        </div>
                        <div class="col-lg-8 col-md-8" style="border: 1px solid lightgray; padding: 15px">
                            <label id="text_2_id_detail" class="label-control"></label>
                        </div>
                        <div class="clearfix"></div>
                        <br/>
                    </div>

                    <div id="group_detail_text_3_id" style="display: none;">
                        <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                            <label class="label-control"><strong>Text ID 3</strong></label>
                        </div>
                        <div class="col-lg-8 col-md-8" style="border: 1px solid lightgray; padding: 15px">
                            <label id="text_3_id_detail" class="label-control"></label>
                        </div>
                        <div class="clearfix"></div>
                        <br/>
                    </div>

                    <div id="group_detail_text_1_en" style="display: none;">
                        <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                            <label class="label-control"><strong>Text EN 1</strong></label>
                        </div>
                        <div class="col-lg-8 col-md-8" style="border: 1px solid lightgray; padding: 15px">
                            <label id="text_1_en_detail" class="label-control"></label>
                        </div>
                        <div class="clearfix"></div>
                        <br/>
                    </div>

                    <div id="group_detail_text_2_en" style="display: none;">
                        <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                            <label class="label-control"><strong>Text EN 2</strong></label>
                        </div>
                        <div class="col-lg-8 col-md-8" style="border: 1px solid lightgray; padding: 15px">
                            <label id="text_2_en_detail" class="label-control"></label>
                        </div>
                        <div class="clearfix"></div>
                        <br/>
                    </div>

                    <div id="group_detail_text_3_en" style="display: none;">
                        <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                            <label class="label-control"><strong>Text EN 3</strong></label>
                        </div>
                        <div class="col-lg-8 col-md-8" style="border: 1px solid lightgray; padding: 15px">
                            <label id="text_3_en_detail" class="label-control"></label>
                        </div>
                        <div class="clearfix"></div>
                        <br/>
                    </div>

                    <div id="group_detail_image_1" style="display: none;">
                        <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                            <label class="label-control"><strong>Image 1</strong></label>
                        </div>
                        <div class="col-lg-9 col-md-9">
                            <img id="image_1_detail" alt="Image" style="width: 100px; height: 100px;"/>
                        </div>
                        <div class="clearfix"></div>
                        <br/>
                    </div>

                    <div id="group_detail_image_2" style="display: none;">
                        <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                            <label class="label-control"><strong>Image 2</strong></label>
                        </div>
                        <div class="col-lg-9 col-md-9">
                            <img id="image_2_detail" alt="Image" style="width: 100px; height: 100px;"/>
                        </div>
                        <div class="clearfix"></div>
                        <br/>
                    </div>

                    <div id="group_detail_image_3" style="display: none;">
                        <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                            <label class="label-control"><strong>Image 3</strong></label>
                        </div>
                        <div class="col-lg-9 col-md-9">
                            <img id="image_3_detail" alt="Image" style="width: 100px; height: 100px;"/>
                        </div>
                        <div class="clearfix"></div>
                        <br/>
                    </div>

                    <div id="group_detail_video_1" style="display: none;">
                        <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                            <label class="label-control"><strong>Video</strong></label>
                        </div>
                        <div class="col-lg-9 col-md-9">
                            <label id="video_1_detail" class="label-control"></label>
                        </div>
                        <div class="clearfix"></div>
                        <br/>
                    </div>

                </div>     
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Add Confirmation!-->
<div class="modal fade bs-modal-sm" id="confirm_modal_add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to submit?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('create');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- Update Confirmation!-->
<div class="modal fade bs-modal-sm" id="confirm_modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to update?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('update');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- delete Confirmation!-->
<div class="modal fade bs-modal-sm" id="delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete?
            </div>
            <div class="modal-footer">
                <input id="delete_id" name="delete_id" type="hidden" />
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="_destroy();">Submit</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('assets')
<script>
    var table;
    var validator_add;
    var validator_edit;

    jQuery(document).ready(function () {
        table = $('#table_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                'type': 'GET',
                'url': 'content/list'
            },
            "columnDefs": [{
                "targets": 2,
                "orderable": false,
                "className": "dt-right"
            }],
            "order": [],
            "bAutoWidth": false,
            "aoColumns": [ {"sWidth": "70%"}, {"sWidth": "15%"}, {"sWidth": "15%"}],
            "lengthMenu": [[100, 250, 500], [100, 250, 500]]
        });
    });

    function ajaxReload() {
        table.ajax.reload();
    }

    function resetFormAddDisplay() {
        $('#modal_add #form_group_text_1_id_add').hide();
        $('#modal_add #form_group_text_2_id_add').hide();
        $('#modal_add #form_group_text_3_id_add').hide();
        $('#modal_add #form_group_text_1_en_add').hide();
        $('#modal_add #form_group_text_2_en_add').hide();
        $('#modal_add #form_group_text_3_en_add').hide();
        $('#modal_add #form_group_image_1_add').hide();
        $('#modal_add #form_group_image_2_add').hide();
        $('#modal_add #form_group_image_3_add').hide();
        $('#modal_add #form_group_video_1_add').hide();
    }

    function resetFormEditDisplay() {
        $('#modal_edit #form_group_text_1_id_edit').hide();
        $('#modal_edit #form_group_text_2_id_edit').hide();
        $('#modal_edit #form_group_text_3_id_edit').hide();
        $('#modal_edit #form_group_text_1_en_edit').hide();
        $('#modal_edit #form_group_text_2_en_edit').hide();
        $('#modal_edit #form_group_text_3_en_edit').hide();
        $('#modal_edit #form_group_image_1_edit').hide();
        $('#modal_edit #form_group_image_2_edit').hide();
        $('#modal_edit #form_group_image_3_edit').hide();
        $('#modal_edit #form_group_video_1_edit').hide();
    }

    function resetDetailDisplay() {
        $('#modal_detail #group_detail_text_1_id').hide();
        $('#modal_detail #group_detail_text_2_id').hide();
        $('#modal_detail #group_detail_text_3_id').hide();
        $('#modal_detail #group_detail_text_1_en').hide();
        $('#modal_detail #group_detail_text_2_en').hide();
        $('#modal_detail #group_detail_text_3_en').hide();
        $('#modal_detail #group_detail_image_1').hide();
        $('#modal_detail #group_detail_image_2').hide();
        $('#modal_detail #group_detail_image_3').hide();
        $('#modal_detail #group_detail_video_1').hide();
    }

    function resetFormAdd() {
        validator_add.resetForm();

        resetFormAddDisplay();

        $('#text_1_id_add').unRichText();
        $('#text_2_id_add').unRichText();
        $('#text_3_id_add').unRichText();
        $('#text_1_en_add').unRichText();
        $('#text_2_en_add').unRichText();
        $('#text_3_en_add').unRichText();

        $('#modal_add #LayoutPreview_add').attr("src", "");
        $('#modal_add #name_add').val('');
        $('#modal_add #layout_add').val('');
        $('#modal_add #type_add').val('');
        $('#modal_add #text_1_id_add').val('');
        $('#modal_add #text_2_id_add').val('');
        $('#modal_add #text_3_id_add').val('');
        $('#modal_add #text_1_en_add').val('');
        $('#modal_add #text_2_en_add').val('');
        $('#modal_add #text_3_en_add').val('');
        $('#modal_add #image_1_add').val('');
        $('#modal_add #image_2_add').val('');
        $('#modal_add #image_3_add').val('');
        $('#modal_add #uploadPreview_1_add').attr("src", "");
        $('#modal_add #uploadPreview_2_add').attr("src", "");
        $('#modal_add #uploadPreview_3_add').attr("src", "");
        $('#modal_add #video_1_add').val('');

        //text editor add
        $('#text_1_id_add').richText({
              imageUpload: false,
              fileUpload: false,
              height: 150,
              videoEmbed: false,
        });
        $('#text_2_id_add').richText({
              imageUpload: false,
              fileUpload: false,
              height: 150,
              videoEmbed: false,
        });
        $('#text_3_id_add').richText({
              imageUpload: false,
              fileUpload: false,
              height: 150,
              videoEmbed: false,
        });
        $('#text_1_en_add').richText({
              imageUpload: false,
              fileUpload: false,
              height: 150,
              videoEmbed: false,
        });
        $('#text_2_en_add').richText({
              imageUpload: false,
              fileUpload: false,
              height: 150,
              videoEmbed: false,
        });
        $('#text_3_en_add').richText({
              imageUpload: false,
              fileUpload: false,
              height: 150,
              videoEmbed: false,
        });
    }

    function LayoutPreviewImage_add() {
        $layout = $('#modal_add #layout_add').val();

        $('#modal_add #LayoutPreview_add').attr("src", "{{asset('img/layout/image')}}".replace("image", $layout+'.png'));

        switch ($layout) {
            case '':
                resetFormAddDisplay();

                break;
            case '1':
                resetFormAddDisplay();

                $('#modal_add #form_group_text_1_id_add').show();
                $('#modal_add #form_group_text_1_en_add').show();
                break;
            case '2':
                resetFormAddDisplay();

                $('#modal_add #form_group_image_1_add').show();
                break;
            case '3':
                resetFormAddDisplay();

                $('#modal_add #form_group_video_1_add').show();
                break;
            case '4':
                resetFormAddDisplay();

                $('#modal_add #form_group_text_1_id_add').show();
                $('#modal_add #form_group_text_1_en_add').show();
                $('#modal_add #form_group_image_1_add').show();
                break;
            case '5':
                resetFormAddDisplay();

                $('#modal_add #form_group_text_1_id_add').show();
                $('#modal_add #form_group_text_1_en_add').show();
                $('#modal_add #form_group_image_1_add').show();
                break;
            case '6':
                resetFormAddDisplay();

                $('#modal_add #form_group_text_1_id_add').show();
                $('#modal_add #form_group_text_1_en_add').show();
                $('#modal_add #form_group_image_1_add').show();
                break;
            case '7':
                resetFormAddDisplay();

                $('#modal_add #form_group_text_1_id_add').show();
                $('#modal_add #form_group_text_1_en_add').show();
                $('#modal_add #form_group_image_1_add').show();
                break;
            case '8':
                resetFormAddDisplay();

                $('#modal_add #form_group_text_1_id_add').show();
                $('#modal_add #form_group_text_2_id_add').show();
                $('#modal_add #form_group_text_1_en_add').show();
                $('#modal_add #form_group_text_2_en_add').show();
                $('#modal_add #form_group_image_1_add').show();
                break;
            case '9':
                resetFormAddDisplay();

                $('#modal_add #form_group_text_1_id_add').show();
                $('#modal_add #form_group_text_2_id_add').show();
                $('#modal_add #form_group_text_1_en_add').show();
                $('#modal_add #form_group_text_2_en_add').show();
                $('#modal_add #form_group_image_1_add').show();
                $('#modal_add #form_group_image_2_add').show();
                break;
            case '10':
                resetFormAddDisplay();

                $('#modal_add #form_group_text_1_id_add').show();
                $('#modal_add #form_group_text_2_id_add').show();
                $('#modal_add #form_group_text_1_en_add').show();
                $('#modal_add #form_group_text_2_en_add').show();
                $('#modal_add #form_group_image_1_add').show();
                $('#modal_add #form_group_image_2_add').show();
                break;
            case '11':
                resetFormAddDisplay();

                $('#modal_add #form_group_text_1_id_add').show();
                $('#modal_add #form_group_text_2_id_add').show();
                $('#modal_add #form_group_text_3_id_add').show();
                $('#modal_add #form_group_text_1_en_add').show();
                $('#modal_add #form_group_text_2_en_add').show();
                $('#modal_add #form_group_text_3_en_add').show();
                $('#modal_add #form_group_image_1_add').show();
                $('#modal_add #form_group_image_2_add').show();
                $('#modal_add #form_group_image_3_add').show();
                break;
            case '12':
                resetFormAddDisplay();

                $('#modal_add #form_group_text_1_id_add').show();
                $('#modal_add #form_group_text_1_en_add').show();
                $('#modal_add #form_group_video_1_add').show();
                break;
            case '13':
                resetFormAddDisplay();

                $('#modal_add #form_group_text_1_id_add').show();
                $('#modal_add #form_group_text_1_en_add').show();
                $('#modal_add #form_group_image_1_add').show();
                $('#modal_add #form_group_video_1_add').show();
                break;
        }
    };

    function LayoutPreviewImage_edit(layout) {
        $('#modal_edit #LayoutPreview_edit').attr("src", "{{asset('img/layout/image')}}".replace("image", layout+'.png'));

        switch (layout) {
            case '':
                resetFormEditDisplay();

                break;
            case '1':
                resetFormEditDisplay();

                $('#modal_edit #form_group_text_1_id_edit').show();
                $('#modal_edit #form_group_text_1_en_edit').show();
                break;
            case '2':
                resetFormEditDisplay();

                $('#modal_edit #form_group_image_1_edit').show();
                break;
            case '3':
                resetFormEditDisplay();

                $('#modal_edit #form_group_video_1_edit').show();
                break;
            case '4':
                resetFormEditDisplay();

                $('#modal_edit #form_group_text_1_id_edit').show();
                $('#modal_edit #form_group_text_1_en_edit').show();
                $('#modal_edit #form_group_image_1_edit').show();
                break;
            case '5':
                resetFormEditDisplay();

                $('#modal_edit #form_group_text_1_id_edit').show();
                $('#modal_edit #form_group_text_1_en_edit').show();
                $('#modal_edit #form_group_image_1_edit').show();
                break;
            case '6':
                resetFormEditDisplay();

                $('#modal_edit #form_group_text_1_id_edit').show();
                $('#modal_edit #form_group_text_1_en_edit').show();
                $('#modal_edit #form_group_image_1_edit').show();
                break;
            case '7':
                resetFormEditDisplay();

                $('#modal_edit #form_group_text_1_id_edit').show();
                $('#modal_edit #form_group_text_1_en_edit').show();
                $('#modal_edit #form_group_image_1_edit').show();
                break;
            case '8':
                resetFormEditDisplay();

                $('#modal_edit #form_group_text_1_id_edit').show();
                $('#modal_edit #form_group_text_2_id_edit').show();
                $('#modal_edit #form_group_text_1_en_edit').show();
                $('#modal_edit #form_group_text_2_en_edit').show();
                $('#modal_edit #form_group_image_1_edit').show();
                break;
            case '9':
                resetFormEditDisplay();

                $('#modal_edit #form_group_text_1_id_edit').show();
                $('#modal_edit #form_group_text_2_id_edit').show();
                $('#modal_edit #form_group_text_1_en_edit').show();
                $('#modal_edit #form_group_text_2_en_edit').show();
                $('#modal_edit #form_group_image_1_edit').show();
                $('#modal_edit #form_group_image_2_edit').show();
                break;
            case '10':
                resetFormEditDisplay();

                $('#modal_edit #form_group_text_1_id_edit').show();
                $('#modal_edit #form_group_text_2_id_edit').show();
                $('#modal_edit #form_group_text_1_en_edit').show();
                $('#modal_edit #form_group_text_2_en_edit').show();
                $('#modal_edit #form_group_image_1_edit').show();
                $('#modal_edit #form_group_image_2_edit').show();
                break;
            case '11':
                resetFormEditDisplay();

                $('#modal_edit #form_group_text_1_id_edit').show();
                $('#modal_edit #form_group_text_2_id_edit').show();
                $('#modal_edit #form_group_text_3_id_edit').show();
                $('#modal_edit #form_group_text_1_en_edit').show();
                $('#modal_edit #form_group_text_2_en_edit').show();
                $('#modal_edit #form_group_text_3_en_edit').show();
                $('#modal_edit #form_group_image_1_edit').show();
                $('#modal_edit #form_group_image_2_edit').show();
                $('#modal_edit #form_group_image_3_edit').show();
                break;
            case '12':
                resetFormEditDisplay();

                $('#modal_edit #form_group_text_1_id_edit').show();
                $('#modal_edit #form_group_text_1_en_edit').show();
                $('#modal_edit #form_group_video_1_edit').show();
                break;
            case '13':
                resetFormEditDisplay();

                $('#modal_edit #form_group_text_1_id_edit').show();
                $('#modal_edit #form_group_text_1_en_edit').show();
                $('#modal_edit #form_group_image_1_edit').show();
                $('#modal_edit #form_group_video_1_edit').show();
                break;
        }
    };

    function submitForm(action) {
        if(action == 'create') {
            $name = $('#modal_add #name_add').val();
            $layout = $('#modal_add #layout_add').val();
            $type    = $('#modal_add #type_add').val();
            $textid_1 = $('#modal_add #text_1_id_add').val();
            $textid_2 = $('#modal_add #text_2_id_add').val();
            $textid_3 = $('#modal_add #text_3_id_add').val();
            $texten_1 = $('#modal_add #text_1_en_add').val();
            $texten_2 = $('#modal_add #text_2_en_add').val();
            $texten_3 = $('#modal_add #text_3_en_add').val();
            $image_1 = $('#modal_add #image_1_add');
            $image_2 = $('#modal_add #image_2_add');
            $image_3 = $('#modal_add #image_3_add');
            $video_1 = $('#modal_add #video_1_add').val();

            $(".loading").show();

            var form = $('#form_add')[0];
            var fdata = new FormData(form);

            fdata.append("name", $name);
            fdata.append("layout", $layout);
            fdata.append("type", $type);
            fdata.append("text_1_id", $textid_1);
            fdata.append("text_2_id", $textid_2);
            fdata.append("text_3_id", $textid_3);
            fdata.append("text_1_en", $texten_1);
            fdata.append("text_2_en", $texten_2);
            fdata.append("text_3_en", $texten_3);
            fdata.append("image_1", $image_1[0].files[0]);
            fdata.append("image_2", $image_2[0].files[0]);
            fdata.append("image_3", $image_3[0].files[0]);
            fdata.append("video_1", $video_1);

            $.ajax({
                type: 'POST',
                url: "content/store", 
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: fdata, 
                contentType: false,
                processData: false,
                success: function(response){
                    if(response.status == 'success') {
                        $('#confirm_modal_add').modal('toggle');
                        $('#modal_add').modal('toggle');

                        resetFormAdd();

                        table.ajax.reload();
                        $("#submit_alert_success").html(response.message);
                        $("#submit_alert_success").show();
                        setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                    } else {
                        $('#confirm_modal_add').modal('toggle');

                        $('#modal_add').animate({ scrollTop: 0 }, 'slow');

                        $("#modal_message_add").text(response.message);
                        $("#modal_message_add").show();
                        setTimeout(function() { $("#modal_message_add").slideUp(); }, 6000);
                    }
                    $(".loading").hide();
                }
            });
        } else if(action == 'update') {
            $id         = $('#update_id').val();
            $name       = $('#modal_edit #name_edit').val();
            $layout     = $('#modal_edit #layout_edit').val();
            $type       = $('#modal_edit #type_edit').val();
            $textid_1   = $('#modal_edit #text_1_id_edit').val();
            $textid_2   = $('#modal_edit #text_2_id_edit').val();
            $textid_3   = $('#modal_edit #text_3_id_edit').val();
            $texten_1   = $('#modal_edit #text_1_en_edit').val();
            $texten_2   = $('#modal_edit #text_2_en_edit').val();
            $texten_3   = $('#modal_edit #text_3_en_edit').val();
            $image_1    = $('#modal_edit #image_1_edit');
            $image_2    = $('#modal_edit #image_2_edit');
            $image_3    = $('#modal_edit #image_3_edit');
            $video_1    = $('#modal_edit #video_1_edit').val();

            $name_old     = $('#modal_edit #name_edit_old').val();
            $layout_old   = $('#modal_edit #layout_edit_old').val();
            $type_old     = $('#modal_edit #type_edit_old').val();
            $textid_1_old = $('#modal_edit #text_1_id_edit_old').val();
            $textid_2_old = $('#modal_edit #text_2_id_edit_old').val();
            $textid_3_old = $('#modal_edit #text_3_id_edit_old').val();
            $texten_1_old = $('#modal_edit #text_1_en_edit_old').val();
            $texten_2_old = $('#modal_edit #text_2_en_edit_old').val();
            $texten_3_old = $('#modal_edit #text_3_en_edit_old').val();
            $image_1_old  = $('#modal_edit #image_1_edit_old').val();
            $image_2_old  = $('#modal_edit #image_2_edit_old').val();
            $image_3_old  = $('#modal_edit #image_3_edit_old').val();
            $video_1_old  = $('#modal_edit #video_1_edit_old').val();

            if($name == $name_old && $layout == $layout_old && $type == $type_old && $textid_1 == $textid_1_old && $textid_2 == $textid_2_old && $textid_3 == $textid_3_old && $texten_1 == $texten_1_old && $texten_2 == $texten_2_old && $texten_3 == $texten_3_old && $video_1 == $video_1_old && $image_1[0].files.length == 0 && $image_2[0].files.length == 0 && $image_3[0].files.length == 0) {
                $('#confirm_modal_edit').modal('toggle');
                $('#modal_edit').modal('toggle');
            } else {
                $(".loading").show();

                var form = $('#form_edit')[0];
                var fdata = new FormData(form);

                fdata.append("id", $id);
                fdata.append("name", $name);
                fdata.append("name_old", $name_old);
                fdata.append("layout", $layout);
                fdata.append("type", $type); 
                fdata.append("type_old", $type_old); 
                fdata.append("text_1_id", $textid_1);
                fdata.append("text_2_id", $textid_2);
                fdata.append("text_3_id", $textid_3);
                fdata.append("text_1_en", $texten_1);
                fdata.append("text_2_en", $texten_2);
                fdata.append("text_3_en", $texten_3);
                fdata.append("image_1", $image_1[0].files[0]);
                fdata.append("image_2", $image_2[0].files[0]);
                fdata.append("image_3", $image_3[0].files[0]);
                fdata.append("image_1_old", $image_1_old);
                fdata.append("image_2_old", $image_2_old);
                fdata.append("image_3_old", $image_3_old);
                fdata.append("video_1", $video_1);

                $.ajax({
                    type: 'POST',
                    url: "content/update", 
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: fdata, 
                    contentType: false,
                    processData: false,
                    success: function(response){
                        if(response.status == 'success') {
                            $('#confirm_modal_edit').modal('toggle');
                            $('#modal_edit').modal('toggle');

                            table.ajax.reload();
                            $("#submit_alert_success").html(response.message);
                            $("#submit_alert_success").show();
                            setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                        } else {
                            $('#confirm_modal_edit').modal('toggle');

                            $('#modal_edit').animate({ scrollTop: 0 }, 'slow');

                            $("#modal_message_edit").html(response.message);
                            $("#modal_message_edit").show();
                            setTimeout(function() { $("#modal_message_edit").slideUp(); }, 6000);
                        }
                        $(".loading").hide();
                    }
                });
            }
        }
    }

    function PreviewImage_1_add() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image_1_add").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_1_add").src = oFREvent.target.result;
        };
    };

    function PreviewImage_2_add() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image_2_add").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_2_add").src = oFREvent.target.result;
        };
    };

    function PreviewImage_3_add() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image_3_add").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_3_add").src = oFREvent.target.result;
        };
    };

    function PreviewImage_1_edit() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image_1_edit").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_1_edit").src = oFREvent.target.result;
        };
    };

    function PreviewImage_2_edit() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image_2_edit").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_2_edit").src = oFREvent.target.result;
        };
    };

    function PreviewImage_3_edit() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image_3_edit").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_3_edit").src = oFREvent.target.result;
        };
    };

    function _delete(id) {
        $('#delete_modal').modal('toggle');
        $('#delete_id').val(id);
    }

    function _destroy() {
        $('#delete_modal').modal('toggle');
        $(".loading").show();
        $id = $('#delete_id').val();

        $.ajax({
            type: 'DELETE',
            url: "content/destroy",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $id
            },
            success: function (response) {
                if (response.status == 'success') {
                    table.ajax.reload();
                    $("#submit_alert_success").html(response.message);
                    $("#submit_alert_success").show();
                    setTimeout(function () {
                        $("#submit_alert_success").slideUp();
                    }, 6000);
                } else {
                    table.ajax.reload();
                    $("#submit_alert_failed").html(response.message);
                    $("#submit_alert_failed").show();
                    setTimeout(function () {
                        $("#submit_alert_failed").slideUp();
                    }, 6000);
                }
                $(".loading").hide();
            }
        });
    }

    function _edit(id) {
        $(".loading").show();

        $.ajax({
            type: 'GET',
            url: "content/detail",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            },
            success: function (response) { 
                $('#text_1_id_edit').unRichText();
                $('#text_2_id_edit').unRichText();
                $('#text_3_id_edit').unRichText();
                $('#text_1_en_edit').unRichText();
                $('#text_2_en_edit').unRichText();
                $('#text_3_en_edit').unRichText();
                $('#modal_edit #text_1_id_edit').val("");
                $('#modal_edit #text_2_id_edit').val("");
                $('#modal_edit #text_3_id_edit').val("");
                $('#modal_edit #text_1_en_edit').val("");
                $('#modal_edit #text_2_en_edit').val("");
                $('#modal_edit #text_3_en_edit').val("");

                LayoutPreviewImage_edit(response.layout.toString());

                $('#update_id').val(response.id);
                $('#modal_edit #name_edit').val(response.name);
                $('#modal_edit #layout_edit').val(response.layout);
                $('#modal_edit #type_edit').val(response.type);
                $('#modal_edit #text_1_id_edit').val(response.text_1_id);
                $('#modal_edit #text_2_id_edit').val(response.text_2_id);
                $('#modal_edit #text_3_id_edit').val(response.text_3_id);
                $('#modal_edit #text_1_en_edit').val(response.text_1_en);
                $('#modal_edit #text_2_en_edit').val(response.text_2_en);
                $('#modal_edit #text_3_en_edit').val(response.text_3_en);

                $('#modal_edit #image_1_edit').val('');
                $('#modal_edit #image_2_edit').val('');
                $('#modal_edit #image_3_edit').val('');
                $('#modal_edit #uploadPreview_1_edit').attr("src", "");
                $('#modal_edit #uploadPreview_2_edit').attr("src", "");
                $('#modal_edit #uploadPreview_3_edit').attr("src", "");

                if(response.image_1 != null) {
                    $('#modal_edit #uploadPreview_1_edit').attr("src", "{{asset('storage/content/image')}}".replace("image", response.image_1));
                };

                if(response.image_2 != null) {
                    $('#modal_edit #uploadPreview_2_edit').attr("src", "{{asset('storage/content/image')}}".replace("image", response.image_2));
                };

                if(response.image_3 != null) {
                    $('#modal_edit #uploadPreview_3_edit').attr("src", "{{asset('storage/content/image')}}".replace("image", response.image_3));
                };
                
                $('#modal_edit #video_1_edit').val(response.video_1);

                $('#modal_edit #name_edit_old').val(response.name);
                $('#modal_edit #layout_edit_old').val(response.layout);
                $('#modal_edit #type_edit_old').val(response.type);
                $('#modal_edit #text_1_id_edit_old').val(response.text_1_id);
                $('#modal_edit #text_2_id_edit_old').val(response.text_2_id);
                $('#modal_edit #text_3_id_edit_old').val(response.text_3_id);
                $('#modal_edit #text_1_en_edit_old').val(response.text_1_en);
                $('#modal_edit #text_2_en_edit_old').val(response.text_2_en);
                $('#modal_edit #text_3_en_edit_old').val(response.text_3_en);
                $('#modal_edit #image_1_edit_old').val(response.image_1);
                $('#modal_edit #image_2_edit_old').val(response.image_2);
                $('#modal_edit #image_3_edit_old').val(response.image_3);
                $('#modal_edit #video_1_edit_old').val(response.video_1);

                //for default content
                $('#modal_edit #type_edit').prop('disabled', false);
                if(response.type == 7) {
                    $('#modal_edit #type_edit').val('0');
                    $('#modal_edit #type_edit').prop('disabled', true);
                }

                //text editor edit
                $('#text_1_id_edit').richText({
                      imageUpload: false,
                      fileUpload: false,
                      height: 150,
                      videoEmbed: false,
                });
                $('#text_2_id_edit').richText({
                      imageUpload: false,
                      fileUpload: false,
                      height: 150,
                      videoEmbed: false,
                });
                $('#text_3_id_edit').richText({
                      imageUpload: false,
                      fileUpload: false,
                      height: 150,
                      videoEmbed: false,
                });
                $('#text_1_en_edit').richText({
                      imageUpload: false,
                      fileUpload: false,
                      height: 150,
                      videoEmbed: false,
                });
                $('#text_2_en_edit').richText({
                      imageUpload: false,
                      fileUpload: false,
                      height: 150,
                      videoEmbed: false,
                });
                $('#text_3_en_edit').richText({
                      imageUpload: false,
                      fileUpload: false,
                      height: 150,
                      videoEmbed: false,
                });

                $('#modal_edit').modal('toggle');

                $(".loading").hide();
            }
        });
    }

    function _detail(id) {
        $(".loading").show();
        $.ajax({
            type: 'GET',
            url: "content/detail",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            },
            success: function (response) {
                $('#modal_detail #name_detail').text(response.name);
                $('#modal_detail #layout_detail').attr("src", "{{asset('img/layout/image')}}".replace("image", response.layout + '.png'));

                switch (response.type) {
                    case 0:
                        $('#modal_detail #type_detail').text("Other");
                        break;
                    case 1:
                        $('#modal_detail #type_detail').text("Home");
                        break;
                    case 2:
                        $('#modal_detail #type_detail').text("Product");
                        break;
                    case 3:
                        $('#modal_detail #type_detail').text("News");
                        break;
                    case 4:
                        $('#modal_detail #type_detail').text("Team");
                        break;
                    case 5:
                        $('#modal_detail #type_detail').text("Partner");
                        break;
                    case 6:
                        $('#modal_detail #type_detail').text("Contact");
                        break;
                }
                
                $('#modal_detail #text_1_id_detail').html(response.text_1_id);
                $('#modal_detail #text_2_id_detail').html(response.text_2_id);
                $('#modal_detail #text_3_id_detail').html(response.text_3_id);
                $('#modal_detail #text_1_en_detail').html(response.text_1_en);
                $('#modal_detail #text_2_en_detail').html(response.text_2_en);
                $('#modal_detail #text_3_en_detail').html(response.text_3_en);

                $('#modal_detail #image_1_detail').attr("src", "");
                $('#modal_detail #image_2_detail').attr("src", "");
                $('#modal_detail #image_3_detail').attr("src", "");

                if(response.image_1 != null) {
                    $('#modal_detail #image_1_detail').attr("src", "{{asset('storage/content/image')}}".replace("image", response.image_1));
                }

                if(response.image_2 != null) {
                    $('#modal_detail #image_2_detail').attr("src", "{{asset('storage/content/image')}}".replace("image", response.image_2));
                }

                if(response.image_3 != null) {
                    $('#modal_detail #image_3_detail').attr("src", "{{asset('storage/content/image')}}".replace("image", response.image_3));
                }

                $('#modal_detail #video_1_detail').text(response.video_1);

                //layout display
                switch (response.layout) {
                    case 1:
                        resetDetailDisplay();

                        $('#modal_detail #group_detail_text_1_id').show();
                        $('#modal_detail #group_detail_text_1_en').show();
                        break;
                    case 2:
                        resetDetailDisplay();

                        $('#modal_detail #group_detail_image_1').show();
                        break;
                    case 3:
                        resetDetailDisplay();

                        $('#modal_detail #form_group_video_1_add').show();
                        break;
                    case 4:
                        resetDetailDisplay();

                        $('#modal_detail #group_detail_text_1_id').show();
                        $('#modal_detail #group_detail_text_1_en').show();
                        $('#modal_detail #group_detail_image_1').show();
                        break;
                    case 5:
                        resetDetailDisplay();

                        $('#modal_detail #group_detail_text_1_id').show();
                        $('#modal_detail #group_detail_text_1_en').show();
                        $('#modal_detail #group_detail_image_1').show();
                        break;
                    case 6:
                        resetDetailDisplay();

                        $('#modal_detail #group_detail_text_1_id').show();
                        $('#modal_detail #group_detail_text_1_en').show();
                        $('#modal_detail #group_detail_image_1').show();
                        break;
                    case 7:
                        resetDetailDisplay();

                        $('#modal_detail #group_detail_text_1_id').show();
                        $('#modal_detail #group_detail_text_1_en').show();
                        $('#modal_detail #group_detail_image_1').show();
                        break;
                    case 8:
                        resetDetailDisplay();

                        $('#modal_detail #group_detail_text_1_id').show();
                        $('#modal_detail #group_detail_text_2_id').show();
                        $('#modal_detail #group_detail_text_1_en').show();
                        $('#modal_detail #group_detail_text_2_en').show();
                        $('#modal_detail #group_detail_image_1').show();
                        break;
                    case 9:
                        resetDetailDisplay();

                        $('#modal_detail #group_detail_text_1_id').show();
                        $('#modal_detail #group_detail_text_2_id').show();
                        $('#modal_detail #group_detail_text_1_en').show();
                        $('#modal_detail #group_detail_text_2_en').show();
                        $('#modal_detail #group_detail_image_1').show();
                        $('#modal_detail #group_detail_image_2').show();
                        break;
                    case 10:
                        resetDetailDisplay();

                        $('#modal_detail #group_detail_text_1_id').show();
                        $('#modal_detail #group_detail_text_2_id').show();
                        $('#modal_detail #group_detail_text_1_en').show();
                        $('#modal_detail #group_detail_text_2_en').show();
                        $('#modal_detail #group_detail_image_1').show();
                        $('#modal_detail #group_detail_image_2').show();
                        break;
                    case 11:
                        resetDetailDisplay();

                        $('#modal_detail #group_detail_text_1_id').show();
                        $('#modal_detail #group_detail_text_2_id').show();
                        $('#modal_detail #group_detail_text_3_id').show();
                        $('#modal_detail #group_detail_text_1_en').show();
                        $('#modal_detail #group_detail_text_2_en').show();
                        $('#modal_detail #group_detail_text_3_en').show();
                        $('#modal_detail #group_detail_image_1').show();
                        $('#modal_detail #group_detail_image_2').show();
                        $('#modal_detail #group_detail_image_3').show();
                        break;
                    case 12:
                        resetDetailDisplay();

                        $('#modal_detail #group_detail_text_1_id').show();
                        $('#modal_detail #group_detail_text_1_en').show();
                        $('#modal_detail #group_detail_video_1').show();
                        break;
                    case 13:
                        resetDetailDisplay();

                        $('#modal_detail #form_group_text_1_id_add').show();
                        $('#modal_detail #form_group_text_1_en_add').show();
                        $('#modal_detail #group_detail_image_1').show();
                        $('#modal_detail #group_detail_video_1').show();
                        break;
                }

                $('#modal_detail').modal('toggle');

                $(".loading").hide();
            }
        });
    }

    //Add Validation
    $().ready(function() {
        validator_add = $("#form_add").validate({
            rules: {
                name_add: {
                    required: true
                },
                layout_add: {
                    required: true
                },
                type_add: {
                    required: true
                }
            },
            messages: {
                name_add: {
                    required: "Please enter a name"
                },
                layout_add: {
                    required: "Please choose a layout"
                },
                type_add: {
                    required: "Please choose a type"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_add').modal('toggle');
            }
        });
    });

    //Edit Validation
    $().ready(function() {
        validator_edit = $("#form_edit").validate({
            rules: {
                name_edit: {
                    required: true
                },
                layout_edit: {
                    required: true
                },
                type_edit: {
                    required: true
                }
            },
            messages: {
                name_edit: {
                    required: "Please enter a name"
                },
                layout_edit: {
                    required: "Please choose a layout"
                },
                type_edit: {
                    required: "Please choose a type"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_edit').modal('toggle');
            }
        });
    });

</script>
@endsection
