@extends('layouts.app')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Company</h3>
                </div>
                <div id="submit_alert_success" class="alert alert-success" style="display: none; text-align: center;"></div>
                <div id="submit_alert_failed" class="alert alert-danger" style="display: none; text-align: center;"></div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div id="tree-output">
                                <div id='modal_message_edit' class="alert alert-danger" style="display: none;"></div>
                                <form action="#" id="form_edit" class="form-horizontal">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Name <span class="required">
                                                    * </span>
                                            </label>
                                            <div class="col-md-7">
                                                <input id="name_edit_old" name="name_edit_old" type="hidden" />
                                                <input id="name_edit" name="name_edit" type="text" data-required="1" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Email <span class="required">
                                                    * </span>
                                            </label>
                                            <div class="col-md-7">
                                                <input id="email_edit_old" name="email_edit_old" type="hidden" />
                                                <input id="email_edit" name="email_edit" type="text" data-required="1" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Phone <span class="required">
                                                    * </span>
                                            </label>
                                            <div class="col-md-7">
                                                <input id="phone_edit_old" name="phone_edit_old" type="hidden" />
                                                <input id="phone_edit" name="phone_edit" type="text" data-required="1" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Email Notification <span class="required">
                                                    * </span>
                                            </label>
                                            <div class="col-md-7">
                                                <input id="email_notif_edit_old" name="email_notif_edit_old" type="hidden" />
                                                <input id="email_notif_edit" name="email_notif_edit" type="text" data-required="1" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Address <span class="required">
                                                    * </span>
                                            </label>
                                            <div class="col-md-7">
                                                <input id="address_edit_old" name="address_edit_old" type="hidden"></input>
                                                <textarea id="address_edit" name="address_edit" type="text" data-required="1" class="form-control" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Image</label>
                                            <div class="col-md-7">
                                                <img id="uploadPreview_edit" alt="Image" style="width: 100px; height: 100px;" />
                                                <input id="image_edit" name="image_edit" type="file" data-required="1" class="form-control" onchange="PreviewImage_edit();"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Description</label>
                                            <div class="col-md-7">
                                                <input id="description_edit_old" name="description_edit_old" type="hidden"></input>
                                                <textarea id="description_edit" name="description_edit" type="text" data-required="1" class="form-control" rows="5"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-offset-2" id="action_edit">
                                    <div>
                                        <input id="update_id" name="update_id" type="hidden"/>
                                        <button type="submit" class="btn btn-warning">Update</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Update Confirmation!-->
<div class="modal fade bs-modal-sm" id="confirm_modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to update?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('update');">Submit</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('assets')
<script>
    var validator_edit;

    jQuery(document).ready(function () {
        get_company_data();
    });

    function get_company_data() {
        $(".loading").show();
        $.ajax({
            type: 'GET',
            url: "company/detail",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {},
            success: function (response) {
                $('#update_id').val(response.id);
                $('#name_edit').val(response.name);
                $('#email_edit').val(response.email);
                $('#phone_edit').val(response.phone);
                $('#email_notif_edit').val(response.email_notif);
                $('#address_edit').val(response.address);
                $('#uploadPreview_edit').attr("src", "{{asset('storage/company/image')}}".replace("image", response.image));
                $('#description_edit').val(response.description);

                $('#name_edit_old').val(response.name);
                $('#email_edit_old').val(response.email);
                $('#phone_edit_old').val(response.phone);
                $('#email_notif_edit_old').val(response.email_notif);
                $('#address_edit_old').val(response.address);
                $('#description_edit_old').val(response.description);

                $(".loading").hide();
            }
        });
    }

    function submitForm(action) {
        if(action == 'create') {
            
        } else if(action == 'update') {
            $id         = $('#update_id').val();
            $name       = $('#name_edit').val();
            $email      = $('#email_edit').val();
            $phone      = $('#phone_edit').val();
            $email_notif        = $('#email_notif_edit').val();
            $address    = $('#address_edit').val();
            $image      = $('#image_edit');
            $description = $('#description_edit').val();

            $name_old    = $('#name_edit_old').val();
            $email_old   = $('#email_edit_old').val();
            $phone_old   = $('#phone_edit_old').val();
            $email_notif_old     = $('#email_notif_edit_old').val();
            $address_old = $('#address_edit_old').val();
            $description_old = $('#description_edit_old').val();

            if($name == $name_old && $email == $email_old && $phone == $phone_old && $email_notif == $email_notif_old && $address == $address_old && $description == $description_old && $image[0].files.length == 0) {
                $('#confirm_modal_edit').modal('toggle');
                //$('#modal_edit').modal('toggle');
            } else {
                $(".loading").show();

                var form = $('#form_edit')[0];
                var fdata = new FormData(form);

                fdata.append("id", $id);
                fdata.append("name", $name);
                fdata.append("email", $email);
                fdata.append("phone", $phone);
                fdata.append("email_notif", $email_notif);
                fdata.append("address", $address);
                fdata.append("image", $image[0].files[0]);
                fdata.append("description", $description);

                $.ajax({
                    type: 'POST',
                    url: "company/update", 
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: fdata, 
                    contentType: false,
                    processData: false,
                    success: function(response){
                        if(response.status == 'success') {
                            $('#confirm_modal_edit').modal('toggle');
                            //$('#modal_edit').modal('toggle');

                            get_company_data();
                             $('#image_edit').val('');
                            //table.ajax.reload();
                            $("#submit_alert_success").html(response.message);
                            $("#submit_alert_success").show();
                            setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                        } else {
                            $('#confirm_modal_edit').modal('toggle');

                            //$('#modal_edit').animate({ scrollTop: 0 }, 'slow');

                            $("#submit_alert_failed").html(response.message);
                            $("#submit_alert_failed").show();
                            setTimeout(function() { $("#submit_alert_failed").slideUp(); }, 6000);
                        }
                        $(".loading").hide();
                    }
                });
            }
        }
    }

    function PreviewImage_edit() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image_edit").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_edit").src = oFREvent.target.result;
        };
    };

    function _edit(id) {
        $(".loading").show();
        $.ajax({
            type: 'GET',
            url: "company/detail",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            },
            success: function (response) {
                $('#update_id').val(response.id);
                $('#modal_edit #name_edit').val(response.name);
                $('#modal_edit #email_edit').val(response.email);
                $('#modal_edit #phone_edit').val(response.phone);
                $('#modal_edit #email_notif_edit').val(response.email_notif);
                $('#modal_edit #address_edit').val(response.address);
                $('#modal_edit #uploadPreview_edit').attr("src", "{{asset('storage/company/image')}}".replace("image", response.image));
                $('#modal_edit #description_edit').val(response.description);

                $('#modal_edit #name_edit_old').val(response.name);
                $('#modal_edit #email_edit_old').val(response.email);
                $('#modal_edit #phone_edit_old').val(response.phone);
                $('#modal_edit #email_notif_edit_old').val(response.email_notif);
                $('#modal_edit #address_edit_old').val(response.address);
                $('#modal_edit #description_edit_old').val(response.description);

                $('#modal_edit').modal('toggle');

                $(".loading").hide();
            }
        });
    }

    //Edit Validation
    $().ready(function() {
        validator_edit = $("#form_edit").validate({
            rules: {
                name_edit: {
                    required: true
                },
                email_edit: {
                    required: true
                },
                phone_edit: {
                    required: true
                },
                email_notif_edit: {
                    required: true
                },
                address_edit: {
                    required: true
                }
            },
            messages: {
                name_edit: {
                    required: "Please enter a name",
                    minlength: "Your name must consist of at least 2 characters"
                },
                email_edit: {
                    required: "Please enter an email",
                    email: "Please enter a valid email"
                },
                phone_edit: {
                    required: "Please enter a phone",
                    minlength: "Your phone must consist of at least 10 characters"
                },
                email_notif_edit: {
                    required: "Please enter a email_notif",
                    minlength: "Your email_notif must consist of at least 10 characters"
                },
                address_edit: {
                    required: "Please enter a address",
                    minlength: "Your address must consist of at least 10 characters"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_edit').modal('toggle');
            }
        });
    });
</script>
@endsection
