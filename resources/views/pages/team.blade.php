@extends('layouts.app')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Team Table</h3>
                </div>
                <div id="submit_alert_success" class="alert alert-success" style="display: none; text-align: center;"></div>
                <div id="submit_alert_failed" class="alert alert-danger" style="display: none; text-align: center;"></div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <a id="sample_editable_1_new" class="btn btn-success" data-toggle="modal" href="#modal_add"
                                onclick="resetFormAdd();">Add New <i class="fa fa-plus"></i></a>
                            <a href="#" title="Refresh" class="btn btn-default" onclick="ajaxReload();"><i class="fa fa-refresh"></i></a>
                        </div>
                    </div>
                    <br>
                    <table class="table table-striped table-bordered table-hover" id="table_list">
                        <thead>
                            <tr>
                                <th>
                                    Info
                                </th>
                                <th>
                                    Photo
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Modal Add !-->
<div class="modal fade" id="modal_add" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Create Team</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form_add" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-2">Name <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="name_add" name="name_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Group <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-9">
                                <select class="form-control" id="group_name_add" name="group_name_add">
                                    <option value="0">-- choose group --</option>
                                    <option value="1">Komaris</option>
                                    <option value="2">Direksi</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Title ID<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="title_id_add" name="title_id_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Title EN<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="title_en_add" name="title_en_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Photo <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <img id="uploadPreview_add" alt="Image" style="width: 120px; height: 150px;" />
                                <input id="photo_add" name="photo_add" type="file" data-required="1" class="form-control" onchange="PreviewImage_add();"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Detail ID<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <textarea id="details_id_add" name="details_id_add" data-required="1" class="form-control" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Detail EN<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <textarea id="details_en_add" name="details_en_add" data-required="1" class="form-control" rows="10"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Update!-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Team</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-2">Name <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="name_edit_old" name="name_edit_old" type="hidden" />
                                <input id="name_edit" name="name_edit" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Group <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="group_name_edit_old" name="group_name_edit_old" type="hidden" />
                                <select class="form-control" id="group_name_edit" name="group_name_edit">
                                    <option value="0">-- choose group --</option>
                                    <option value="1">Komaris</option>
                                    <option value="2">Direksi</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Title ID<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="title_id_edit_old" name="title_id_edit_old" type="hidden" />
                                <input id="title_id_edit" name="title_id_edit" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Title EN<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="title_en_edit_old" name="title_en_edit_old" type="hidden" />
                                <input id="title_en_edit" name="title_en_edit" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Photo <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <img id="uploadPreview_edit" alt="Image" style="width: 120px; height: 150px;" />
                                <input id="photo_edit" name="photo_edit" type="file" data-required="1" class="form-control" onchange="PreviewImage_edit();"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Details ID<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="details_id_edit_old" name="details_id_edit_old" type="hidden" />
                                <textarea id="details_id_edit" name="details_id_edit" data-required="1" class="form-control" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Details EN<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="details_en_edit_old" name="details_en_edit_old" type="hidden" />
                                <textarea id="details_en_edit" name="details_en_edit" data-required="1" class="form-control" rows="10"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <input id="update_id" name="update_id" type="hidden" />
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Detail!-->
<div class="modal fade" id="modal_detail" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Detail Team</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-4 col-md-4">
                        <label class="label-control"><strong>Name</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <label id="name_detail" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-4 col-md-4">
                        <label class="label-control"><strong>Title</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <label id="title_detail" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-4 col-md-4">
                        <label class="label-control"><strong>Photo</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <label id="photo_detail" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-4 col-md-4">
                        <label class="label-control"><strong>Details</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <label id="details_detail" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>
                    <div class="clearfix"></div>
                </div>     
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Add Confirmation!-->
<div class="modal fade bs-modal-sm" id="confirm_modal_add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to submit?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('create');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- Update Confirmation!-->
<div class="modal fade bs-modal-sm" id="confirm_modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to update?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('update');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- Update Confirmation!-->
<div class="modal fade bs-modal-sm" id="delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete?
            </div>
            <div class="modal-footer">
                <input id="delete_id" name="delete_id" type="hidden" />
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="_destroy();">Submit</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('assets')
<script>
    var table;
    var validator_add;
    var validator_edit;

    jQuery(document).ready(function () {
        table = $('#table_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                'type': 'GET',
                'url': 'team/list'
            },
            "columnDefs": [{
                "targets": [1,2],
                "className": "dt-center"
            }, {
                "targets": [1,2],
                "orderable": false
            }],
            "order": [],
            "bAutoWidth": false,
            "aoColumns": [{"sWidth": "66%"}, {"sWidth": "25%"}, {"sWidth": "9%"}]
        });
    });

    function ajaxReload() {
        table.ajax.reload();
    }

    function resetFormAdd() {
        validator_add.resetForm();

        $('#modal_add #name_add').val('');
        $('#modal_add #group_name_add').val('');
        $('#modal_add #title_id_add').val('');
        $('#modal_add #title_en_add').val('');
        $('#modal_add #photo_add').val('');
        $('#modal_add #details_id_add').val('');
        $('#modal_add #details_en_add').val('');
    }

    function submitForm(action) {
        if (action == 'create') {
            $name = $('#modal_add #name_add').val();
            $group = $('#modal_add #group_name_add').val();
            $title_id = $('#modal_add #title_id_add').val();
            $title_en = $('#modal_add #title_en_add').val();
            $photo = $('#modal_add #photo_add');
            $details_id = $('#modal_add #details_id_add').val();
            $details_en = $('#modal_add #details_en_add').val();

            $(".loading").show();

            var form = $('#form_add')[0];
            var fdata = new FormData(form);

            fdata.append("name", $name);
            fdata.append("group_name", $group);
            fdata.append("title_id", $title_id);
            fdata.append("title_en", $title_en);
            fdata.append("photo", $photo[0].files[0]);
            fdata.append("details_id", $details_id);
            fdata.append("details_en", $details_en);

            $.ajax({
                type: 'POST',
                url: "team/store",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: fdata, 
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.status == 'success') {
                        $('#confirm_modal_add').modal('toggle');
                        $('#modal_add').modal('toggle');

                        resetFormAdd();

                        table.ajax.reload();
                        $("#submit_alert_success").html(response.message);
                        $("#submit_alert_success").show();
                        setTimeout(function () {
                            $("#submit_alert_success").slideUp();
                        }, 6000);
                    } else {
                        $('#confirm_modal_add').modal('toggle');

                        $("#modal_message_add").text(response.message);
                        $("#modal_message_add").show();
                        setTimeout(function() { $("#modal_message_add").slideUp(); }, 6000);
                    }
                    $(".loading").hide();
                }
            });
        } else if (action == 'update') {
            $id = $('#update_id').val();
            $name = $('#modal_edit #name_edit').val();
            $group = $('#modal_edit #group_name_edit').val();
            $title_id = $('#modal_edit #title_id_edit').val();
            $title_en = $('#modal_edit #title_en_edit').val();
            $photo = $('#modal_edit #photo_edit');
            $details_id = $('#modal_edit #details_id_edit').val();
            $details_en = $('#modal_edit #details_en_edit').val();

            $name_old = $('#modal_edit #name_edit_old').val();
            $group_old = $('#modal_edit #group_name_edit_old').val();
            $title_id_old = $('#modal_edit #title_id_edit_old').val();
            $title_en_old = $('#modal_edit #title_en_edit_old').val();
            $details_id_old = $('#modal_edit #details_id_edit_old').val();
            $details_en_old = $('#modal_edit #details_en_edit_old').val();

            if ($name == $name_old && $title_id == $title_id_old && $title_en == $title_en_old && $group == $group_old && $details_id == $details_id_old && $details_en == $details_en_old && $photo[0].files.length == 0) {
                $('#confirm_modal_edit').modal('toggle');
                $('#modal_edit').modal('toggle');
            } else {
                $(".loading").show();

                var form = $('#form_edit')[0];
                var fdata = new FormData(form);

                fdata.append("id", $id);
                fdata.append("name", $name);
                fdata.append("group_name", $group);
                fdata.append("title_id", $title_id);
                fdata.append("title_en", $title_en);
                fdata.append("photo", $photo[0].files[0]);
                fdata.append("details_id", $details_id);
                fdata.append("details_en", $details_en);

                $.ajax({
                    type: 'POST',
                    url: "team/update",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: fdata, 
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response.status == 'success') {
                            $('#confirm_modal_edit').modal('toggle');
                            $('#modal_edit').modal('toggle');

                            table.ajax.reload();
                            $("#submit_alert_success").html(response.message);
                            $("#submit_alert_success").show();
                            setTimeout(function () {
                                $("#submit_alert_success").slideUp();
                            }, 6000);
                        } else {
                            $('#confirm_modal_edit').modal('toggle');

                            $("#modal_message_edit").html(response.message);
                            $("#modal_message_edit").show();
                            setTimeout(function () {
                                $("#modal_message_edit").slideUp();
                            }, 6000);
                        }
                        $(".loading").hide();
                    }
                });
            }
        }
    }

    function PreviewImage_add() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("photo_add").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_add").src = oFREvent.target.result;
        };
    };

    function PreviewImage_edit() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("photo_edit").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_edit").src = oFREvent.target.result;
        };
    };

    function _delete(id) {
        $('#delete_modal').modal('toggle');
        $('#delete_id').val(id);
    }

    function _destroy(id) {
        $('#delete_modal').modal('toggle');
        $(".loading").show();
        $id = $('#delete_id').val();

        $.ajax({
            type: 'DELETE',
            url: "team/destroy",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $id
            },
            success: function (response) {
                if (response.status == 'success') {
                    table.ajax.reload();
                    $("#submit_alert_success").html(response.message);
                    $("#submit_alert_success").show();
                    setTimeout(function () {
                        $("#submit_alert_success").slideUp();
                    }, 6000);
                } else {
                    table.ajax.reload();
                    $("#submit_alert_failed").html(response.message);
                    $("#submit_alert_failed").show();
                    setTimeout(function () {
                        $("#submit_alert_failed").slideUp();
                    }, 6000);
                }
                $(".loading").hide();
            }
        });
    }

    function _edit(id) {
        $(".loading").show();
        $.ajax({
            type: 'GET',
            url: "team/detail",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            },
            success: function (response) {
                $('#update_id').val(response.id);
                $('#modal_edit #name_edit').val(response.name);
                $('#modal_edit #group_name_edit').val(response.group_name);
                $('#modal_edit #title_id_edit').val(response.title_id);
                $('#modal_edit #title_en_edit').val(response.title_en);
                $('#modal_edit #details_id_edit').val(response.details_id);
                $('#modal_edit #details_en_edit').val(response.details_en);
                $('#modal_edit #uploadPreview_edit').attr("src", "{{asset('storage/team/image')}}".replace("image", response.photo));

                $('#modal_edit #name_edit_old').val(response.name);
                $('#modal_edit #group_name_edit_old').val(response.group_name);
                $('#modal_edit #title_id_edit_old').val(response.title_id);
                $('#modal_edit #title_en_edit_old').val(response.title_en);
                $('#modal_edit #details_id_edit_old').val(response.details_id);
                $('#modal_edit #details_en_edit_old').val(response.details_en);

                $('#modal_edit').modal('toggle');

                $(".loading").hide();
            }
        });
    }

    function _detail(id) {
        $(".loading").show();
        $.ajax({
            type: 'GET',
            url: "team/detail",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            },
            success: function (response) {
                $('#modal_detail #name_detail').text(response.name);
                $('#modal_detail #photo_detail').text(response.photo);
                $('#modal_detail #title_detail').text(response.title);

                $status = '';
                switch (response.status) {
                    case "Active":
                        $status = '<span class="label label-sm label-success">Active</span>';
                        break;
                    case "Inactive":
                        $status = '<span class="label label-sm label-warning">Inactive</span>';
                        break;
                    case "Locked":
                        $status = '<span class="label label-sm label-danger">Locked</span>';
                        break;
                    default:
                        $status = '-';
                }

                $('#modal_detail #status_detail').html($status);

                $('#modal_detail').modal('toggle');

                $(".loading").hide();
            }
        });
    }

    //Add Validation
    $().ready(function() {
        validator_add = $("#form_add").validate({
            rules: {
                name_add: {
                    required: true,
                    minlength: 2
                },
                title_id_add: {
                    required: true,
                },
                title_en_add: {
                    required: true,
                },
                photo_add: {
                    required: true,
                }
            },
            messages: {
                name_add: {
                    required: "Please enter a name",
                    minlength: "Your name must consist of at least 2 characters"
                },
                title_id_add: {
                    required: "Please enter a title",
                    title: "Please enter a valid title"
                },
                title_en_add: {
                    required: "Please enter a title",
                    title: "Please enter a valid title"
                },
                photo_add: {
                    required: "Please enter a photo",
                    minlength: "Your photo must consist of at least 10 characters"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_add').modal('toggle');
            }
        });
    });

    //Edit Validation
    $().ready(function() {
        validator_edit = $("#form_edit").validate({
            rules: {
                name_edit: {
                    required: true,
                    minlength: 2
                },
                title_id_edit: {
                    required: true
                },
                title_en_edit: {
                    required: true
                }
            },
            messages: {
                name_edit: {
                    required: "Please enter a name",
                    minlength: "Your name must consist of at least 2 characters"
                },
                title_id_edit: {
                    required: "Please enter a title",
                    title: "Please enter a valid title"
                },
                title_en_edit: {
                    required: "Please enter a title",
                    title: "Please enter a valid title"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_edit').modal('toggle');
            }
        });
    });

</script>
@endsection
