@extends('layouts.app')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Users Table</h3>
        </div>
        <div id="submit_alert_success" class="alert alert-success" style="display: none; text-align: center;"></div>
        <div id="submit_alert_failed" class="alert alert-danger" style="display: none; text-align: center;"></div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <a id="sample_editable_1_new" class="btn btn-success" data-toggle="modal" href="#modal_add" onclick="resetFormAdd();">Add New <i class="fa fa-plus"></i></a>
                    <a href="#" title="Refresh" class="btn btn-default" onclick="ajaxReload();"><i class="fa fa-refresh"></i></a>
                </div>
            </div>
            <br>
            <table class="table table-striped table-bordered table-hover" id="table_list">
                <thead>
                    <tr>
                        <th>
                             Username
                        </th>
                        <th>
                             Email
                        </th>
                        <th>
                             Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- create -->
<div class="modal fade" id="modal_add" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Create User</h4>
            </div>
            <div class="modal-body">
                
                <form action="#" id="form_add" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="name_add" name="name_add" type="text" data-required="1" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="email_add" name="email_add" type="text" data-required="1" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Password <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="password_add" name="password_add" type="password" data-required="1" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Confirm Password <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="password_confirmation_add" name="password_confirmation_add" type="password" data-required="1" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
    
        </div>
        
    </div>
    
</div>

<!-- update -->
<div class="modal fade" id="modal_edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update User</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="name_edit_old" name="name_edit_old" type="hidden"/>
                                <input id="name_edit" name="name_edit" type="text" data-required="1" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="email_edit_old" name="email_edit_old" type="hidden"/>
                                <input id="email_edit" name="email_edit" type="text" data-required="1" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <input id="update_id" name="update_id" type="hidden"/>
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- submit confirmation -->
<div class="modal fade bs-modal-sm" id="confirm_modal_add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                 Are you sure want to submit?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('create');">Submit</button>
            </div>
        </div>
    </div>
</div>
<!--update confirmation -->
<div class="modal fade bs-modal-sm" id="confirm_modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                 Are you sure want to update?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('update');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- delete confirmation -->
<div class="modal fade bs-modal-sm" id="delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                 Are you sure want to delete?
            </div>
            <div class="modal-footer">
                <input id="delete_id" name="delete_id" type="hidden"/>
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="_destroy();">Delete</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('assets')
<script>
    var table;
    var validator_add;
    var validator_edit;

    jQuery(document).ready(function() {       
        table = $('#table_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                'type' : 'GET',
                'url': 'user/list'     
            },
            "columnDefs": [{
                "targets": 2,
                "orderable": false,
                "className": "dt-center"
            }],
            "order": [],
            "bAutoWidth": false,
            "aoColumns": [{"sWidth":"43%"},{"sWidth":"48%"},{"sWidth":"9%"}]
        });
    });

    function ajaxReload() {
        table.ajax.reload();
    }

    function resetFormAdd() {
        validator_add.resetForm();

        $('#modal_add #name_add').val('');
        $('#modal_add #email_add').val('');
        $('#modal_add #password_add').val('');
        $('#modal_add #password_confirmation_add').val('');
    }

    //create
    function submitForm(action) {
        if(action == 'create') {
            $name               = $('#modal_add #name_add').val();
            $email              = $('#modal_add #email_add').val();
            $password           = $('#modal_add #password_add').val();
            $password_confirm   = $('#modal_add #password_confirmation_add').val();
            
            if($password != $password_confirm) {
                $('#confirm_modal_add').modal('toggle');

                $('#modal_add_alert_danger2').show();

                $('#password_add').val('');
                $('#password_confirmation_add').val('');
            } else {
                $('#confirm_modal_add').modal('toggle');
                $('#modal_add').modal('toggle');

                resetFormAdd();

                $(".loading").show();

                $.ajax({
                    type: 'POST',
                    url: "user/store", 
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        'name': $name,
                        'email': $email,
                        'password': $password
                    }, 
                    success: function(response){
                        if(response.status == 'success') {
                            table.ajax.reload();
                            $("#submit_alert_success").html(response.message);
                            $("#submit_alert_success").show();
                            setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                        } else {
                            table.ajax.reload();
                            $("#submit_alert_failed").html(response.message);
                            $("#submit_alert_failed").show();
                            setTimeout(function() { $("#submit_alert_failed").slideUp(); }, 6000);
                        }
                        $(".loading").hide();
                    }
                });
            }
        } else if(action == 'update') {
            $id                 = $('#update_id').val();
            $name               = $('#modal_edit #name_edit').val();
            $email              = $('#modal_edit #email_edit').val();

            $name_old           = $('#modal_edit #name_edit_old').val();
            $email_old          = $('#modal_edit #email_edit_old').val();

            if($name == $name_old && $email == $email_old) {
                $('#confirm_modal_edit').modal('toggle');
                $('#modal_edit').modal('toggle');
            } else {
                $('#confirm_modal_edit').modal('toggle');
                $('#modal_edit').modal('toggle');

                $(".loading").show();

                $.ajax({
                    type: 'PUT',
                    url: "user/update", 
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        'id': $id,
                        'name': $name,
                        'email': $email,
                        'email_old': $email_old,
                    }, 
                    success: function(response){
                        if(response.status == 'success') {
                            table.ajax.reload();
                            $("#submit_alert_success").html(response.message);
                            $("#submit_alert_success").show();
                            setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                        } else {
                            table.ajax.reload();
                            $("#submit_alert_failed").html(response.message);
                            $("#submit_alert_failed").show();
                            setTimeout(function() { $("#submit_alert_failed").slideUp(); }, 6000);
                        }
                        $(".loading").hide();
                    }
                });
            }
        }
    }

    //delete
    function _delete(id) {
        $('#delete_modal').modal('toggle');
        $('#delete_id').val(id);   
    }

    function _destroy(id) {
        $('#delete_modal').modal('toggle');

        $(".loading").show();

        $id = $('#delete_id').val();

        $.ajax({
            type: 'DELETE',
            url: "user/destroy", 
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $id
            }, 
            success: function(response){
                if(response.status == 'success') {
                    table.ajax.reload();
                    $("#submit_alert_success").html(response.message);
                    $("#submit_alert_success").show();
                    setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                } else {
                    table.ajax.reload();
                    $("#submit_alert_failed").html(response.message);
                    $("#submit_alert_failed").show();
                    setTimeout(function() { $("#submit_alert_failed").slideUp(); }, 6000);
                }
                $(".loading").hide();
            }
        });
    }

    //update
    function _edit(id) {
        $(".loading").show();
        $.ajax({
            type: 'GET',
            url: "user/detail", 
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            }, 
            success: function(response){
                validator_edit.resetForm();

                $('#update_id').val(response.id);
                $('#modal_edit #name_edit').val(response.name);
                $('#modal_edit #email_edit').val(response.email);

                $('#modal_edit #name_edit_old').val(response.name);
                $('#modal_edit #email_edit_old').val(response.email);
            
                $('#modal_edit').modal('toggle');  

                $(".loading").hide();
            }
        });  
    }

    //form validation
    $().ready(function() {
        // validate signup form on keyup and submit
        validator_add = $("#form_add").validate({
            rules: {
                name_add: {
                    required: true,
                    minlength: 2
                },
                email_add: {
                    required: true,
                    email: true
                },
                password_add: {
                    required: true,
                    minlength: 6
                },
                password_confirmation_add: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password_add"
                }
            },
            messages: {
                name_add: {
                    required: "Please enter a name",
                    minlength: "Your name must consist of at least 2 characters"
                },
                email_add: {
                    required: "Please enter an email",
                    email: "Please enter a valid email"
                },
                password_add: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 6 characters long"
                },
                password_confirmation_add: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 6 characters long",
                    equalTo: "Please enter the same password as above"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_add').modal('toggle');
            }
        });
    });

    $().ready(function() {
        // validate signup form on keyup and submit
        validator_edit = $("#form_edit").validate({
            rules: {
                name_edit: {
                    required: true,
                    minlength: 2
                },
                email_edit: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name_edit: {
                    required: "Please enter a name",
                    minlength: "Your name must consist of at least 2 characters"
                },
                email_edit: {
                    required: "Please enter an email",
                    email: "Please enter a valid email"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_edit').modal('toggle');
            }
        });
    });
</script>
@endsection