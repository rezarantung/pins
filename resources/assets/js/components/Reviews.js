import React, { Component } from 'react'

export default class Reviews extends Component {
    render() {
        const { title, subtitle, href, buttonText, img } = this.props
        return (
            <div className='reviews-area hidden-xs'>
                <div className='work-us'>
                <div className='work-left-text'>
                    <a href='#'><img src={'/frontend/img/about/' + img} alt='' /></a>
                </div>
                <div className='work-right-text text-center'>
                    <h2>{title}</h2>
                    <h5>{subtitle}</h5>
                    <a href={Helpers.fHref(href)} className='ready-btn'>{buttonText}</a>
                </div>
                </div>
            </div>
        );
    }
}
