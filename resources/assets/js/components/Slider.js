import React, { Component } from 'react'

import '../lib/nivo-slider/js/jquery.nivo.slider.js'

import { Helpers } from '../utils'

export default class Slider extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.sliders.length > 0) {
            setTimeout(() => {
                this.setState({
                    isLoading: false
                })

                this.$slider = $(this.refs.slider)
                this.$slider.nivoSlider({
                    effect: 'random',
                    slices: 15,
                    boxCols: 12,
                    boxRows: 8,
                    animSpeed: 500,
                    pauseTime: 5000,
                    startSlide: 0,
                    directionNav: true,
                    controlNavThumbs: false,
                    pauseOnHover: true,
                    manualAdvance: false,
                })
            }, 1)
        }
    }

    render() {
        const { sliders, home } = this.props

        return (
            <div id='home' className='slider-area'>
                <div className='bend niceties preview-2'>
                    <div ref='slider' id='ensign-nivoslider' className='slides'>
                        {this.state.isLoading && <div className='loading'>
                            &nbsp;
                        </div>}

                        {sliders.map((slider, index) =>
                            <img key={index} src={slider.img} alt='' style={{display: 'none'}} title={'#slider-direction-'+(index+1)} />
                        )}
                    </div>

                    {sliders.map((slider, index) =>
                        <SliderItem
                            key={index}
                            id={index+1}
                            title={slider.title}
                            subtitle={slider.subtitle}
                            href={home ? 'nav/' + slider.key : slider.key}
                            buttonText={slider.buttonText}
                        />
                    )}
                </div>
            </div>
        );
    }
}

class SliderItem extends Component {
    render() {
        const { id, title, subtitle, href, buttonText } = this.props
        return (
            <div id={'slider-direction-'+id} className='slider-direction'>
                <div className='container fit-center'>
                <div className='row'>
                    <div className='col-md-12 col-sm-12 col-xs-12'>
                    <div className='slider-content'>

                        <div className='layer-1-1 wow fadeInDown' data-wow-duration='2s' data-wow-delay='.2s'>
                        <h1 className='title1'>{title}</h1>
                        </div>

                        {subtitle &&
                            <div className='layer-1-2 wow fadeInUp' data-wow-duration='2s' data-wow-delay='.1s'>
                                <h2 className='title2'>{Helpers.nl2br(subtitle)}</h2>
                            </div>
                        }

                        <div className='layer-1-3 hidden-xs wow fadeInUp' data-wow-duration='2s' data-wow-delay='.2s'>
                        {/* <a className='ready-btn right-btn page-scroll' href='#services'>See Services</a> */}
                        {href != '#' &&<a className='ready-btn page-scroll' href={'#/'+href}>{buttonText || Helpers.localeText('learn_more')}</a>}
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        )
    }
}
