import React, { Component } from 'react'

export default class SectionHeadline extends Component {
    render() {
        return (
            <div className='row wow fadeInDown'>
                <div className='col-md-12 col-sm-12 col-xs-12'>
                    <div className='section-headline text-center'>
                        <h2>{this.props.title}</h2>
                    </div>
                </div>
            </div>
        );
    }
}
