import React, { Component } from 'react'
import { connect } from 'react-redux'
import Tilt from 'react-tilt'
import Palette from 'react-palette'

import '../lib/isotope/isotope.pkgd.min.js'

import SectionHeadline from './SectionHeadline'
import { fetchHomeProduct } from '../redux/actions/productActions';
import { Helpers } from '../utils';

@connect((store) => {
    return {
        products: store.product.products.parent,
        items: store.product.products.child,
    }
})
export default class Portfolio extends Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        this.props.dispatch(fetchHomeProduct())
    }

    componentDidUpdate() {
        $(window).on("load", function() {
            let $container = $('.awesome-project-content')
            $container.isotope({
                filter: '*',
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            })
        })
    }

    render() {
        const { products, items } = this.props

        return (
            <div id='portfolio' className='portfolio-area area-padding fix'>
                <div className='container'>
                <SectionHeadline title={Helpers.localeText('product_services')} />

                <div className='row'>
                    <PortfolioTab header={products}/>

                    <div ref='isotope' className='awesome-project-content'>

                        {items && items.map((item, index) =>
                            <PortfolioItem
                                key={index}
                                href={item.href}
                                filter={item.filter}
                                img={item.img}
                                icon={item.icon}
                                title={item.title}
                                subtitle={item.parent}
                                type='product' />
                        )}

                    </div>
                </div>
                </div>
            </div>
        );
    }
}

class PortfolioTab extends Component {
    onTabClicked(e) {
        let pro_menu_active = $('.project-menu li a.active')
        pro_menu_active.removeClass('active')

        $(e.target).addClass('active')
        let selector = $(e.target).attr('data-filter')

        let $container = $('.awesome-project-content')
        $container.isotope({
            filter: selector,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        })

        e.preventDefault()
        e.stopPropagation()
    }

    render() {
        const { header } = this.props

        return (
            <div className='fix'>
            <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div className='awesome-menu '>
                <ul className='project-menu'>
                    <li><a href='#' onClick={this.onTabClicked} className='active' data-filter='*'>{Helpers.localeText('all')}</a></li>
                    {header && header.map((item, index) =>
                        <li key={index}><a href='#' onClick={this.onTabClicked} className={item.active ? 'active' : ''} data-filter={'.'+item.filter}>{item.title}</a></li>
                    )}
                </ul>
                </div>
            </div>
            </div>
        );
    }
}

export class PortfolioItem extends Component {
    render() {
        const { filter, img, icon, title, subtitle, href, type } = this.props

        let image = (type == 'product') ? Helpers.imgUrlProduct(img) : img
        let icn = (type == 'product') ? Helpers.imgUrlMenu(icon) : icon
        let hideImg = (type == 'partner') ? 'hidden' : ''
        let link = (type == 'partner') ? null : Helpers.fHref('/#/nav/'+href)

        let pal = <Palette image={image}>
            {palette => (
                <a className='two-side-rounded-prod' href={link} style={{ background: palette.vibrant }}>
                <img className={'a-img ' + hideImg} src={image} alt='' />
                </a>
            )}
        </Palette>

        return (
            <div className={'col col-md-3 col-sm-3 col-xs-12 ' + filter}>
                <div className='single-awesome-project'>
                <Tilt className='awesome-img Tilt'>
                    {pal}
                    <div className='add-actions add-actions-2 text-center two-side-rounded-prod Tilt-inner'>
                        <div className='project-dec'>
                            <a className='venobox' data-gall='myGallery' href={link}>
                                <div className='center-div'>
                                    <img src={icn} alt='' />
                                    <h4>{title}</h4>
                                    <span>{subtitle}</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </Tilt>
                </div>
            </div>
        );
    }
}
