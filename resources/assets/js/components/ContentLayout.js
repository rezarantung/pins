import React, { Component } from 'react'
import { Helpers } from '../utils';

export default class ContentLayout extends Component {
    render() {
        const { content } = this.props

        const image1 = Helpers.imgUrlContent(content.image_1)
        const image2 = Helpers.imgUrlContent(content.image_2)
        const image3 = Helpers.imgUrlContent(content.image_3)

        switch(content.layout) {
            case 1: return <Layout1 text={content.text_1} />
            case 2: return <Layout2 image={image1} />
            case 3: return <Layout3 video={content.video_1} />
            case 4: return <Layout4 text={content.text_1} image={image1} />
            case 5: return <Layout5 text={content.text_1} image={image1} />
            case 6: return <Layout6 text={content.text_1} image={image1} />
            case 7: return <Layout7 text={content.text_1} image={image1} />
            case 8: return <Layout8 text1={content.text_1} text2={content.text_2} image={image1} />
            case 9: return <Layout9 text1={content.text_1} text2={content.text_1}
                image1={image1} image2={image1} />
            case 10: return <Layout10 text1={content.text_1} text2={content.text_2}
                image1={image1} image2={image2} />
            case 11: return <Layout11 text1={content.text_1} text2={content.text_2} text3={content.text_3}
                image1={image1} image2={image2} image3={image3} />
            case 12: return <Layout12 text={content.text_1} video={content.video_1} />
            case 13: return <Layout13 text={content.text_1} video={content.video_1} image={image1} />
            default: return <Layout1 text='' />
        }
    }
}

// Text Only
class Layout1 extends Component {
    render() {
        const { text } = this.props
        return (
            <div className='content-text' dangerouslySetInnerHTML={{__html: text }} />
        )
    }
}

// Image Only
class Layout2 extends Component {
    render() {
        const { image } = this.props
        return (
            <div className='entry-img-frame'>
                <img src={image} />
            </div>
        )
    }
}

// Video Only
class Layout3 extends Component {
    render() {
        const { video } = this.props

        return (
            <div style={{maxWidth:560, margin: 'auto', paddingBottom: 30}}>
                <div className='videoWrapper'>
                    <iframe width='560' height='315' src={Helpers.videoUrl(video)} frameBorder='0' allow='autoplay; encrypted-media' allowFullScreen/>
                </div>
            </div>
        )
    }
}

// Text With Image On Top
class Layout4 extends Component {
    render() {
        const { text, image } = this.props
        return (
            <div>
                <Layout2 image={image} />
                <Layout1 text={text} />
            </div>
        )
    }
}

// Text With Image On Bottom
class Layout5 extends Component {
    render() {
        const { text, image } = this.props
        return (
            <div>
                <Layout1 text={text} />
                <Layout2 image={image} />
            </div>
        )
    }
}

// Text With Image On Left
class Layout6 extends Component {
    render() {
        const { text, image } = this.props
        return (
            <div className='row'>
                <div className='col-md-6'>
                    <Layout2 image={image} />
                </div>
                <div className='col-md-6'>
                    <Layout1 text={text} />
                </div>
            </div>
        )
    }
}

// Text With Image On Right
class Layout7 extends Component {
    render() {
        const { text, image } = this.props
        return (
            <div className='row'>
                <div className='col-md-6'>
                    <Layout1 text={text} />
                </div>
                <div className='col-md-6'>
                    <Layout2 image={image} />
                </div>
            </div>
        )
    }
}

// Image Between Text
class Layout8 extends Component {
    render() {
        const { text1, text2, image } = this.props
        return (
            <div>
                <Layout1 text={text1} />
                <Layout2 image={image} />
                <Layout1 text={text2} />
            </div>
        )
    }
}

// 2 Image 2 Text
class Layout9 extends Component {
    render() {
        const { text1, text2, image1, image2 } = this.props
        return (
            <div>
                <Layout2 image={image1} />
                <Layout1 text={text1} />
                <Layout2 image={image2} />
                <Layout1 text={text2} />
            </div>
        )
    }
}

// Double Image+Text
class Layout10 extends Component {
    render() {
        const { text1, text2, image1, image2 } = this.props
        return (
            <div>
                <div className='row'>
                    <div className='col-md-6'>
                        <Layout1 text={text1} />
                    </div>
                    <div className='col-md-6'>
                        <Layout2 image={image1} />
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        <Layout2 image={image2} />
                    </div>
                    <div className='col-md-6'>
                        <Layout1 text={text2} />
                    </div>
                </div>
            </div>
        )
    }
}

// Tripple Image+Text
class Layout11 extends Component {
    render() {
        const { text1, text2, text3, image1, image2, image3 } = this.props
        return (
            <div>
                <div className='row'>
                    <div className='col-md-6'>
                        <Layout1 text={text1} />
                    </div>
                    <div className='col-md-6'>
                        <Layout2 image={image1} />
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        <Layout2 image={image2} />
                    </div>
                    <div className='col-md-6'>
                        <Layout1 text={text2} />
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        <Layout1 text={text3} />
                    </div>
                    <div className='col-md-6'>
                        <Layout2 image={image3} />
                    </div>
                </div>
            </div>
        )
    }
}

// Video With Text
class Layout12 extends Component {
    render() {
        const { text, video } = this.props
        return (
            <div>
                <Layout3 video={video} />
                <Layout1 text={text} />
            </div>
        )
    }
}

// Video+Text+Image
class Layout13 extends Component {
    render() {
        const { text, video, image } = this.props
        return (
            <div>
                <Layout3 video={video} />
                <Layout1 text={text} />
                <Layout2 image={image} />
            </div>
        )
    }
}
