import React, { Component } from 'react'
import { connect } from 'react-redux'

import NewsItem from './NewsItem'
import SectionHeadline from './SectionHeadline'
import { fetchNewsLatest } from '../redux/actions/newsActions';
import { Helpers } from '../utils';

@connect((store) => {
    return {
        news: store.news.news,
        fetched: store.news.fetched
    }
})
export default class LatestNews extends Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        this.props.dispatch(fetchNewsLatest())
    }

    render() {
        const { news, fetched } = this.props
        return (
            <div id='blog' className='blog-area'>
                <div className='blog-inner area-padding'>
                <div className='blog-overly'></div>
                <div className='container '>
                    <SectionHeadline title={Helpers.localeText('latest_news')} />
                    <div className='row'>

                        {fetched && news.map((nws,index) =>
                            <NewsItem
                                key={index}
                                wowDur='1s'
                                wowDelay={(index*0.3)+'s'}
                                href={'/#/news/'+nws.id}
                                img={nws.img}
                                comment={nws.comments}
                                createdAt={nws.date}
                                title={nws.title}
                                content={nws.content}
                            />
                        )}
                    </div>
                </div>
                </div>
            </div>
        );
    }
}
