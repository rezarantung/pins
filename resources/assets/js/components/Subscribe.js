import React, { Component } from 'react'

export default class Subscribe extends Component {
    render() {
        const { color, text, buttonHref, buttonText } = this.props

        return (
            <div className='suscribe-area' style={{backgroundColor: color}}>
                <div className='container'>
                <div className='row'>
                    <div className='col-lg-12 col-md-12 col-sm-12 col-xs=12'>
                    <div className='suscribe-text text-center'>
                        <h3>{text}</h3>
                        <a className='sus-btn' href={buttonHref}>{buttonText}</a>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        );
    }
}
