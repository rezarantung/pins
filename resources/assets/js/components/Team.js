import React, { Component } from 'react'
import { connect } from 'react-redux'
import Tilt from 'react-tilt'

import SectionHeadline from './SectionHeadline'
import { Helpers } from '../utils';
import { selectTeam } from '../redux/actions/teamActions';

export default class Team extends Component {
    render() {
        const { title, teams } = this.props

        return (
            <div id='team' className='our-team-area'>
                <SectionHeadline title={title} />

                <div className='awesome-menu'>
                    <ul className='nav nav-pills mb-3 project-menu' id='pills-tab' role='tablist'>
                        <li className='nav-item' style={{float: 'none'}}>
                            <a className='nav-link active' id='commissioner-tab' data-toggle='pill' href='#pills-commissioner' role='tab' aria-controls='pills-commissioner' aria-selected='true'>{Helpers.localeText('commissioner')}</a>
                        </li>
                        <li className='nav-item' style={{float: 'none'}}>
                            <a className='nav-link' id='pills-director-tab' data-toggle='pill' href='#pills-director' role='tab' aria-controls='pills-director' aria-selected='false'>{Helpers.localeText('director')}</a>
                        </li>
                    </ul>
                </div>

                <div className='tab-content' id="pills-tabContent" style={{padding:0}}>
                    <div className='entry-content tab-pane show active' id='pills-commissioner' role='tabpanel' aria-labelledby='pills-commissioner-tab'>
                        <div className='team-top row'>
                            {teams.map((team, index) => {
                                if (team.group == 1) return <TeamItem key={index} name={team.name} level={team.title} img={Helpers.imgUrlTeam(team.photo)} details={team.details}/>
                            })}
                        </div>
                    </div>
                    <div className='entry-content tab-pane' id='pills-director' role='tabpanel' aria-labelledby='pills-director-tab'>
                        <div className='team-top row'>
                            {teams.map((team, index) => {
                                if (team.group == 2) return <TeamItem key={index} name={team.name} level={team.title} img={Helpers.imgUrlTeam(team.photo)} details={team.details}/>
                            })}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

@connect((store) => {
    return {
        selectedTeam: store.team.selectedTeam,
    }
})
class TeamItem extends Component {
    render() {
        const {name, level, img, details} = this.props

        return (
            <div className='col-md-3 col-sm-3 col-xs-12'>
                <Tilt className='awesome-img Tilt'>
                    <div className='single-team-member'>
                        <a href='#' onClick={(e)=>{
                            e.preventDefault()
                            e.stopPropagation()

                            this.props.dispatch(selectTeam(name, level, details))
                        }}>
                            <div className='team-img Tilt-inner'>
                                <img src={img} alt=''/>
                            </div>
                            <div className='team-content text-center Tilt-inner'>
                                <h4>{name}</h4>
                                <p>{level}</p>
                            </div>
                        </a>
                    </div>
                </Tilt>
            </div>
        );
    }
}
