import React, { Component } from 'react'
import Moment from 'moment'

import { Helpers } from '../utils'

export default class Subscribe extends Component {
    render() {
        const { threaded, userImg, author, date, comment } = this.props
        const dateText = Moment(date).fromNow()

        return (
            <li className={threaded ? 'threaded-comments' : ''}>
                <div className='comments-details'>
                <div className='comments-list-img'>
                    <img src={userImg || '/frontend/img/blog/b02.jpg'} alt='post-author'/>
                </div>
                <div className='comments-content-wrap'>
                    <span>
                        <b><a href='#'>{author}</a></b>
                        <span className='post-time'>{dateText}</span>
                    </span>
                    <div>{comment && Helpers.nl2p(comment)}</div>
                </div>
                </div>
            </li>
        );
    }
}
