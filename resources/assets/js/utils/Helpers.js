import React from 'react'
import { Constants } from './index'
import { Locale } from './Locale';

export const Helpers = {
    removeHash(data) {
      return data.replace('#', '')
    },

    nl2br(inp) {
        return inp.split("\\n").map((item, key) => {
            return <span key={key}>{item}<br/></span>
        })
    },

    nl2p(inp) {
        return inp.split("\\n").map((item, key) => {
            return <p key={key}>{item}</p>
        })
    },

    isEmptyString(str) {
        return (!str || str.length === 0 || !str.trim())
    },

    getImageCover(img) {
        return !Helpers.isEmptyString(img) ? Helpers.imgUrlContent(img) : Constants.IMG_COVER_DEFAULT
    },

    localeUrl(link) {
        const lang = localStorage.getItem('language') ? localStorage.getItem('language') : 'id'
        return link + '?locale=' + lang
    },

    localeText(id) {
        const lang = localStorage.getItem('language') ? localStorage.getItem('language') : 'id'
        return Locale[lang][id]
    },

    getLocale() {
        return localStorage.getItem('language') ? localStorage.getItem('language') : 'id'
    },

    videoUrl(link) {
        let id = (link.split(Constants.YOUTUBE_BASE_WATCH_URL)[1]) || ''
        return Constants.YOUTUBE_BASE_EMBED_URL + '/' + id
    },

    fHref(link) {
        const sepa = link.charAt(0) == '/' ? '' : '/'
        return Constants.BASE_URL + sepa + link
    },

    imgUrlMenu(link) {
        return Constants.DIR_MENU_URL + '/' + link
    },

    imgUrlNews(link) {
        return Constants.DIR_NEWS_URL + '/' + link
    },

    imgUrlProduct(link) {
        return Constants.DIR_PRODUCT_URL + '/' + link
    },

    imgUrlContent(link) {
        return Constants.DIR_CONTENT_URL + '/' + link
    },

    imgUrlCompany(link) {
        return Constants.DIR_COMPANY_URL + '/' + link
    },

    imgUrlPartner(link) {
        return Constants.DIR_PARTNER_URL + '/' + link
    },

    imgUrlSocMed(link) {
        return Constants.DIR_SOCIAL_MEDIA_URL + '/' + link
    },

    imgUrlTeam(link) {
        return Constants.DIR_TEAM_URL + '/' + link
    },

    guidGenerator() {
        var S4 = function() {
           return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        };
        return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
    },
  }
