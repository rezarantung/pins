import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Helpers } from '../utils'

import ContentLayout from '../components/ContentLayout'
import ImageHeadline from '../components/ImageHeadline'
import SectionHeadline from '../components/SectionHeadline'
import { TabItem, TabContent } from '../components/Tabs'

import { setActiveMenu } from '../redux/actions/menuActions'
import { PortfolioItem } from '../components/Portfolio'

@connect((store) => {
    return {
        active: store.menu.active,
        menuDetail: store.menu.menuDetail,
        fetched: store.menu.mdFetched
    }
})
export default class ContentProduct extends Component {
    componentDidMount() {
        this.props.dispatch(setActiveMenu(this.props.href))
    }

    render() {
        const {fetched, menuDetail} = this.props

        let comp = <div id='preloader'></div>

        if (fetched) {
            const { title, image, featured, overviewContent, whyUsContent, productContent, casedContent, partner } = menuDetail.product

            let tab = <ProductTab overviewContent={overviewContent} whyUsContent={whyUsContent} productContent={productContent} casedContent={casedContent} />

            if (featured) tab = <FeaturedTab overviewContent={overviewContent} child={menuDetail.child} partner={partner} />

            comp =
                <div>
                    <ImageHeadline title='' img={Helpers.imgUrlProduct(image)} />
                    <div className='container'>
                        <div className='row'>
                        <article className='blog-post-wrapper soft-shadow wow fadeInUp'>
                            <div className='post-information'>
                                <SectionHeadline title={title}/>
                                {tab}
                            </div>

                        </article>

                        </div>
                    </div>
                </div>
        }

        return comp
    }
}

class FeaturedTab extends Component {
    render() {
        const { child, partner, overviewContent } = this.props

        let tabClass = overviewContent ? 'tab-pane' : '';

        return (
            <div>

            {overviewContent &&
            <div className='awesome-menu'>
                <ul className='nav nav-pills mb-3 project-menu' id='pills-tab' role='tablist'>
                    <li className='nav-item' style={{float: 'none'}}>
                        <a className='nav-link active' id='overview-tab' data-toggle='pill' href='#pills-overview' role='tab' aria-controls='pills-overview' aria-selected='true'>Overview</a>
                    </li>
                    <li className='nav-item' style={{float: 'none'}}>
                        <a className='nav-link' id='pills-product-tab' data-toggle='pill' href='#pills-product' role='tab' aria-controls='pills-product' aria-selected='false'>{Helpers.localeText('product')}</a>
                    </li>
                    <li className='nav-item' style={{float: 'none'}}>
                        <a className='nav-link' id='pills-partner-tab' data-toggle='pill' href='#pills-partner' role='tab' aria-controls='pills-partner' aria-selected='false'>{Helpers.localeText('partner')}</a>
                    </li>
                </ul>
            </div>
            }

            <div className='tab-content' id="pills-tabContent" style={{padding:0}}>

                {overviewContent &&
                    <div className='entry-content tab-pane show active' id='pills-overview' role='tabpanel' aria-labelledby='pills-overview-tab'>
                        <h2>{Helpers.localeText('overview')}</h2>
                        <ContentLayout content={overviewContent} />
                    </div>
                }

                <div className={'entry-content ' + tabClass} id='pills-product' role='tabpanel' aria-labelledby='pills-product-tab'>
                    { child && child.length > 0 && <h2>{Helpers.localeText('product')}</h2> }
                    { (!child || child.length == 0) && <h2>{Helpers.localeText('no_product')}</h2> }
                    <div className='team-top row'>
                    <div ref='isotope' className='awesome-project-content'>

                        {child && child.map((item, index) =>
                            <PortfolioItem
                                key={index}
                                href={item.href}
                                filter={item.filter}
                                img={item.img}
                                icon={item.icon}
                                title={item.title}
                                subtitle={item.parent}
                                type='product' />
                        )}

                        </div>
                    </div>
                </div>

                {partner &&
                <div className='entry-content tab-pane' id='pills-partner' role='tabpanel' aria-labelledby='pills-partner-tab'>
                    <h2>{Helpers.localeText('partner')}</h2>
                    <div className='team-top row'>
                    <div ref='isotope' className='awesome-project-content'>

                        {partner && partner.map((item, index) =>
                            <PortfolioItem
                                key={index}
                                href={''}
                                filter={''}
                                img={Helpers.imgUrlPartner(item.img)}
                                icon={Helpers.imgUrlPartner(item.img)}
                                title={item.title}
                                subtitle={''}
                                type='partner' />
                        )}

                        </div>
                    </div>
                </div>}

            </div>
            </div>
        )
    }
}

class ProductTab extends Component {
    render() {
        const { overviewContent, whyUsContent, productContent, casedContent } = this.props

        return (
            <div>
                <div className='awesome-menu'>
                    <ul className='nav nav-pills mb-3 project-menu' id='pills-tab' role='tablist'>
                        {overviewContent &&
                            <TabItem tabkey='overview' active={true} title={Helpers.localeText('overview')} />}
                        {whyUsContent &&
                            <TabItem tabkey='why-us' active={false} title={Helpers.localeText('why_us')} />}
                        {productContent &&
                            <TabItem tabkey='product' active={false} title={Helpers.localeText('product')} />}
                        {casedContent &&
                            <TabItem tabkey='cased' active={false} title={Helpers.localeText('cased')} />}
                    </ul>
                </div>
                <div className='tab-content' id="pills-tabContent">
                    {overviewContent &&
                        <TabContent tabkey={'overview'} active={true} title={Helpers.localeText('overview')}>
                            <ContentLayout content={overviewContent} />
                        </TabContent>
                    }
                    {whyUsContent &&
                        <TabContent tabkey={'why-us'} active={false} title={Helpers.localeText('why_us')}>
                            <ContentLayout content={whyUsContent} />
                        </TabContent>
                    }
                    {productContent &&
                        <TabContent tabkey={'product'} active={false} title={Helpers.localeText('product')}>
                            <ContentLayout content={productContent} />
                        </TabContent>
                    }
                    {casedContent &&
                        <TabContent tabkey={'cased'} active={false} title={Helpers.localeText('cased')}>
                            <ContentLayout content={casedContent} />
                        </TabContent>
                    }
                </div>

            </div>
        )
    }
}
