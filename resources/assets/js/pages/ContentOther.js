import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Helpers } from '../utils';

import ContentLayout from '../components/ContentLayout';
import ImageHeadline from '../components/ImageHeadline'
import SectionHeadline from '../components/SectionHeadline';

import { setActiveMenu } from '../redux/actions/menuActions'

@connect((store) => {
    return {
        active: store.menu.active,
        menuDetail: store.menu.menuDetail,
        fetched: store.menu.mdFetched
    }
})
export default class ContentOther extends Component {
    componentDidMount() {
        this.props.dispatch(setActiveMenu(this.props.href))
    }

    render() {
        const {fetched, menuDetail} = this.props

        let comp = <div id='preloader'></div>

        if (fetched) {
            const { title, content } = menuDetail

            if (content) {
                comp =
                    <div>
                        <ImageHeadline title='' img={Helpers.getImageCover(content.image_1)} />
                        <div className='container'>
                            <div className='row'>
                            <article className='blog-post-wrapper soft-shadow wow fadeInUp'>
                                <div className='post-information'>
                                    <SectionHeadline title={title}/>
                                    <div className='entry-content'>
                                        <ContentLayout content={content} />
                                    </div>
                                </div>

                            </article>

                            </div>
                        </div>
                    </div>
            }
        }

        return comp
    }
}
