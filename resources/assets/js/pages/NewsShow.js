import React, { Component } from 'react'
import { connect } from 'react-redux'
import Moment from 'react-moment'
import { FacebookShareButton, FacebookIcon, GooglePlusShareButton, GooglePlusIcon, TwitterShareButton, TwitterIcon } from 'react-share';

import { Helpers, Constants } from '../utils/index'

import CommentItem from '../components/CommentItem'
import ImageHeadline from '../components/ImageHeadline'
import SectionHeadline from '../components/SectionHeadline';

import { setActiveMenu } from '../redux/actions/menuActions'
import { fetchSingleNews } from '../redux/actions/newsActions';
import { fetchComment, addComment } from '../redux/actions/commentActions';
import Metadata from '../components/Metadata';
import Analytic from '../components/Analytic';

@connect((store) => {
    return {
        active: store.menu.active,
        news: store.news.sNews,
        fetched: store.news.snFetched,
        listComments: store.comment.comment,
        cmFetched: store.comment.fetched,
    }
})
export default class NewsShow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            author: '',
            email: '',
            comment: ''
        };

        this.authorHandleChange = this.authorHandleChange.bind(this)
        this.emailHandleChange = this.emailHandleChange.bind(this)
        this.commentHandleChange = this.commentHandleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentWillMount() {
        this.props.dispatch(fetchSingleNews(this.props.match.params.id))
        this.props.dispatch(fetchComment(this.props.match.params.id))
    }

    componentDidMount() {
        this.props.dispatch(setActiveMenu('news'))
    }

    authorHandleChange(event) {
        this.setState({author: event.target.value})
    }

    emailHandleChange(event) {
        this.setState({email: event.target.value})
    }

    commentHandleChange(event) {
        this.setState({comment: event.target.value})
    }

    handleSubmit(event) {
        this.props.dispatch(
            addComment(this.state.author,
                this.state.comment,
                this.state.email,
                this.props.match.params.id))

        this.props.dispatch(fetchComment(this.props.match.params.id))
        this.setState({
            author: '',
            email: '',
            comment: ''
        })

        event.preventDefault()
    }

    render() {
        const {news, fetched, listComments, cmFetched} = this.props

        let comp = <div id="preloader"></div>
        if (fetched) {
            const { id, title, content, allContent, img, comments, date } = news
            let commentText = comments + ' comment' + (comments && comments > 1 ? 's' : '')
            if (Helpers.getLocale() == 'id') {
                commentText = comments + ' komentar'
            }

            // const dateText = Moment(date).fromNow()
            const url = Constants.BASE_URL + '/n/' + id + '/#' + this.props.location.pathname

            let commentComp = <li>There is no comment</li>

            if (cmFetched) {
                commentComp = listComments.map((com, id) => {
                    return <CommentItem
                        key={id}
                        userImg='/frontend/img/blog/b02.jpg'
                        author={com.author}
                        date={com.date}
                        comment={com.comment} />
                })
            }

            comp =
                <div>
                    <Analytic location={this.props.location} />
                    <Metadata title={title} desc={jQuery(content).text()} img={Helpers.imgUrlNews(img)} url={url} />

                    <ImageHeadline title={''} img={Helpers.imgUrlNews(img)} />
                    <div className='container'>
                        <div className='row'>
                        <article className='blog-post-wrapper soft-shadow wow fadeInUp'>
                            <div className='post-information'>
                                <SectionHeadline title={title}/>
                                <div className='entry-meta'>
                                    <span className='author-meta'><i className='fa fa-user'></i> <a>admin</a></span>
                                    <span><i className='fa fa-clock-o'></i>
                                    <Moment locale={Helpers.getLocale()} fromNow>{date}</Moment>
                                    </span>
                                    <span><i className='fa fa-comments-o'></i> <a>{commentText}</a></span>
                                    <div className='share'>
                                        <FacebookShareButton url={url} >
                                            <FacebookIcon size={32} round />
                                        </FacebookShareButton>

                                        <GooglePlusShareButton url={url} >
                                            <GooglePlusIcon size={32} round />
                                        </GooglePlusShareButton>

                                        <TwitterShareButton url={url} >
                                            <TwitterIcon size={32} round />
                                        </TwitterShareButton>
                                    </div>
                                </div>
                                <div className='entry-img-frame'>
                                    <img className='two-side-rounded-2' src={Helpers.imgUrlNews(img)} />
                                </div>
                                <div className='entry-content' dangerouslySetInnerHTML={{__html: allContent}}>
                                </div>
                            </div>

                            <div className='single-post-comments'>
                                <div className='comments-area'>
                                <div className='comments-heading'>
                                    <h3>{commentText}</h3>
                                </div>
                                <div className='comments-list'>
                                    <ul>
                                        {commentComp}
                                    </ul>
                                </div>
                                </div>
                                <div className='comment-respond'>
                                <h3 className='comment-reply-title'>{Helpers.localeText('leave_a_comment')} </h3>
                                <span className='email-notes'>{Helpers.localeText('email_not_published_info')}</span>
                                <form onSubmit={this.handleSubmit}>
                                    <div className='row'>
                                        <div className='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                                            <p>{Helpers.localeText('name')}</p>
                                            <input type='text' value={this.state.author} onChange={this.authorHandleChange}/>
                                        </div>
                                        <div className='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                                            <p>{Helpers.localeText('email')}</p>
                                            <input type='email' value={this.state.email} onChange={this.emailHandleChange}/>
                                        </div>
                                    </div>
                                    <div className='row'>
                                        <div className='col-lg-12 col-md-12 col-sm-12 comment-form-comment'>
                                            <p>{Helpers.localeText('comment')}</p>
                                            <textarea id='message-box' cols='30' rows='10' value={this.state.comment} onChange={this.commentHandleChange}/>
                                            <input type='submit' value={Helpers.localeText('post_comment')} />
                                        </div>
                                    </div>
                                </form>
                                </div>

                            </div>

                        </article>

                        </div>
                    </div>
                </div>
        }

        return comp
    }
}
