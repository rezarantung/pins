import React, { Component } from 'react'
import { connect } from 'react-redux'

import ImageHeadline from '../components/ImageHeadline'
import Team from '../components/Team';

import { setActiveMenu } from '../redux/actions/menuActions'
import { Helpers } from '../utils';

@connect((store) => {
    return {
        active: store.menu.active,
        menuDetail: store.menu.menuDetail,
        fetched: store.menu.mdFetched,
        selectedTeam: store.team.selectedTeam,
    }
})
export default class ContentTeam extends Component {
    componentDidMount() {
        this.props.dispatch(setActiveMenu(this.props.href))
    }

    render() {
        const {fetched, menuDetail, selectedTeam } = this.props

        let comp = <div id='preloader'></div>

        if (fetched) {
            const { teams, title } = menuDetail

            if (teams) {
                comp =
                    <div>
                        <ImageHeadline title='' img={Helpers.fHref('/img/frontend/default_cover_image.jpg')} />
                        <div className='container'>
                            <div className='row'>
                            <article className='blog-post-wrapper soft-shadow wow fadeInUp'>
                                <div className='post-information'>
                                    <Team teams={teams} title={title} />
                                    <div className='entry-content team-content'>
                                        <h2>{selectedTeam.name}</h2>
                                        <h3>{selectedTeam.title}</h3>
                                        <p>{selectedTeam.details}</p>
                                    </div>
                                </div>
                            </article>

                            </div>
                        </div>
                    </div>
            }
        }

        return comp
    }
}
