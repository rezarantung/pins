import React, { Component } from 'react'
import { connect } from 'react-redux'

import ImageHeadline from '../components/ImageHeadline'
import SectionHeadline from '../components/SectionHeadline'
import { PortfolioItem } from '../components/Portfolio'
import { TabItem, TabContent } from '../components/Tabs'
// import TabItem

import { setActiveMenu } from '../redux/actions/menuActions'
import { Helpers } from '../utils';

@connect((store) => {
    return {
        active: store.menu.active,
        menuDetail: store.menu.menuDetail,
        fetched: store.menu.mdFetched,
        selectedTeam: store.team.selectedTeam,
        teamSelected: store.team.selected,
    }
})
export default class ContentPartner extends Component {
    componentDidMount() {
        this.props.dispatch(setActiveMenu(this.props.href))
    }

    render() {
        const {fetched, menuDetail } = this.props

        let comp = <div id='preloader'></div>

        if (fetched) {
            const { partner, title } = menuDetail

            if (partner) {
                comp =
                    <div>
                        <ImageHeadline title='' img={Helpers.fHref('/img/frontend/default_cover_image.jpg')} />
                        <div className='container'>
                            <div className='row'>
                            <article className='blog-post-wrapper soft-shadow wow fadeInUp'>
                                <div className='post-information'>
                                    <SectionHeadline title={title} />
                                    <div className='awesome-menu'>
                                        <ul className='nav nav-pills mb-3 project-menu' id='pills-tab' role='tablist'>

                                            {partner.map((item, index) =>
                                                <TabItem key={index} tabkey={item.key} active={index == 0} title={item.title} letterSpacing={2}/>
                                            )}
                                        </ul>
                                    </div>

                                    <div className='tab-content' id="pills-tabContent" style={{padding:0}}>
                                        {partner.map((item, index) =>
                                            <ContentItem key={index} tabkey={item.key} active={index == 0} title={item.title} child={item.child}/>
                                        )}
                                    </div>

                                </div>
                            </article>

                            </div>
                        </div>
                    </div>
            }
        }

        return comp
    }
}

class ContentItem extends Component {
    render() {
        const { active, tabkey, title, child } = this.props

        return (
            <TabContent tabkey={tabkey} active={active} title={title}>
                <div className='team-top row'>
                <div ref='isotope' className='awesome-project-content'>
                    {child && child.map((item, index) =>
                        <PortfolioItem
                            key={index}
                            href={''}
                            filter={''}
                            img={Helpers.imgUrlPartner(item.img)}
                            icon={Helpers.imgUrlPartner(item.img)}
                            title={item.title}
                            subtitle={''}
                            type='partner' />
                        )}
                </div>
                </div>
            </TabContent>
        )
    }
}
