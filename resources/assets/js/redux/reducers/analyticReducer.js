export default function reducer(state={
    analytic: {},
    added: false,
    error: null,
}, action) {
    switch (action.type) {
        case 'ADD_ANALYTIC_REJECTED': {
            return {...state, error: action.payload }
        }
        case 'ADD_ANALYTIC_FULFILLED': {
            return {...state, added: true, enquiry: action.payload}
        }
    }

    return state
}
