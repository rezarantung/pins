import { combineReducers } from 'redux'

import menu from './menuReducer'
import footer from './footerReducer'
import slider from './sliderReducer'
import product from './productReducer'
import news from './newsReducer'
import comment from './commentReducer'
import team from './teamReducer'
import language from './languageReducer'
import enquiry from './enquiryReducer'
import analytic from './analyticReducer'
import area from './areaReducer'

export default combineReducers ({menu, footer, product, slider, news, comment, team, language, enquiry, analytic, area})
