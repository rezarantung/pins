export default function reducer(state={
    area: {},
    fetching: false,
    fetched: false,
    error: null
}, action) {
    switch (action.type) {
        case 'FETCH_AREA': {
            return {...state, fetching: true}
        }
        case 'FETCH_AREA_REJECTED': {
            return {...state, fetching: false, error: action.payload}
        }
        case 'FETCH_AREA_FULFILLED': {
            return {...state, fetching: false, fetched: true, area: action.payload}
        }
    }

    return state
}
