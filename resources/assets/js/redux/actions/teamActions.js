export function selectTeam(name, title, details) {
    return (dispatch) => {
        dispatch({ type: 'SELECT_TEAM', payload: {
            name: name,
            title: title,
            details: details,
        } })
    }
}
