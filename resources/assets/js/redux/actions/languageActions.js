export function selectLanguage(lang) {
    return (dispatch) => {
        dispatch({ type: 'SELECT_LANGUAGE', payload: lang })
    }
}
