import Axios from 'axios'
import { Constants, Helpers } from '../../utils';

export function fetchComment(id) {
    return (dispatch) => {
        Axios.get(Helpers.localeUrl(Constants.API_COMMENTS_URL+'/'+id))
        .then(res => {
            dispatch({ type: 'FETCH_COMMENT_FULFILLED', payload: res.data })
        })
        .catch(err => {
            dispatch({ type: 'FETCH_COMMENT_REJECTED', payload: err })
        })
    }
}

export function addComment(author, comment, email, articleId) {
    return (dispatch) => {
        Axios.post(Constants.API_COMMENTS_URL, {
            author: author,
            comment: comment,
            email: email,
            article_id: articleId
        })
        .then(res => {
            dispatch({ type: 'ADD_COMMENT_FULFILLED', payload: res.data })
        })
        .catch(err => {
            dispatch({ type: 'ADD_COMMENT_REJECTED', payload: err })
        })
    }
}
