import Axios from 'axios'
import { Constants, Helpers } from '../../utils';

export function fetchMenu() {
    return (dispatch) => {
        Axios.get(Helpers.localeUrl(Constants.API_MENU_URL))
        .then(res => {
            dispatch({ type: 'FETCH_MENU_FULFILLED', payload: res.data })
        })
        .catch(err => {
            dispatch({ type: 'FETCH_MENU_REJECTED', payload: err })
        })
    }
}

export function fetchMenuDetails(link) {
    return (dispatch) => {
        dispatch({ type: 'FETCH_MENU_DETAIL' })
        Axios.get(Helpers.localeUrl(Constants.API_MENU_URL + '/' + link))
        .then(res => {
            dispatch({ type: 'FETCH_MENU_DETAIL_FULFILLED', payload: res.data })
        })
        .catch(err => {
            dispatch({ type: 'FETCH_MENU_DETAIL_REJECTED', payload: err })
        })
    }
}

export function setActiveMenu(active) {
    return (dispatch) => {
        dispatch({ type: 'SET_MENU_ACTIVE', payload: active})
    }
}

export function setMenuStick(state) {
    return (dispatch) => {
        dispatch({ type: 'SET_MENU_STICKED', payload: state})
    }
}
