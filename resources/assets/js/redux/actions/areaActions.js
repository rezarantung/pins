import Axios from 'axios'
import { Constants, Helpers } from '../../utils';

export function fetchArea() {
    return (dispatch) => {
        Axios.get(Helpers.localeUrl(Constants.API_AREA_URL))
        .then(res => {
            dispatch({ type: 'FETCH_AREA_FULFILLED', payload: res.data })
        })
        .catch(err => {
            dispatch({ type: 'FETCH_AREA_REJECTED', payload: err })
        })
    }
}
