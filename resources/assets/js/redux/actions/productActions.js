import Axios from 'axios'
import { Constants, Helpers } from '../../utils';

export function fetchHomeProduct() {
    return (dispatch) => {
        Axios.get(Helpers.localeUrl(Constants.API_PRODUCTS_URL))
        .then(res => {
            dispatch({ type: 'FETCH_PRODUCT_FULFILLED', payload: res.data })
        })
        .catch(err => {
            dispatch({ type: 'FETCH_PRODUCT_REJECTED', payload: err })
        })
    }
}
