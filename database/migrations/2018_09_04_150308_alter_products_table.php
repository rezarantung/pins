<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductsTable extends Migration
{
    public function up()
    {
        Schema::table('products', function (Blueprint $table) 
        {
            $table->dropColumn('content');
            
            $table->integer('overview_id')->unsigned()->nullable();
            $table->integer('why_us_id')->unsigned()->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('cased_id')->unsigned()->nullable();

            
            $table->foreign('overview_id')->references('id')->on('contents');

            $table->foreign('why_us_id')->references('id')->on('contents');
            
            $table->foreign('product_id')->references('id')->on('contents');
            
            $table->foreign('cased_id')->references('id')->on('contents');
            
            $table->string('excerpt');
        });
    }

    public function down()
    {
        //
    }
}
