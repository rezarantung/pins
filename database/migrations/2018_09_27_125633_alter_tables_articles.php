<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablesArticles extends Migration
{
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->renameColumn('title', 'title_id');
            $table->renameColumn('description_short', 'description_short_id');
            $table->renameColumn('description_long', 'description_long_id');
            $table->text('title_en');
            $table->text('description_short_en');
            $table->text('description_long_en');
        });
    }

    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->renameColumn('title');
            $table->renameColumn('description_short');
            $table->renameColumn('description_long');
            $table->dropColumn('title_en');
            $table->dropColumn('description_short_en');
            $table->dropColumn('description_long_en');
        });
    }
}
