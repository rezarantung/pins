<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCompaniesTable extends Migration
{
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('vission')->nullable()->change();
            $table->string('mission')->nullable()->change();
        });
    }

    public function down()
    {
        //
    }
}
