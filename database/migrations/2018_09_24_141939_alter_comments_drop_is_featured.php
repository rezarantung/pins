<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCommentsDropIsFeatured extends Migration
{
    public function up()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->dropColumn('is_featured');
        });
    }

    public function down()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->boolean('is_featured');
        });
    }
}
