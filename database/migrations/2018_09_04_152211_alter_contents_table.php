<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContentsTable extends Migration
{
    public function up()
    {
        Schema::table('contents', function (Blueprint $table) {
            $table->integer('layout');
            $table->text('text_1_id');
            $table->text('text_2_id');
            $table->text('text_3_id');
            $table->text('text_4_id');
            $table->text('text_1_en');
            $table->text('text_2_en');
            $table->text('text_3_en');
            $table->text('text_4_en');
            $table->string('image_1');
            $table->string('image_2');
            $table->string('image_3');
            $table->string('image_4');
            $table->string('video_1');
            $table->string('video_2');
        });
    }

    public function down()
    {
        //
    }
}
