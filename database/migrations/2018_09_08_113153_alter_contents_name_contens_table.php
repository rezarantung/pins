<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContentsNameContensTable extends Migration
{

    public function up()
    {
        Schema::table('contents', function (Blueprint $table) {
            $table->dropColumn('name');
        });

        Schema::table('contents', function (Blueprint $table) {
            $table->string('name');
        });
    }

    public function down()
    {
        //
    }
}
