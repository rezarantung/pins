# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.19)
# Database: pins_db
# Generation Time: 2018-10-08 15:35:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table analytics
# ------------------------------------------------------------

DROP TABLE IF EXISTS `analytics`;

CREATE TABLE `analytics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `useragent` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table areas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `areas`;

CREATE TABLE `areas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'false',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `areas` WRITE;
/*!40000 ALTER TABLE `areas` DISABLE KEYS */;

INSERT INTO `areas` (`id`, `name`, `location`, `created_at`, `updated_at`, `deleted_at`, `email`, `telp`, `fax`, `featured`)
VALUES
	(1,'NOC PINS','Jl. Medan Merdeka Selatan (Monas) Jakarta Selatan','2018-10-03 07:07:27','2018-10-03 07:07:27',NULL,'helpdesk@pins.co.id','021348444999','02134831121','true'),
	(2,'MEDAN (Area Sumatera)','Graha Merah Putih Lt 6 Jl.Putri Hijau No 1, Medan 20111','2018-10-03 07:07:27','2018-10-03 07:07:27',NULL,NULL,NULL,NULL,'false'),
	(3,'JAKARTA (Area Jabodetabek)','Jl. Medan Merdeka Selatan (Monas) Jakarta Pusat','2018-10-03 07:07:27','2018-10-03 07:07:27',NULL,NULL,NULL,NULL,'false'),
	(4,'BANDUNG (Area Jawa Barat)','Jl. Supratman No 62, Bandung','2018-10-03 07:07:27','2018-10-03 07:07:27',NULL,NULL,NULL,NULL,'false'),
	(5,'SEMARANG (Area Jawa Tengah)','Jl. Pahlawan No 10, Semarang','2018-10-03 07:07:27','2018-10-03 07:07:27',NULL,NULL,NULL,NULL,'false'),
	(6,'DENPASAR (Area Bali & Nusa Tenggara)','Graha Merah Putih Lt 6 Jl. Teuku Umar No 6, Denpasar','2018-10-03 07:07:27','2018-10-03 07:07:27',NULL,NULL,NULL,NULL,'false'),
	(7,'MAKASSAR (Area Sulawesi, Maluku & Papua)','Jl. A.P Pettarani No 2, Makassar','2018-10-03 07:07:27','2018-10-03 07:07:27',NULL,NULL,NULL,NULL,'false'),
	(8,'BALIKPAPAN (Area Kalimantan)','Jl. MT Haryono No 169, Balikpapan','2018-10-03 07:07:27','2018-10-03 07:07:27',NULL,NULL,NULL,NULL,'false'),
	(9,'SURABAYA (Area Jawa Timur)','Jl. Sumatra No 131, Surabaya','2018-10-03 07:07:27','2018-10-03 07:07:27',NULL,NULL,NULL,NULL,'false');

/*!40000 ALTER TABLE `areas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table articles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_short_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_long_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `is_featured` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'false',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `title_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_short_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_long_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `articles_user_id_foreign` (`user_id`),
  CONSTRAINT `articles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;

INSERT INTO `articles` (`id`, `title_id`, `description_short_id`, `description_long_id`, `image`, `video`, `user_id`, `is_featured`, `created_at`, `updated_at`, `deleted_at`, `title_en`, `description_short_en`, `description_long_en`)
VALUES
	(1,'PINS Rayakan Semarak Kemerdekaan','<div>Masih dengan suasana kemerdekaan dalam memperingati HUT RI ke-73, PT PINS Indonesia menggelar acara kemerdekan yang bertema Semarak Kemerdekaan dan bertempat di kantor PINS Telkom Landmark Tower Lt.42, Jakarta pada 23 Agustus 2018. Acara ini dihadiri oleh para jajaran Direksi PINS dan seluruh karyawan PINS domisili Jakarta. Acara berlangsung dengan sangat meriah dan semua karyawan sangat antusian disepanjang acara.</div>','<div>Masih dengan suasana kemerdekaan dalam memperingati HUT RI ke-73, PT PINS Indonesia menggelar acara kemerdekan yang bertema Semarak Kemerdekaan dan bertempat di kantor PINS Telkom Landmark Tower Lt.42, Jakarta pada 23 Agustus 2018. Acara ini dihadiri oleh para jajaran Direksi PINS dan seluruh karyawan PINS domisili Jakarta. Acara berlangsung dengan sangat meriah dan semua karyawan sangat antusian disepanjang acara.</div><div><br></div><div>Acara Semarak Kemerdekaan ini dibuka dengan kegiatan Leader Talks – Sharing Session by CEO PT PINS Indonesia Mohammad Firdaus, dalam sharing session, Firdaus mengatakan bahwa lebih dari 50% karyawan PINS adalah Gen Y, yang mana Gen Y ini memiliki semangat tinggi, achievement oriented, kreatif dan inovatif dalam berfikir. ”Dominasi Gen Y ini memiliki peranan yang sangat penting dalam mensupport PINS untuk mencapai target perusahaan dan memenuhi visi misi PINS yaitu To Become The Leading of The IoT Company in Indonesia” Ucap Firdaus. Firdaus juga kembali mengingatkan mengenai culture The Telkom Way yang mana menjadi pedoman dalam keseharian dilingkungan TelkomGroup.</div><div><br></div><div>Seluruh karyawan dengan baik menyimak sharing session yang diberikan dan sangat aktif menjawab ketika Firdaus melempar beberapa pertanyaan seputar visi misi serta target PINS juga pertanyaan seputar culture The Telkom Way, ini bertujuan untuk mengetahui sejauh mana para karyawan sudah mengenal PINS dan culture The Telkom Way.</div><div><br></div><div>Selepas sharing session, acara dilanjutkan dengan berbagai lomba kemerdekaan seperti tarik tambang, race crackers, food drama dan banyak lagi, perlombaan yang dapat mengujii team work dan kekompakan para karyawan dalam menyelesaikan setiap tantangan. Semua karyawan termasuk para senior leader ikut berpatisipasi dalam menambah keseruan acara. Acara diakhiri dengan pembagian hadiah pemenang lomba dan foto bersama.</div>','article_2_1539005222.jpg','-',2,'true','2018-09-23 02:17:05','2018-10-08 14:34:22',NULL,'PINS Rayakan Semarak Kemerdekaan','<div>Masih dengan suasana kemerdekaan dalam memperingati HUT RI ke-73, PT PINS Indonesia menggelar acara kemerdekan yang bertema Semarak Kemerdekaan dan bertempat di kantor PINS Telkom Landmark Tower Lt.42, Jakarta pada 23 Agustus 2018. Acara ini dihadiri oleh para jajaran Direksi PINS dan seluruh karyawan PINS domisili Jakarta. Acara berlangsung dengan sangat meriah dan semua karyawan sangat antusian disepanjang acara.</div>','<div>Masih dengan suasana kemerdekaan dalam memperingati HUT RI ke-73, PT PINS Indonesia menggelar acara kemerdekan yang bertema Semarak Kemerdekaan dan bertempat di kantor PINS Telkom Landmark Tower Lt.42, Jakarta pada 23 Agustus 2018. Acara ini dihadiri oleh para jajaran Direksi PINS dan seluruh karyawan PINS domisili Jakarta. Acara berlangsung dengan sangat meriah dan semua karyawan sangat antusian disepanjang acara.</div><div><br></div><div>Acara Semarak Kemerdekaan ini dibuka dengan kegiatan Leader Talks – Sharing Session by CEO PT PINS Indonesia Mohammad Firdaus, dalam sharing session, Firdaus mengatakan bahwa lebih dari 50% karyawan PINS adalah Gen Y, yang mana Gen Y ini memiliki semangat tinggi, achievement oriented, kreatif dan inovatif dalam berfikir. ”Dominasi Gen Y ini memiliki peranan yang sangat penting dalam mensupport PINS untuk mencapai target perusahaan dan memenuhi visi misi PINS yaitu To Become The Leading of The IoT Company in Indonesia” Ucap Firdaus. Firdaus juga kembali mengingatkan mengenai culture The Telkom Way yang mana menjadi pedoman dalam keseharian dilingkungan TelkomGroup.</div><div><br></div><div>Seluruh karyawan dengan baik menyimak sharing session yang diberikan dan sangat aktif menjawab ketika Firdaus melempar beberapa pertanyaan seputar visi misi serta target PINS juga pertanyaan seputar culture The Telkom Way, ini bertujuan untuk mengetahui sejauh mana para karyawan sudah mengenal PINS dan culture The Telkom Way.</div><div><br></div><div>Selepas sharing session, acara dilanjutkan dengan berbagai lomba kemerdekaan seperti tarik tambang, race crackers, food drama dan banyak lagi, perlombaan yang dapat mengujii team work dan kekompakan para karyawan dalam menyelesaikan setiap tantangan. Semua karyawan termasuk para senior leader ikut berpatisipasi dalam menambah keseruan acara. Acara diakhiri dengan pembagian hadiah pemenang lomba dan foto bersama.</div>'),
	(2,'Gandeng Orbiz, PINS Hadirkan Solusi GPS RTK','<div>Menghadapi persaingan bisnis yang semakin kompetitif, PINS berusaha untuk terus meningkatkan Value perusahaan dengan memberikan perbaikan dan peningkatan terhadap kapabilitas sumber daya manusia, bisnis proses, teknologi dan partnership perusahaan secara berkelanjutan.</div>','<div>Menghadapi persaingan bisnis yang semakin kompetitif, PINS berusaha untuk terus meningkatkan Value perusahaan dengan memberikan perbaikan dan peningkatan terhadap kapabilitas sumber daya manusia, bisnis proses, teknologi dan partnership perusahaan secara berkelanjutan.</div><div><br></div><div>Rabu 1 Agustus 2018, bertempat di kantor PINS TLT lt.42, PINS Indonesia melakukan signing ceremony partnership agreement dengan Orbiz International dari Hongkong. Penandatanganan ini dilakukan oleh CEO PINS Indonesia Mohammad Firdaus dan CEO Orbiz International Jason Lee, yang juga disaksikan langsung oleh Direktur Enterprise &amp; Business Service Telkom Dian Rachmawan, dan VP Enterprise Business Development Dudy Effendi.</div><div><br></div><div>“Hari ini kami melakukan kerjasama dengan Orbiz dalam menghadirkan layanan GPS RTK (real time kinematic). Kami merasa market di Indonesia sangat potential, sehingga kami perlu berpartnership dengan&nbsp; Orbiz ” Ungkap Firdaus PINS Indonesia sebagai subsidiary dari TelkomGroup yang bergerak di bidang IoT (Internet Of Things) bersama Orbiz menghadirkan solusi IoT layanan GPS RTK (GPS Real-Time Kinematic).</div><div><br></div><div>Solusi GPS RTK (GPS Real-Time Kinematic) adalah sistem untuk melakukan tracking object menggunakan GPS dengan akurasi sampai centimeter level. Teknologi GPS RTK ini sangat cocok untuk kegitan tracking, baik di darat, laut maupun udara.</div><div><br></div><div>Dalam sambutannya Dian Rachmawan menyampaikan dukungan atas kerjasama yang dilakukan oleh PINS dan Orbiz. “PINS Indonesia sudah berada di jalur yang tepat, untuk bekerjasama dengan Orbiz. Karena kita ketahui industri telco saat ini sedang menurun, dan kita harus membuat revenue stream baru dan tidak rely kepada legacy, karena legacy will be shifted menuju digital” Jelas Dian.</div><div><br></div><div>Dian juga menambahkan bahwa yang dibutuhkan oleh dunia enterprise saat ini tidak lain adalah cost efficiency dan increase productivity, dan IoT akan menjawab semua kebutuhan tersebut.</div><div><br></div><div>Teknologi GPS RTK ini cocok digunakan untuk segment transportasi, logistik, konstruksi, keamanan, dan pertanahan. PINS dan Orbiz berbagi peran dalam kerjasama ini, dimana PINS dan Telkom bersinergy untuk melakukan penetrasi ke pasar.</div>','article_1_1539005234.jpg','-',2,'true','2018-09-24 11:37:03','2018-10-08 14:34:55',NULL,'Gandeng Orbiz, PINS Hadirkan Solusi GPS RTK','<div>Menghadapi persaingan bisnis yang semakin kompetitif, PINS berusaha untuk terus meningkatkan Value perusahaan dengan memberikan perbaikan dan peningkatan terhadap kapabilitas sumber daya manusia, bisnis proses, teknologi dan partnership perusahaan secara berkelanjutan.</div>','<div>Menghadapi persaingan bisnis yang semakin kompetitif, PINS berusaha untuk terus meningkatkan Value perusahaan dengan memberikan perbaikan dan peningkatan terhadap kapabilitas sumber daya manusia, bisnis proses, teknologi dan partnership perusahaan secara berkelanjutan.</div><div><br></div><div>Rabu 1 Agustus 2018, bertempat di kantor PINS TLT lt.42, PINS Indonesia melakukan signing ceremony partnership agreement dengan Orbiz International dari Hongkong. Penandatanganan ini dilakukan oleh CEO PINS Indonesia Mohammad Firdaus dan CEO Orbiz International Jason Lee, yang juga disaksikan langsung oleh Direktur Enterprise &amp; Business Service Telkom Dian Rachmawan, dan VP Enterprise Business Development Dudy Effendi.</div><div><br></div><div>“Hari ini kami melakukan kerjasama dengan Orbiz dalam menghadirkan layanan GPS RTK (real time kinematic). Kami merasa market di Indonesia sangat potential, sehingga kami perlu berpartnership dengan&nbsp; Orbiz ” Ungkap Firdaus PINS Indonesia sebagai subsidiary dari TelkomGroup yang bergerak di bidang IoT (Internet Of Things) bersama Orbiz menghadirkan solusi IoT layanan GPS RTK (GPS Real-Time Kinematic).</div><div><br></div><div>Solusi GPS RTK (GPS Real-Time Kinematic) adalah sistem untuk melakukan tracking object menggunakan GPS dengan akurasi sampai centimeter level. Teknologi GPS RTK ini sangat cocok untuk kegitan tracking, baik di darat, laut maupun udara.</div><div><br></div><div>Dalam sambutannya Dian Rachmawan menyampaikan dukungan atas kerjasama yang dilakukan oleh PINS dan Orbiz. “PINS Indonesia sudah berada di jalur yang tepat, untuk bekerjasama dengan Orbiz. Karena kita ketahui industri telco saat ini sedang menurun, dan kita harus membuat revenue stream baru dan tidak rely kepada legacy, karena legacy will be shifted menuju digital” Jelas Dian.</div><div><br></div><div>Dian juga menambahkan bahwa yang dibutuhkan oleh dunia enterprise saat ini tidak lain adalah cost efficiency dan increase productivity, dan IoT akan menjawab semua kebutuhan tersebut.</div><div><br></div><div>Teknologi GPS RTK ini cocok digunakan untuk segment transportasi, logistik, konstruksi, keamanan, dan pertanahan. PINS dan Orbiz berbagi peran dalam kerjasama ini, dimana PINS dan Telkom bersinergy untuk melakukan penetrasi ke pasar.</div>'),
	(3,'Perkuat Bisnis IoT, PINS Indonesia Gandeng Accenture','<div>PT PINS Indonesia dan PT Accenture Indonesia, menandatangani Teaming Agreement dalam hal implementasi Internet of Things (IoT) dan solusi digital. Teaming Agreement tersebut ditandatangani secara bersama-sama oleh President DirectorPT PINS Indonesia Mohammad Firdaus dan President Director PT Accenture Indonesia Neneng Goenadi, yang juga disaksikan oleh VP Enterprise Business Development Dudy Effendi serta Deputy EGM DDS Ery Punta pada Jumat (15/12).<br></div>','<div><div>PT PINS Indonesia dan PT Accenture Indonesia, menandatangani Teaming Agreement dalam hal implementasi Internet of Things (IoT) dan solusi digital. Teaming Agreement tersebut ditandatangani secara bersama-sama oleh President DirectorPT PINS Indonesia Mohammad Firdaus dan President Director PT Accenture Indonesia Neneng Goenadi, yang juga disaksikan oleh VP Enterprise Business Development Dudy Effendi serta Deputy EGM DDS Ery Punta pada Jumat (15/12).</div><div><br></div><div>Dalam sambutannya Direktur Utama PINS mengaku senang atas teaming agreement antara PINS dengan Accenture. “Kerja sama ini merupakan langkah yang baik dan strategis, sebagaimana kita ketahui Accenture sendiri merupakan world class player di bidang IoT, dan PINS saat ini sedang fokus untuk terus mengembangkan IoT di Indonesia,” ungkap Firdaus.</div><div><br></div><div>Senada dengan Firdaus, President Director PT Accenture Indonesia Neneng Goenadi juga menyambut baik Teaming Agreement ini. “Saya berharap dengan penandatanganan Teaming Agreement antara PINS dengan Accenture ini, dapat melahirkan project-project besar, yang juga akan membangun Indonesia,” ujar Neneng.</div><div><br></div><div>Accenture sendiri akan berperan sebagai partner PINS yang menyediakan berbagai use case dan solusi IoT atau solusi digital, kemudian PINS bersama dengan Telkom berperan untuk mengembangkan solusi agar dapat di implementasikan ke berbagai customer. PINS dan Accenture akan bersama-sama mengembangkan 3 segmen bisnis yang memiliki prospek terbesar (dalam bidang manufacturing, transportasi/logistik dan smartcity).</div><div><br></div><div>Diharapkan dengan kerjasama ini, accenture dapat membantu PINS dalam menjawab kebutuhan solusi IoT di market, dan akan memperkuat positioning PINS sebagai The IoT Company.</div></div>','article_3_1539006098.jpg','-',2,'true','2018-10-08 13:41:38','2018-10-08 13:41:38',NULL,'Perkuat Bisnis IoT, PINS Indonesia Gandeng Accenture','<div>PT PINS Indonesia dan PT Accenture Indonesia, menandatangani Teaming Agreement dalam hal implementasi Internet of Things (IoT) dan solusi digital. Teaming Agreement tersebut ditandatangani secara bersama-sama oleh President DirectorPT PINS Indonesia Mohammad Firdaus dan President Director PT Accenture Indonesia Neneng Goenadi, yang juga disaksikan oleh VP Enterprise Business Development Dudy Effendi serta Deputy EGM DDS Ery Punta pada Jumat (15/12).<br></div>','<div><div>PT PINS Indonesia dan PT Accenture Indonesia, menandatangani Teaming Agreement dalam hal implementasi Internet of Things (IoT) dan solusi digital. Teaming Agreement tersebut ditandatangani secara bersama-sama oleh President DirectorPT PINS Indonesia Mohammad Firdaus dan President Director PT Accenture Indonesia Neneng Goenadi, yang juga disaksikan oleh VP Enterprise Business Development Dudy Effendi serta Deputy EGM DDS Ery Punta pada Jumat (15/12).</div><div><br></div><div>Dalam sambutannya Direktur Utama PINS mengaku senang atas teaming agreement antara PINS dengan Accenture. “Kerja sama ini merupakan langkah yang baik dan strategis, sebagaimana kita ketahui Accenture sendiri merupakan world class player di bidang IoT, dan PINS saat ini sedang fokus untuk terus mengembangkan IoT di Indonesia,” ungkap Firdaus.</div><div><br></div><div>Senada dengan Firdaus, President Director PT Accenture Indonesia Neneng Goenadi juga menyambut baik Teaming Agreement ini. “Saya berharap dengan penandatanganan Teaming Agreement antara PINS dengan Accenture ini, dapat melahirkan project-project besar, yang juga akan membangun Indonesia,” ujar Neneng.</div><div><br></div><div>Accenture sendiri akan berperan sebagai partner PINS yang menyediakan berbagai use case dan solusi IoT atau solusi digital, kemudian PINS bersama dengan Telkom berperan untuk mengembangkan solusi agar dapat di implementasikan ke berbagai customer. PINS dan Accenture akan bersama-sama mengembangkan 3 segmen bisnis yang memiliki prospek terbesar (dalam bidang manufacturing, transportasi/logistik dan smartcity).</div><div><br></div><div>Diharapkan dengan kerjasama ini, accenture dapat membantu PINS dalam menjawab kebutuhan solusi IoT di market, dan akan memperkuat positioning PINS sebagai The IoT Company.</div></div>'),
	(4,'PINS Indonesia Gelar RUPST Tahun Buku 2017 Umumkan Pergantian Dua Direksi','<div>Jakarta – Sebagai bentuk pertanggung jawaban atas jalannya Perseroan sepanjang tahun 2017, PT PINS Indonesia menyelenggarakan Rapat Umum Pemegang Saham Tahunan (RUPST) tahun buku 2017 yang dilaksanakan di ruang rapat krakatau Telkom Landmark Tower Lt.38 Jakarta Selatan, Kamis (22/3).<br></div>','<div><div>Jakarta – Sebagai bentuk pertanggung jawaban atas jalannya Perseroan sepanjang tahun 2017, PT PINS Indonesia menyelenggarakan Rapat Umum Pemegang Saham Tahunan (RUPST) tahun buku 2017 yang dilaksanakan di ruang rapat krakatau Telkom Landmark Tower Lt.38 Jakarta Selatan, Kamis (22/3).</div><div><br></div><div>RUPST dipimpin Komisaris Utama Otong Iip dan dihadiri Wakil pemegang saham yaitu Dir Keu Harry M. Zen serta seluruh jajaran Direksi dan Komisaris PT PINS Indonesia.</div><div><br></div><div>CEO PT PINS Indonesia Mohammad Firdaus melaporkan bahwa pada tahun 2017, PINS Indonesia mencatatkan pencapaian Revenue sebesar Rp3,6 triliun kemudian EBITDA sebesar Rp112 miliar, dan Net Income sebesar Rp55 miliar.</div><div><br></div><div>RUPST kali ini memutuskan adanya pergantian di jajaran Direksi PINS Indonesia. Adapun pergantian tersebut di antaranya Notje Rosanti sebelumnya berposisi AVP Subsidiary Performance Telkom kini sebagai Direktur Finance &amp; Business Support PINS menggantikan Hermawan Koesmanaputra, Imam Santoso sebelumnya berposisi EGM DSS kini sebagai Direktur Operation PINS menggantikan Abdi Mulyanta Ginting yang kini ditugaskan sebagai EGM DSS.</div><div><br></div><div>Adanya pergantian dua direksi PINS Indonesia, maka susunan lengkap Dewan Komisaris PINS dan Dewan Direksi PINS adalah sebagai berikut:</div><div><b><br></b></div><div><b>– Dewan Komisaris</b></div><div><br></div><div>Komisaris Utama : Otong Iip</div><div>Komisaris : Abdi Mulyana Ginting</div><div>Komisaris : Jemy Vestius Confido</div><div>Komisaris : Irnanda Laksana</div><div><b><br></b></div><div><b>– Dewan Direksi</b></div><div><br></div><div>Direktur Utama : Mohammad Firdaus</div><div>Direktur Sales : Benny Artono</div><div>Direktur Operation : Imam Santoso</div><div>Direktur Finance &amp; Business Support : Notje Rosanti</div><div>Dengan diambilnya keputusan dalam RUPST harapannya PINS dapat semakin maju dalam mewujudkan visi dan misi PINS dan dapat terus memberikan yang terbaik untuk TelkomGroup.</div></div>','article_4_1539006502.jpg','-',2,'true','2018-10-08 13:48:22','2018-10-08 13:48:22',NULL,'PINS Indonesia Gelar RUPST Tahun Buku 2017 Umumkan Pergantian Dua Direksi','<div>Jakarta – Sebagai bentuk pertanggung jawaban atas jalannya Perseroan sepanjang tahun 2017, PT PINS Indonesia menyelenggarakan Rapat Umum Pemegang Saham Tahunan (RUPST) tahun buku 2017 yang dilaksanakan di ruang rapat krakatau Telkom Landmark Tower Lt.38 Jakarta Selatan, Kamis (22/3).<br></div>','<div><div>Jakarta – Sebagai bentuk pertanggung jawaban atas jalannya Perseroan sepanjang tahun 2017, PT PINS Indonesia menyelenggarakan Rapat Umum Pemegang Saham Tahunan (RUPST) tahun buku 2017 yang dilaksanakan di ruang rapat krakatau Telkom Landmark Tower Lt.38 Jakarta Selatan, Kamis (22/3).</div><div><br></div><div>RUPST dipimpin Komisaris Utama Otong Iip dan dihadiri Wakil pemegang saham yaitu Dir Keu Harry M. Zen serta seluruh jajaran Direksi dan Komisaris PT PINS Indonesia.</div><div><br></div><div>CEO PT PINS Indonesia Mohammad Firdaus melaporkan bahwa pada tahun 2017, PINS Indonesia mencatatkan pencapaian Revenue sebesar Rp3,6 triliun kemudian EBITDA sebesar Rp112 miliar, dan Net Income sebesar Rp55 miliar.</div><div><br></div><div>RUPST kali ini memutuskan adanya pergantian di jajaran Direksi PINS Indonesia. Adapun pergantian tersebut di antaranya Notje Rosanti sebelumnya berposisi AVP Subsidiary Performance Telkom kini sebagai Direktur Finance &amp; Business Support PINS menggantikan Hermawan Koesmanaputra, Imam Santoso sebelumnya berposisi EGM DSS kini sebagai Direktur Operation PINS menggantikan Abdi Mulyanta Ginting yang kini ditugaskan sebagai EGM DSS.</div><div><br></div><div>Adanya pergantian dua direksi PINS Indonesia, maka susunan lengkap Dewan Komisaris PINS dan Dewan Direksi PINS adalah sebagai berikut:</div><div><span style=\"font-weight: 700;\"><br></span></div><div><span style=\"font-weight: 700;\">– Dewan Komisaris</span></div><div><br></div><div>Komisaris Utama : Otong Iip</div><div>Komisaris : Abdi Mulyana Ginting</div><div>Komisaris : Jemy Vestius Confido</div><div>Komisaris : Irnanda Laksana</div><div><span style=\"font-weight: 700;\"><br></span></div><div><span style=\"font-weight: 700;\">– Dewan Direksi</span></div><div><br></div><div>Direktur Utama : Mohammad Firdaus</div><div>Direktur Sales : Benny Artono</div><div>Direktur Operation : Imam Santoso</div><div>Direktur Finance &amp; Business Support : Notje Rosanti</div><div>Dengan diambilnya keputusan dalam RUPST harapannya PINS dapat semakin maju dalam mewujudkan visi dan misi PINS dan dapat terus memberikan yang terbaik untuk TelkomGroup.</div></div>');

/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `article_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `showed` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'false',
  PRIMARY KEY (`id`),
  KEY `comments_announcement_id_foreign` (`article_id`),
  CONSTRAINT `comments_announcement_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table companies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `email_notif` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;

INSERT INTO `companies` (`id`, `name`, `email`, `phone`, `address`, `image`, `description`, `created_at`, `updated_at`, `deleted_at`, `email_notif`)
VALUES
	(1,'PINS Indonesia','kontak@pins.co.id','0215202560','Jl. HR Rasuna Said kav.C11-C14 Jakarta Selatan','pins_1537549405.jpg','PINS Indonesia is a company dealing with the integration of devices and network supported by human resources capability and the best system. As a team, we always focus on the development of innovation to meet the the customers’ need amid the volatile industry situation. We also fully focus on the company transformation in order to be able to win the competition to get the added value for our customers, employees dan shareholers.','2018-10-01 10:49:13','2018-10-08 14:56:06',NULL,'pins.website@gmail.com');

/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table contents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contents`;

CREATE TABLE `contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `layout` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_1_id` text COLLATE utf8mb4_unicode_ci,
  `text_2_id` text COLLATE utf8mb4_unicode_ci,
  `text_3_id` text COLLATE utf8mb4_unicode_ci,
  `text_1_en` text COLLATE utf8mb4_unicode_ci,
  `text_2_en` text COLLATE utf8mb4_unicode_ci,
  `text_3_en` text COLLATE utf8mb4_unicode_ci,
  `image_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;

INSERT INTO `contents` (`id`, `created_at`, `updated_at`, `layout`, `type`, `deleted_at`, `name`, `text_1_id`, `text_2_id`, `text_3_id`, `text_1_en`, `text_2_en`, `text_3_en`, `image_1`, `image_2`, `image_3`, `video_1`)
VALUES
	(1,'2018-09-20 11:09:21','2018-09-20 11:14:40',4,2,NULL,'IoT Services','<div>IoT Service / Machine to Machine (M2M) adalah solusi yang bagus untuk Perencanaan, Desain, Pengadaan, Pelaksanaan, Perangkat operasi dengan sensor jaringan terintegrasi dan sistem Computing, analisis data. Secara keseluruhan, sistem membuat seluruh perangkat dan sensor dapat mengirim data melalui jaringan untuk sistem komputasi, sehingga dapat memberikan analisis data real time untuk pengambilan keputusan lebih cepat.</div><div>produk dan layanan ini dapat terjadi dalam berbagai skala, mulai dari skala besar seperti Smart Kota serta skala menengah seperti pengembangan Smart Building. Berbagai perangkat dan sensor yang terhubung ke sistem komputasi bermuara pada Sistem Otomasi dan pusat kendali.</div><div>Dalam pelaksanaannya, konsep Smart Kota diwujudkan dengan pemantauan dan pengendalian lalu lintas menggunakan kamera yang berbeda sensor (CCTV), sistem otomatisasi lampu lalu lintas dan penerangan jalan dikendalikan dari Pusat Pengendalian. Demikian juga untuk Smart Building yang memiliki kemampuan analisis data Building Automation untuk mengelola penggunaan energi yang digunakan AC (Pendingin Ruangan) dan Light dalam pencahayaan dengan menggunakan Smart Grid. Di luar rentang bahwa sensor untuk keamanan seperti detektor asap / api dan tombol panik dalam mengantisipasi insiden darurat.</div><div>IOT solusi / M2M telah terbukti mampu memberikan efisiensi penggunaan energi, meningkatkan produktivitas dan keamanan.</div>','<div><br></div>','<div><br></div>','<div>IoT Service / Machine to Machine (M2M) is a good solution for Planning, Design, Procurement, Implementation, Operation devices with integrated network sensor and Computing system, data analysis. Entirely, a system makes all devices and sensor able to transmit data via network for a computation system, thereby able to provide real time data analysis for faster decision making.</div><div>This product and service may occur in various scales, starting from large scale such as City Smart as well as medium scale such as development of Smart Building. Various devices and connected to the computation system will lead to the Automation System Otomasi and control center.</div><div>In its implementation, the City Smart concept is manifested by traffic monitoring and control using camera with different sensor (CCTV), automatic system of traffic light and street lighting are controlled from the Control Center. It also applies to the Smart Building that has capability of analysis of data on Building Automation to manage the consumption energy used in AC (Air Conditioner) and Light in the illumination by using Smart Grid. It excludes sensor for security such smoke/fire detector and panic button to anticipate emergency incident.</div><div>IOT solusi / M2M is alreday proven of able to provide efficiency in the energy use, increase productivity and security in caring.<br></div>','<div><br></div>','<div><br></div>','image_1537441761.png',NULL,NULL,NULL),
	(2,'2018-09-20 11:10:31','2018-09-20 11:14:39',1,2,NULL,'Mobility Services','<div><div>Merupakan layanan solusi korporasi yang mencakup pengembangan, perencanaan, implementasi dan perawatan terkait tuntutan korporasi dalam memenuhi kebutuhan mobilitas karyawan yang harus terhubung dengan aset data perusahaan secara aman.</div><div>Layanan kami memastikan Perusahaan Kecil / Menengah, Korporasi maupun Penyelenggara Layanan Publik mendapatkan kemudahan dalam pengelolaan perangkat mobile (handset, tablet, notebook) sebagai sarana kerja karyawan meliputi Penyediaan Perangkat, Pengaturan Aplikasi pada Perangkat, Pengaturan Akses ke Data Perusahaan dan Pengaturan Sistem Keamanan Data dan Jaringan. Disamping sistem pengaturan, Solusi kami menjadikan Perusahaan dapat melakukan analisis terhadap perilaku penggunaan perangkat mobile melalui sistem analitik yang akurat untuk memperoleh pola pengaturan yang effektif dan effisien.</div></div>','<div><br></div>','<div><br></div>','<div><div>It is a Corporate’s solutions service covering the Development, Planning, Implementation and Maintenance related to the corporate’s demands to meet the employees‘ need for mobility that must be connected to data on the company’s assets safely.</div><div>Our services ensures small/medium Companies, corporations or Public Service Providers to gain easiness in managing the mobile devices (handsets, tablet, notebook) as the employees’&nbsp;work facilities that cover the provision of devices, application arrangement in the devices, Arrangement of Access to Company’s Data and Arrangement of Data and Network Security system. In addition to the arrangement system, our solutions allow the company to be able to carry out analysis to the behavior of the use of mobile devices through an accurate analytical system to obtain the effective and efficient arrangement patterns.</div></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(3,'2018-09-20 11:11:10','2018-09-20 11:14:42',1,2,NULL,'CPE Manage Services','<div><div>Merupakan layanan solusi korporasi yang mencakup perencanaan, desain, pengadaan, implementasi, pengoperasian, dan jaminan dalam pemenuhan sistem perangkat dan jaringan data yang terintegrasi untuk mendukung berbagai perusahaan dalam kegiatan usaha. Dengan sumberdaya yang selalu kami kembangkan, layanan CPE Services kami senantiasa mengikuti perkembangan teknologi agar memberikan ruang bagi Anda untuk fokus kepada proses bisnis utama Perusahaan.,</div><div>Produk dalam bagian ini antara lain mencakup pengadaan perangkat keras IT (komputer, periferal, servers, routers, dan lain-lain). Sedangkan jasa layanan yang tercakup antara lain konsultasi kebutuhan perangkat, pengadaan, pemasangan, serta proses integrasi &amp; pemeliharaan.</div><div>Kami juga memberikan memberikan produk sistem keamanan dan optimalisasi jaringan yang dilengkapi dengan sistem monitoring secara periodik maupun real time yang dapat memberikan alert system apabila terjadi serangan agar Perusahaan Anda dapat memanfaatkan jaringan secara optimal dan terbebas dari berbagai serangan peretas dunia maya.</div></div>','<div><br></div>','<div><br></div>','<div><div>It is the Corporate’s solutions service covering the Planning, Design, Procurement, Implementation, Operation, and Guarantees in the fulfillment of the devices system and integrated data network to support various&nbsp;companies in their business activities. With the resources we always develop, our CPE Services always follows the technology developments in order be able to provide space to you to focus on your key business processes.</div><div>Products in this section covers the procurement of IT hardware (computers, peripherals, servers, routers, etc). While the services cover such as, Consultation on the need for device, procurement, installation, and Integration &amp; Maintenance Process.</div><div>We also provide products in forms of security system and network optimization equipped with periodical as well as real time monitoring system that is able to provide alert system when there is attack thereby allowing your company to utilize the network optimally and free from the various attacks of hackers</div></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(6,'2018-09-20 12:40:48','2018-10-08 15:06:07',1,2,NULL,'IoT Smart Building - Overview','<div>Perkembangan teknologi diyakini mampu mempermudah pengelolaan sumberdaya di gedung, dengan implementasi konsep Internet of Things (IoT) pada infrastruktur sensor dan jaringan akan menghasilkan efisiensi dan efektifitas pengendalian energi yang merupakan beban terbesar dalam pengelolaan.</div><div>PINS INDONESIA bersama mitra-mitra kelas dunia menghadirkan Solusi Smart Building untuk mewujudkan intelligent building dan green building. Dapatkan brosur kami smart-building-e-brosur atau Solusi IoT brosur IoT Product Catalog</div>','<div><br></div>','<div><br></div>','<div>The development of technology is believed to be able to facilitate the management of resources in buildings, with the implementation of the concept of Internet of Things (IoT) on sensor and network infrastructure will produce efficiency and effectiveness of energy control which is the biggest burden in management.</div><div>PINS INDONESIA and world-class partners present Smart Building Solutions to realize intelligent building and green building. Get our smart-building-e-brochure or IoT Product Catalog brochure IoT brochure</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(7,'2018-09-20 12:41:49','2018-09-20 12:41:49',1,2,NULL,'IoT Smart Building - Why Us','<div>Ini testing why us dalam indonesia</div>','<div><br></div>','<div><br></div>','<div>This is testing why us in English</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(8,'2018-09-20 12:43:15','2018-09-20 12:52:57',1,2,NULL,'IoT Smart Building - Product','<div>Perkembangan teknologi diyakini mampu mempermudah pengelolaan sumberdaya di gedung, dengan implementasi konsep Internet of Things (IoT) pada infrastruktur sensor dan jaringan akan menghasilkan efisiensi dan efektifitas pengendalian energi yang merupakan beban terbesar dalam pengelolaan.</div><div>Produk testing.</div><div>PINS INDONESIA bersama mitra-mitra kelas dunia menghadirkan Solusi Smart Building untuk mewujudkan intelligent building dan green building. Dapatkan brosur kami smart-building-e-brosur atau Solusi IoT brosur IoT Product Catalog</div>','<div><br></div>','<div><br></div>','<div>Perkembangan teknologi diyakini mampu mempermudah pengelolaan sumberdaya di gedung, dengan implementasi konsep Internet of Things (IoT) pada infrastruktur sensor dan jaringan akan menghasilkan efisiensi dan efektifitas pengendalian energi yang merupakan beban terbesar dalam pengelolaan.</div><div>English testing.</div><div>PINS INDONESIA bersama mitra-mitra kelas dunia menghadirkan Solusi Smart Building untuk mewujudkan intelligent building dan green building. Dapatkan brosur kami smart-building-e-brosur atau Solusi IoT brosur IoT Product Catalog</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(9,'2018-09-20 12:44:07','2018-09-20 12:54:01',1,2,NULL,'IoT Smart Building - Cased','<div>Melalui wakilnya, Vice Presiden of Human Capital PINS Indonesia Ahmad Syafri dan GM Procurement Revi Guspa mengatakan bahwa PINS Indonesia mendukung kegiatan CSR ini. Syafri berharap dengan adanya barang pemberian ini dapat membantu dalam kegiatan sehari-hari yayasan.</div><div><br></div><div>Wasuri selaku ketua yayasan menyambut dengan baik CSR ini, dimana yayasan ini juga masih membutuhkan sarana seperti meublier ini. Senada dengan wasuri, salah satu pengurus ponpes Tahfizhul Qur’an Aziziyyah mengucapkan banyak terima kasih atas bantuan yang diberikan oleh PINS, dan berharap dapat bermanfaat bagi yayasan. Rencananya meublier ini akan digunakan untuk lab Bahasa.</div><div><br></div><div>“Semoga sarana meublier ini dapat dimanfaatkan dengan sebaik-baiknya dan kami minta doanya agar PINS Indonesia terus maju dan berkembang kedepan”. Pungkas Syafri.</div>','<div><br></div>','<div><br></div>','<div>Melalui wakilnya, Vice Presiden of Human Capital PINS Indonesia Ahmad Syafri dan GM Procurement Revi Guspa mengatakan bahwa PINS Indonesia mendukung kegiatan CSR ini. Syafri berharap dengan adanya barang pemberian ini dapat membantu dalam kegiatan sehari-hari yayasan.</div><div><br></div><div>Wasuri selaku ketua yayasan menyambut dengan baik CSR ini, dimana yayasan ini juga masih membutuhkan sarana seperti meublier ini. Senada dengan wasuri, salah satu pengurus ponpes Tahfizhul Qur’an Aziziyyah mengucapkan banyak terima kasih atas bantuan yang diberikan oleh PINS, dan berharap dapat bermanfaat bagi yayasan. Rencananya meublier ini akan digunakan untuk lab Bahasa.</div><div><br></div><div>“Semoga sarana meublier ini dapat dimanfaatkan dengan sebaik-baiknya dan kami minta doanya agar PINS Indonesia terus maju dan berkembang kedepan”. Pungkas Syafri.</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(10,'2018-09-20 12:57:44','2018-09-20 12:57:44',8,0,NULL,'About - Overview','<div>PT. PINS Indonesia adalah perusahaan yang aktif dalam integrasi perangkat dan jaringan dengan kemampuan sumber daya manusia dan kapabilitas sistem yang terbaik. Sebagai sebuah tim, kami senantiasa fokus pada pengembangan inovasi untuk memenuhi kebutuhan customer yang dihadapkan pada situasi industri yang selalu berubah. Kami juga fokus penuh pada transformasi perusahaan agar tetap mampu memenangkan persaingan untuk mencapai nilai tambah bagi customer, karyawan dan shareholer kami.</div><div><br></div><div>PINS berdiri sejak tanggal 17 Oktober 1995 dengan nama PT. Pramindo Ikat Nusantara. Pada awalnya fokus bisnis Perseroan adalah untuk menyelenggarakan Kerja Sama Operasi (KSO) telekomunikasi di wilayah Sumatera.</div><div><br></div><div>Pada tahun 2002, saham Perseroan seluruhnya diambil alih oleh PT. Telekomunikasi Indonesia Tbk (TELKOM), sebuah perusahaan telekomunikasi terbesar di Indonesia, dan mengacu pada CSS TELKOM maka mulai Oktober 2010 Perseroan memfokuskan diri pada portofolio Premise Integration Service. Perubahan nama perusahaan dari PT. Pramindo Ikat Nusantara menjadi PT. PINS INDONESIA ini dikukuhkan tanggal 20 Desember 2012.</div>','<div><div>Pada tahun 2016, PINS Indonesia adalah perusahaan yang aktif dalam bisnis integrasi perangkat, jaringan, sistem dan proses menggunakan konsep Internet of Things (IoT) dengan kemampuan sumber daya manusia dan kapabilitas kesisteman yang terbaik. PINS Indonesia mempunyai tagline “The IoT Comapany”.</div><div>Sebagai sebuah tim, kami senantiasa fokus pada pengembangan inovasi untuk memenuhi kebutuhan customer yang dihadapkan pada situasi industri yang selalu berubah. Dengan senantiasa bersinergi bersama Telkom Group, kami juga fokus penuh pada transformasi perusahaan agar tetap mampu memenangkan persaingan untuk mencapai nilai tambah bagi customer, karyawan dan shareholder kami.</div><div>Pengalaman di bidang telekomunikasi selama lebih dari 20 tahun telah memposisikan Perseroan sebagai perusahaan penyedia sarana dan prasarana layanan telekomunikasi terlengkap dan terpercaya di seluruh Nusantara. Hal ini turut membangun kepercayaan diri Perseroan untuk melangkah lebih jauh melalui ekspansi bisnis telekomunikasi dan informatika multinasional. Didukung oleh sumber daya dan kapabilitas yang dimiliki, Perseroan siap bersaing untuk memberikan layanan yang lebih unggul, berkualitas, dan terjangkau di seluruh Indonesia.</div></div>','<div><br></div>','<div><div>PINS Indonesia is a company dealing with the integration of devices and network supported by human resources capability and the best system. As a team, we always focus on the development of innovation to meet the the customers’ need amid the volatile industry situation. We also fully focus on the company transformation in order to be able to win the competition to get the added value for our customers, employees dan shareholers.</div><div><br></div><div>PINS was established on October 17, 1995 under the name PT. Pramindo Ikat Nusantara. In early of its esablishment, the Company’s business focused on the entry into of Joint Operation (JO) on telecommunications in Sumatra.</div><div><br></div><div>In 2002, the Company’s shares were entirely acquired by PT. Telekomunikasi Indonesia Tbk (Telkom), the largest telecommunication company in Indonesia, and referring to the CSS TELKOM, thenin October 2010, the Company focussed on Integration Service Premise portfolio. The changing of the company’s name to PT. Pramindo Ikat Nusantara to PT. PINS INDONESIA was affirmed on December 20, 2012.</div></div>','<div><div>In 2016, PINS Indonesia was a company dealing with business in the integration of devices, networks, systems and processes by using the Internet of Things (IoT) concept supported by human resources’ capabilities and the best system capabilities. PINS Indonesia’s tagline is “The IoT Company”</div><div><br></div><div>As a team, we always focus on the development of innovations to meet the customers’needs amid the ever changing industry situation. By always entering into synergy together as a Telkom Group, we also focus on the transformation of the company to lead us the winner of the competition to get added value for our customers, employees and shareholders.</div><div><br></div><div>Experience in the telecommunication field for more than 20 years has led the Company as the provider of complete and reliable telecommunication services facilities and infrastructure in Indonesia. It also built the Company’s self- confidence to to take farther step through multinational telecommunications and information business expansion. Supported by the best resources and capabilities, the Company is ready to compete to provide excellent, high quality, and affordable services throughout Indonesia.</div></div>','<div><br></div>','overview_1537448264.jpg',NULL,NULL,NULL),
	(11,'2018-09-20 13:01:11','2018-09-20 13:01:11',1,0,NULL,'About - Visi Misi','<div><h2>VISI</h2></div><div>Menjadi perusahaan terdepan dalam penyediaan layanan solusi Internet of Things (IoT) di Indonesia.</div><h2>\nMISI</h2><div>Memaksimalkan nilai stakeholder dengan memberikan solusi teritegrasi end-to-end dan bersinergi dengan TelkomGroup<br></div>','<div><br></div>','<div><br></div>','<div><div><h2>VISION</h2></div><div>To become a leading IoT company in Indonesia.</div><h2>\nMISSION</h2><div>Maximizing the stakeholder’s value by providing end-to end integrated solution and building synergy with TelkomGroup</div></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(12,'2018-09-20 13:02:27','2018-09-20 13:05:08',1,0,NULL,'About - Career','<div><div>PT. PINS Indonesia adalah perusahaan penyedia solusi Internet of Things (IoT) yang berkembang pesat . Sebagai anak perusahaan dari PT. Telekomunikasi Indonesia, Tbk kami beroperasi di seluruh Indonesia.</div><div>Bersama dengan TelkomGroup dan mitra kelas dunia, kami menawarkan berbagai solusi inovatif yang sesuai dengan kebutuhan Customer yang selalu bertambah.</div><h2>\n\nBergabung Bersama Kami</h2><div>Jika Anda berminat bekerja di PT PINS Indonesia, silahkan kirim data dan lamaran Anda ke recruitment@pins.co.id atau dapat dikirimkan ke alamat :</div><div><br></div><div>DEPARTEMEN HUMAN CAPITAL</div><div>PINS INDONESIA</div><div>Plaza Kuningan</div><div>Gedung Annex Lt. 7</div><div>Jl. HR Rasuna Said Kav. C11-C14</div><div>Jakarta Selatan 12940</div><div>Indonesia</div></div>','<div><br></div>','<div><br></div>','<div><div>PT. PINS Indonesia is a fast developing provider of Internet of Things (IoT) solution. A a subsidiary of PT. Telekomunikasi Indonesia, Tbk our operating area is throughout Indonesia.</div><div>Along with TelkomGroup and world class partners, we offer a wide range of innovative solutions that are in line with the needs of the Customers whose number always increases.</div><h2>\nJoin Us</h2><div>If you are interested in working in PT Indonesia PINS, please send your application and data to recruitment@pins.co.id or to the address below:</div><div>HUMAN CAPITAL DEPARTMENT</div><div>PINS INDONESIA</div><div>Plaza Kuningan</div><div>Gedung Annex Lt. 7</div><div>Jl. HR Rasuna Said Kav. C11-C14</div><div>Jakarta Selatan 12940</div><div>Indonesia</div></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(13,'2018-09-21 16:31:57','2018-09-21 16:31:57',1,2,NULL,'PRIME - Overview','<div><div>Sebuah layanan yang memungkinkan perusahaan untuk mengatur, mengamankan, memantau, menemukan, melacak perangkat bergerak seperti handphone, tablet dan laptop.</div><div>Solusi Enterprise Mobility Management dilengkapi dengan jaminan keamanan data, reliability, dan sistem monitoring dan assurance 24 jam.</div><div>Area layanan mecakup domsetik hingga internasional. (Penjualan terbatas untuk wilayah Indonesia)</div></div>','<div><br></div>','<div><br></div>','<div><div>Sebuah layanan yang memungkinkan perusahaan untuk mengatur, mengamankan, memantau, menemukan, melacak perangkat bergerak seperti handphone, tablet dan laptop.</div><div>Solusi Enterprise Mobility Management dilengkapi dengan jaminan keamanan data, reliability, dan sistem monitoring dan assurance 24 jam.</div><div>English,</div><div>Area layanan mecakup domsetik hingga internasional. (Penjualan terbatas untuk wilayah Indonesia)</div></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(14,'2018-09-21 16:33:03','2018-09-21 16:33:03',1,2,NULL,'PRIME - Why Us','<div><div>Contoh text, Sebuah layanan yang memungkinkan perusahaan untuk mengatur, mengamankan, memantau, menemukan, melacak perangkat bergerak seperti handphone, tablet dan laptop.</div><div>Solusi Enterprise Mobility Management dilengkapi dengan jaminan keamanan data, reliability, dan sistem monitoring dan assurance 24 jam.</div><div>Area layanan mecakup domsetik hingga internasional. (Penjualan terbatas untuk wilayah Indonesia)</div></div>','<div><br></div>','<div><br></div>','<div><div>Example inggris, Sebuah layanan yang memungkinkan perusahaan untuk mengatur, mengamankan, memantau, menemukan, melacak perangkat bergerak seperti handphone, tablet dan laptop.</div><div>Solusi Enterprise Mobility Management dilengkapi dengan jaminan keamanan data, reliability, dan sistem monitoring dan assurance 24 jam.</div><div>Area layanan mecakup domsetik hingga internasional. (Penjualan terbatas untuk wilayah Indonesia)</div></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(15,'2018-09-21 16:34:46','2018-09-21 16:34:46',1,2,NULL,'PRIME - Product','<div><div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sed nulla vitae tellus sodales vehicula at vitae mauris. Donec vitae quam in leo egestas dictum et ut magna. Donec accumsan sed orci vitae cursus. Integer molestie, risus pellentesque aliquam mollis, elit nisl fringilla dui, id laoreet turpis lorem nec ligula. Duis pharetra ipsum vitae metus vehicula pharetra. Integer maximus iaculis tellus id volutpat. Praesent semper blandit rutrum. Pellentesque in bibendum elit. Mauris nunc risus, malesuada sed maximus sed, pretium quis metus.</div><div>Praesent dignissim quam in fermentum elementum. Nulla commodo erat tortor, at scelerisque est molestie a. Nulla feugiat nulla et elit accumsan, et semper purus aliquet. Pellentesque dictum tempus nunc, id rutrum turpis sagittis nec. Nullam ac justo tortor. Maecenas sollicitudin a diam ut feugiat. Vestibulum ac felis quis neque fringilla consequat at non neque. Integer laoreet sodales mollis. Etiam placerat dictum mauris, in facilisis massa luctus ut. Pellentesque vulputate facilisis mauris vel lobortis. Nulla placerat lorem dui, at iaculis leo lobortis lacinia. Praesent nec nisl at mauris convallis cursus. Integer est eros, varius faucibus ligula commodo, aliquet lacinia ex. Duis ac diam rhoncus, mollis est a, sodales dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div></div>','<div><br></div>','<div><br></div>','<div><div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sed viverra nisi. Pellentesque vel nulla dictum, condimentum mauris vel, egestas lacus. Fusce suscipit dolor in egestas cursus. Donec laoreet dapibus dolor vel sagittis. Vestibulum efficitur purus nisi. Nam et maximus ipsum. Mauris pulvinar diam et neque mollis elementum. Cras eleifend nisi et ligula imperdiet egestas. Praesent sagittis felis nisi, eu sollicitudin leo interdum vel. Donec ultricies quis est at tincidunt. Donec dolor lectus, pharetra pellentesque nunc vel, vulputate porta mi.</div><div>Nulla in bibendum neque. Pellentesque porttitor fermentum scelerisque. Aenean sagittis neque non tempus pulvinar. Etiam sed venenatis quam. Quisque suscipit augue sit amet enim molestie, id mollis ligula commodo. Suspendisse tempor enim sagittis, eleifend mi sit amet, dapibus libero. Fusce lacus neque, cursus eu metus eget, pulvinar maximus augue. Suspendisse nec lacus nec lorem iaculis porttitor. Phasellus mollis dui lectus, vel maximus nulla ornare ac. Donec fermentum malesuada vehicula.</div></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(16,'2018-09-21 16:36:41','2018-09-21 16:36:41',1,2,NULL,'PRIME - Cased','<div><div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vestibulum eget ligula vel commodo. Nam ut eros sit amet sapien placerat lobortis. Maecenas vehicula quam ut rutrum maximus. In vel leo eu tortor laoreet interdum. Aliquam ultrices volutpat nisi, ut vehicula dui lacinia non. Morbi vestibulum sed nisi et euismod. Morbi nunc risus, vulputate eu enim vel, faucibus posuere risus. Duis interdum lectus ut enim imperdiet, eu pretium nibh pretium. Morbi at porta purus, eu facilisis lorem. Vestibulum tincidunt, eros nec molestie ornare, libero felis ullamcorper enim, id laoreet mi sapien sed urna. Quisque sed fermentum ipsum. Praesent sed imperdiet libero. Etiam sit amet ullamcorper mauris, eget facilisis sem. Nunc et enim enim. Cras aliquam nulla sed luctus congue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</div><div>Pellentesque in rutrum felis, vel dignissim magna. In rhoncus porta enim, ac tincidunt augue sollicitudin vel. Sed fermentum dui quis justo mollis sodales. Nullam eu efficitur elit. Nulla facilisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultrices erat id est vulputate eleifend. Aliquam eget velit ut metus posuere vulputate. Praesent eget odio magna. Nunc quis suscipit ipsum.</div></div>','<div><br></div>','<div><br></div>','<div><div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum auctor elit in mauris finibus tincidunt. Mauris rutrum efficitur pharetra. Proin elementum, est vitae ultrices vulputate, tortor urna blandit leo, vitae maximus ipsum nulla non erat. Nam hendrerit sapien non convallis hendrerit. Nulla iaculis at sem non pretium. Morbi eget scelerisque massa. Proin interdum ante ut sagittis ultricies. Sed vitae elit ligula. Mauris fringilla id mauris quis pulvinar. Duis finibus libero vel justo mollis posuere. Praesent auctor pretium enim id volutpat.</div><div>Sed fermentum sollicitudin lobortis. Vivamus aliquet odio sed mi volutpat, in sollicitudin metus lobortis. Nam id dolor et elit faucibus hendrerit. Duis accumsan dui id lacus mattis sodales. Nullam commodo enim vitae justo luctus condimentum. Praesent at venenatis ante. Donec aliquet odio ut mattis viverra. Maecenas lacinia scelerisque dignissim.</div></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(17,'2018-09-21 17:07:58','2018-09-21 17:07:58',4,2,NULL,'Seat Management - Overview','<div>Seat Management adalah metode mengkoordinasikan semua workstation (PC, Laptop, Printer, zero client / thin client solution) dalam jaringan perusahaan, dengan mengawasi installasi, operasi serta pemeliharaan hardware dan software dalam setiap workstation, yang juga merupakan solusi dari permasalahan pengadaan PC, Laptop, dan Printer, zero client / thin client solution dalam jumlah besar.<br></div>','<div><br></div>','<div><br></div>','<div>Seat Management adalah metode mengkoordinasikan semua workstation (PC, Laptop, Printer, zero client / thin client solution) dalam jaringan perusahaan, dengan mengawasi installasi, operasi serta pemeliharaan hardware dan software dalam setiap workstation, yang juga merupakan solusi dari permasalahan pengadaan PC, Laptop, dan Printer, zero client / thin client solution dalam jumlah besar. English.<br></div>','<div><br></div>','<div><br></div>','sm_1537553278.jpg',NULL,NULL,NULL),
	(18,'2018-09-21 17:13:58','2018-09-21 17:13:58',1,2,NULL,'Seat Management - Why Us','<div><h1>Fitur</h1></div>\n<div><ul><li>Inventory Management<br></li><li>Software Aplikasi Management<br></li><li>Software Deployment dan Distribution Management<br></li><li>Software License Management<br></li><li>Software Change Alert<br></li><li>Patch Management<br></li><li>Software Metering<br></li><li>Bandwitdth Throttling<br></li><li>Zero Client / Thin Client Solution<br></li></ul></div>','<div><div>Fitur</div><div><br></div><div><br></div><div>Inventory Management</div><div>Software Aplikasi Management</div><div>Software Deployment dan Distribution Management</div><div>Software License Management</div><div>Software Change Alert</div><div>Patch Management</div><div>Software Metering</div><div>Bandwitdth Throttling</div><div>Zero Client / Thin Client Solution</div></div>','<div><div>Fitur</div><div><br></div><div><br></div><div>Inventory Management</div><div>Software Aplikasi Management</div><div>Software Deployment dan Distribution Management</div><div>Software License Management</div><div>Software Change Alert</div><div>Patch Management</div><div>Software Metering</div><div>Bandwitdth Throttling</div><div>Zero Client / Thin Client Solution</div></div>','<div><h1>Fitur</h1></div><div><ul><li>Inventory Management<br></li><li>Software Aplikasi Management<br></li><li>Software Deployment dan Distribution Management<br></li><li>Software License Management<br></li><li>Software Change Alert<br></li><li>Patch Management<br></li><li>Software Metering<br></li><li>Bandwitdth Throttling<br></li><li>Zero Client / Thin Client Solution</li></ul></div>','<div><div>Fitur</div><div><br></div><div><br></div><div>Inventory Management</div><div>Software Aplikasi Management</div><div>Software Deployment dan Distribution Management</div><div>Software License Management</div><div>Software Change Alert</div><div>Patch Management</div><div>Software Metering</div><div>Bandwitdth Throttling</div><div>Zero Client / Thin Client Solution</div></div>','<div><div>Fitur</div><div><br></div><div><br></div><div>Inventory Management</div><div>Software Aplikasi Management</div><div>Software Deployment dan Distribution Management</div><div>Software License Management</div><div>Software Change Alert</div><div>Patch Management</div><div>Software Metering</div><div>Bandwitdth Throttling</div><div>Zero Client / Thin Client Solution</div></div>',NULL,NULL,NULL,NULL),
	(19,'2018-09-21 17:15:30','2018-09-21 17:15:30',1,2,NULL,'Seat Management - Product','<div>Testing</div>','<div><br></div>','<div><br></div>','<div>Testing english</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(20,'2018-09-21 17:17:03','2018-09-21 17:17:03',3,2,NULL,'Seat Management - Cased','<div><br></div>','<div><br></div>','<div><br></div>','<div><br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,'https://www.youtube.com/watch?v=RfKjn_AzuwU'),
	(21,'2018-10-04 12:24:07','2018-10-04 12:24:07',1,2,NULL,'Government Industries','<font face=\"lucida grande, helvetica, verdana, arial, sans-serif\">Ini contoh text industri goverment dengan bahasa indonesia</font>','<div><br></div>','<div><br></div>','<div>This is an example for the content of the government industries</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(22,'2018-10-04 12:25:13','2018-10-04 12:25:13',1,2,NULL,'Healthcare Industries','<div>Ini adalah contoh sebuah content untuk industri kesehatan</div>','<div><br></div>','<div><br></div>','<div>This is an example for a healthcare industries content</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(23,'2018-10-04 12:31:43','2018-10-04 12:31:43',1,2,NULL,'IoT Gateway - Overview','<div>Sebuah contoh kontent</div>','<div><br></div>','<div><br></div>','<div>An example</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(24,'2018-10-04 12:32:20','2018-10-04 12:32:20',1,2,NULL,'IoT Gateway - Why Us','<div>Lorem ipsum</div>','<div><br></div>','<div><br></div>','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(25,'2018-10-04 12:32:39','2018-10-04 12:32:39',1,2,NULL,'IoT Gateway - Cased','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(26,'2018-10-04 12:33:00','2018-10-04 12:33:00',1,2,NULL,'IoT Gateway - Product','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(27,'2018-10-04 12:33:32','2018-10-04 12:33:32',1,2,NULL,'IT Infrastructure Management - Overview','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(28,'2018-10-04 12:33:49','2018-10-04 12:33:49',1,2,NULL,'IT Infrastructure Management - Cased','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(29,'2018-10-04 12:34:04','2018-10-04 12:34:04',1,2,NULL,'IT Infrastructure Management - Product','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(30,'2018-10-04 12:34:22','2018-10-04 12:34:22',1,2,NULL,'IT Infrastructure Management - Why Us','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(31,'2018-10-04 12:34:58','2018-10-04 12:34:58',1,2,NULL,'Smart Lighting - Overview','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(32,'2018-10-04 12:35:11','2018-10-04 12:35:11',1,2,NULL,'Smart Lighting - Cased','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(33,'2018-10-04 12:35:27','2018-10-04 12:35:27',1,2,NULL,'Smart Lighting - Product','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(34,'2018-10-04 12:35:44','2018-10-04 12:35:44',1,2,NULL,'Smart Lighting - Why Us','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(35,'2018-10-04 12:36:14','2018-10-04 12:36:14',1,2,NULL,'Smart Water - Overview','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(36,'2018-10-04 12:36:29','2018-10-04 12:36:29',1,2,NULL,'Smart Water - Cased','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(37,'2018-10-04 12:36:46','2018-10-04 12:36:46',1,2,NULL,'Smart Water - Product','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(38,'2018-10-04 12:37:18','2018-10-04 12:37:18',1,2,NULL,'Smart Water - Why Us','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>Lorem ipsum<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(39,'2018-10-04 16:37:33','2018-10-04 16:37:33',1,2,NULL,'Smart Living - Overview','<div>Ini adalah lorem ipsum</div>','<div><br></div>','<div><br></div>','<div>This is lorem ipsum</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(40,'2018-10-04 16:37:58','2018-10-04 16:37:58',1,2,NULL,'Smart Living - Cased','<div>Ini adalah lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>This is lorem ipsum<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(41,'2018-10-04 16:38:18','2018-10-04 16:38:18',1,2,NULL,'Smart Living - Why Us','<div>Ini adalah lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>This is Lorem ipsum</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(42,'2018-10-04 16:38:49','2018-10-04 16:38:49',1,2,NULL,'Smart Living - Product','<div>Ini adalah lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>This is lorem ipsum</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(43,'2018-10-04 16:39:44','2018-10-04 16:39:44',1,2,NULL,'Handsets - Overview','<div>Hanya teks saja</div>','<div><br></div>','<div><br></div>','<div>This is text only</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(44,'2018-10-04 16:40:07','2018-10-04 16:40:07',1,2,NULL,'Handsets - Product','<div>Ini adalah lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>This is lorem ipsum</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(45,'2018-10-04 16:40:49','2018-10-04 16:40:49',1,2,NULL,'Handsets - Why Us','<div>Ini adalah lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>This is lorem ipsum</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(46,'2018-10-04 16:41:13','2018-10-04 16:41:13',1,2,NULL,'Handsets - Cased','<div>Ini adalah lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>This is a lorem ipsum</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(47,'2018-10-04 16:42:12','2018-10-04 16:42:36',1,2,NULL,'ICT Security Services - Overview','<div>Ini adalah lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>This is lorem ipsum</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(48,'2018-10-04 16:43:04','2018-10-04 16:43:04',1,2,NULL,'ICT Security Services - Why Us','<div>Ini adalah lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>This is lorem ipsum</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(49,'2018-10-04 16:43:29','2018-10-04 16:43:29',1,2,NULL,'ICT Security Services - Product','<div>Ini adalah lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>This is lorem ipsum</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(50,'2018-10-04 16:43:48','2018-10-04 16:43:48',1,2,NULL,'ICT Security Services - Cased','<div>Ini adalah lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>This is lorem ipsum</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(51,'2018-10-04 16:44:25','2018-10-04 16:44:25',1,2,NULL,'IT Hardware - Overview','<span style=\"font-family: &quot;lucida grande&quot;, helvetica, verdana, arial, sans-serif;\">Ini adalah lorem ipsum overview</span><br>','<div><br></div>','<div><br></div>','<div>This is an overview lorem ipsum</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(52,'2018-10-04 16:45:47','2018-10-04 16:45:47',1,2,NULL,'IT Hardware - Cased','<div>Ini adalah lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>This is a lorem ipsum</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(53,'2018-10-04 16:46:11','2018-10-04 16:46:11',1,2,NULL,'IT Hardware - Why Us','<div>Ini adalah lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>This is an example</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(54,'2018-10-04 16:46:37','2018-10-04 16:46:37',1,2,NULL,'IT Hardware - Product','<div>Ini adalah lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>This is a lorem ipsum</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(55,'2018-10-04 16:47:06','2018-10-04 16:47:06',1,2,NULL,'Managed CPE Services - Overview','<div>Ini adalah lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>This is an overview</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(56,'2018-10-04 16:47:34','2018-10-04 16:47:34',1,2,NULL,'Managed CPE Services - Cased','<div>Ini adalah lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>This is a cased study<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(57,'2018-10-04 16:48:12','2018-10-04 16:48:12',1,2,NULL,'Managed CPE Services - Why Us','<span style=\"font-family: &quot;lucida grande&quot;, helvetica, verdana, arial, sans-serif;\">Ini adalah lorem ipsum</span><br>','<div><br></div>','<div><br></div>','<div>This is why us example content</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(58,'2018-10-04 16:48:34','2018-10-04 16:48:34',1,2,NULL,'Managed CPE Services - Product','<div>Ini adalah lorem ipsum<br></div>','<div><br></div>','<div><br></div>','<div>This is a product content</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(59,'2018-10-05 06:49:52','2018-10-05 06:49:52',1,2,NULL,'Education Industries','<div>Ini contoh kontent</div>','<div><br></div>','<div><br></div>','<div>This is an example content</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(60,'2018-10-05 06:50:23','2018-10-05 06:50:23',1,2,NULL,'Finance Industries','<div>Ini contoh kontent<br></div>','<div><br></div>','<div><br></div>','<div>This is an example</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(61,'2018-10-05 06:50:48','2018-10-05 06:50:48',1,2,NULL,'Banking Industries','<div>Ini contoh kontent<br></div>','<div><br></div>','<div><br></div>','<div>This is an example</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(62,'2018-10-05 06:51:08','2018-10-05 06:51:08',1,2,NULL,'Retail Industries','<div>Ini contoh kontent<br></div>','<div><br></div>','<div><br></div>','<div>This is an example</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(63,'2018-10-05 06:51:48','2018-10-05 06:51:48',1,2,NULL,'Transport & Logistics Industries','<div>Ini contoh kontent<br></div>','<div><br></div>','<div><br></div>','<div>This is an example content</div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL),
	(64,'2018-10-05 06:52:10','2018-10-05 06:52:10',1,2,NULL,'Media Industries','<div>Ini contoh kontent<br></div>','<div><br></div>','<div><br></div>','<div>This is an example content<br></div>','<div><br></div>','<div><br></div>',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table email_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `email_templates`;

CREATE TABLE `email_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `email_templates` WRITE;
/*!40000 ALTER TABLE `email_templates` DISABLE KEYS */;

INSERT INTO `email_templates` (`id`, `type`, `name`, `subject`, `content`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'1','Notif Admin - Contact Us','Contact Us','<div><p>Pengguna mengirimkan email pada website PINS Indonesia</p><h5>Nama</h5><p>**name**</p><h5>Email</h5><p>**email**</p><h5>Telp</h5><p>**phone**</p><h5>Pesan</h5><p>**message**</p></div>','2018-01-01 00:00:00','2018-09-28 07:28:48',NULL),
	(2,'1','Notif User - Contact Us','Contact Us','<div><p>Halo, **name**</p><p>Terima kasih telah mengirimkan pesan kepada kami, tim kami akan segera menghubungi Anda dalam waktu dekat untuk proses selanjutnya.</p></div>','2018-01-01 00:00:00','2018-09-28 07:28:48',NULL),
	(3,'2','Notif Admin - Comment','Comment','<div><p>Pengguna mengirimkan komentar pada **article** article</p><h5>Nama</h5><p>**name**</p><h5>Email</h5><p>**email**</p><h5>Komentar</h5><p>**comment**</p></div>','2018-01-01 00:00:00','2018-09-28 07:28:48',NULL),
	(4,'2','Notif User - Comment','Comment','<div><p>Halo, **name**</p><p>Terima kasih telah mengirimkan komentar kepada kami, komentar Anda akan tampil setelah diverifikasi oleh Admin.</p></div>','2018-01-01 00:00:00','2018-09-28 07:28:48',NULL);

/*!40000 ALTER TABLE `email_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table enquiries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `enquiries`;

CREATE TABLE `enquiries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `name_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menus_parent_id_foreign` (`parent_id`),
  CONSTRAINT `menus_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `menus` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;

INSERT INTO `menus` (`id`, `parent_id`, `name_id`, `ref_id`, `type`, `link`, `created_at`, `updated_at`, `deleted_at`, `name_en`, `order`, `icon`)
VALUES
	(1,NULL,'Beranda',NULL,1,'home','2018-09-21 16:51:44','2018-09-21 16:51:44',NULL,'Home',1,NULL),
	(2,NULL,'Produk & Layanan',NULL,2,'product-services','2018-09-21 16:51:44','2018-09-21 16:51:44',NULL,'Product & Services',20,NULL),
	(3,NULL,'Berita',NULL,3,'news','2018-09-21 16:51:44','2018-09-21 16:51:44',NULL,'News',30,NULL),
	(4,NULL,'Tentang Kami',NULL,7,'about','2018-09-21 16:51:44','2018-09-21 16:51:44',NULL,'About Us',40,NULL),
	(5,4,'Tim',NULL,4,'team','2018-09-21 16:51:44','2018-09-21 16:51:44',NULL,'Team',10,'about-team.png'),
	(6,4,'Rekanan',NULL,5,'partner','2018-09-21 16:51:44','2018-09-21 16:51:44',NULL,'Partner',20,'about-partner.png'),
	(7,4,'Kontak',NULL,6,'contact','2018-09-21 16:51:44','2018-09-21 16:51:44',NULL,'Contact',30,'about-contact.png'),
	(8,4,'Pengantar','10',0,'overview','2018-09-21 16:53:14','2018-09-21 16:53:14',NULL,'Overview',1,'about-overview_1537552394.png'),
	(9,4,'Visi Misi','11',0,'vission_mission','2018-09-21 16:54:33','2018-09-21 16:54:33',NULL,'Vission Mission',2,'about-vision_1537552473.png'),
	(10,4,'Karir','12',0,'career','2018-09-21 16:56:04','2018-09-21 16:56:04',NULL,'Career',11,'about-career_1537552564.png'),
	(11,2,'Layanan IoT','1',2,'iot_services','2018-09-21 16:58:14','2018-09-21 16:58:14',NULL,'IoT Services',1,'product-iot_1537552694.png'),
	(12,2,'Layanan Mobilitas','2',2,'mobility_service','2018-09-21 16:58:57','2018-09-21 16:58:57',NULL,'Mobility Service',2,'product-mobility_1537552737.png'),
	(13,2,'Layanan Manajemen CPE','3',2,'cpe_manage_services','2018-09-21 16:59:57','2018-09-21 16:59:57',NULL,'CPE Manage Services',3,'product-cpe_1537552797.png'),
	(14,11,'Gedung Pintar IoT','4',2,'iot_smart_building','2018-09-21 17:01:38','2018-09-21 17:01:38',NULL,'IoT Smart Building',1,'iot-smart-building-icon_1537552898.png'),
	(15,12,'PRIME','5',2,'prime','2018-09-21 17:02:28','2018-10-04 16:57:04',NULL,'PRIME',2,'mobility-prime-icon_1537552948.png'),
	(16,13,'Seat Management','6',2,'seat_management','2018-09-21 17:18:49','2018-09-21 17:18:49',NULL,'Seat Management',1,'cpe-seat-management-icon_1537553929.png'),
	(17,NULL,'Industri',NULL,2,'industries','2018-10-04 12:08:19','2018-10-04 12:08:19',NULL,'Industries',21,NULL),
	(18,17,'Pemerintahan','7',2,'government','2018-10-04 12:10:38','2018-10-05 05:29:20',NULL,'Government',11,'product-gov_1538655038.png'),
	(19,17,'Kesehatan','8',2,'healthcare','2018-10-04 12:11:06','2018-10-05 05:29:30',NULL,'Healthcare',12,'product-healthcare_1538655066.png'),
	(20,18,'Gedung Pintar IoT','4',2,'iot_smart_building','2018-10-04 12:12:21','2018-10-04 16:11:36',NULL,'IoT Smart Building',1,'iot-smart-building-icon_1538655141.png'),
	(21,11,'Pengelolaan Infrastruktur IT','10',2,'it_infrastructure_management','2018-10-04 16:34:28','2018-10-04 16:34:28',NULL,'IT Infrastructure Management',2,'iot-it-infra-icon_1538670868.png'),
	(22,11,'IoT Gateway','9',2,'iot_gateway','2018-10-04 16:35:16','2018-10-04 17:01:47',NULL,'IoT Gateway',4,'iot-iot-gateway-icon_1538670916.png'),
	(23,11,'Smart Lighting','11',2,'smart_lighting','2018-10-04 16:36:04','2018-10-04 17:06:46',NULL,'Smart Lighting',5,'iot-smart-lighting-icon_1538670964.png'),
	(24,11,'Smart Water','12',2,'smart_water','2018-10-04 16:36:39','2018-10-04 17:08:48',NULL,'Smart Water',6,'iot-smart-water-icon_1538670999.png'),
	(25,12,'Handsets','14',2,'handsets','2018-10-04 16:57:35','2018-10-04 16:57:35',NULL,'Handsets',1,'mobility-handset-icon_1538672255.png'),
	(26,13,'ICT Security Services','15',2,'ict_security_services','2018-10-04 16:58:25','2018-10-04 16:58:25',NULL,'ICT Security Services',2,'cpe-ict-security-system-icon_1538672305.png'),
	(27,13,'IT Hardware','16',2,'it_hardware','2018-10-04 16:59:08','2018-10-04 16:59:08',NULL,'IT Hardware',3,'cpe-it-hardware-icon_1538672348.png'),
	(28,13,'Managed CPE Services','17',2,'managed_cpe_services','2018-10-04 17:00:12','2018-10-04 17:00:12',NULL,'Managed CPE Services',4,'cpe-managed-cpe-services-icon_1538672412.png'),
	(29,11,'Smart Living','13',2,'smart_living','2018-10-04 17:01:35','2018-10-04 17:01:35',NULL,'Smart Living',3,'iot-smart-living-icon_1538672495.png'),
	(30,19,'Smart Living','13',2,'smart_living','2018-10-04 17:10:13','2018-10-04 17:10:27',NULL,'Smart Living',1,'iot-smart-living-icon_1538673013.png'),
	(31,18,'Pengelolaan Infrastruktur IT','10',2,'it_infrastructure_management','2018-10-04 17:11:57','2018-10-04 17:12:53',NULL,'IT Infrastructure Management',2,'iot-it-infra-icon_1538673117.png'),
	(32,18,'IoT Gateway','9',2,'iot_gateway','2018-10-04 17:13:25','2018-10-04 17:13:25',NULL,'IoT Gateway',3,'iot-iot-gateway-icon_1538673205.png'),
	(33,19,'Smart Water','12',2,'smart_water','2018-10-04 17:14:27','2018-10-04 17:14:27',NULL,'Smart Water',2,'iot-smart-water-icon_1538673267.png'),
	(34,17,'Pendidikan','18',2,'education','2018-10-05 05:29:06','2018-10-05 07:13:33',NULL,'Education',10,'education_w_1538717346.png'),
	(35,17,'Keuangan','19',2,'finance','2018-10-05 05:30:15','2018-10-05 07:13:42',NULL,'Finance',13,'finance_w_1538717415.png'),
	(36,17,'Perbankan','20',2,'banking','2018-10-05 05:30:53','2018-10-05 07:13:50',NULL,'Banking',14,'banking_w_1538717453.png'),
	(37,17,'Retail','21',2,'retail','2018-10-05 05:34:57','2018-10-05 07:14:00',NULL,'Retail',15,'retail_w_1538717697.png'),
	(38,17,'Transportasi Logistik','22',2,'transport_logistic','2018-10-05 05:36:13','2018-10-05 07:14:10',NULL,'Transport Logistic',16,'transport_w_1538717773.png'),
	(39,17,'Media','23',2,'media','2018-10-05 05:39:33','2018-10-05 07:14:18',NULL,'Media',17,'media_w_1538717973.png');

/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2018_08_23_025705_add_status_and_number_to_users_table',1),
	(4,'2018_08_25_070145_create_enquiries_table',2),
	(5,'2018_08_25_100320_create_teams_table',2),
	(6,'2018_08_25_155157_add_deleted_at_to_users_table',2),
	(7,'2018_08_26_082929_create_areas_table',2),
	(8,'2018_08_26_091015_create_products_table',2),
	(9,'2018_08_26_230615_create_partners_table',2),
	(10,'2018_08_26_230732_create_offices_table',2),
	(11,'2018_08_26_232133_create_companies_table',2),
	(12,'2018_08_26_232343_create_socialmedia_table',2),
	(13,'2018_08_27_073429_create_announcements_table',2),
	(14,'2018_08_27_091713_create_comments_table',2),
	(15,'2018_08_27_073429_create_articles_table',3),
	(16,'2018_09_01_074250_drop_status_and_number_to_user_table',4),
	(17,'2018_09_01_074506_create_contents_table',4),
	(18,'2018_09_01_074847_create_menus_table',4),
	(19,'2018_09_04_143900_alter_menus_table',5),
	(20,'2018_09_04_150127_change_nullable_menu',5),
	(21,'2018_09_04_150308_alter_products_table',5),
	(22,'2018_09_04_152211_alter_contents_table',5),
	(23,'2018_09_05_134818_add_order_icon_menus',6),
	(24,'2018_09_05_135155_add_images_products',6),
	(25,'2018_09_06_155322_add_type_name_to_content',7),
	(26,'2018_09_06_160253_add_delete_at_to_content',7),
	(27,'2018_09_06_213746_alter_menu4',7),
	(28,'2018_09_08_124456_alter_is_featured_articles',8),
	(29,'2018_09_08_113153_alter_contents_name_contens_table',9),
	(30,'2018_09_11_172654_add_deleted_at_to_team',10),
	(31,'2018_09_13_123312_add_group_name_in_teams',11),
	(32,'2018_09_13_124607_add_details_in_teams',12),
	(33,'2018_09_13_132411_delete_content_from_content',13),
	(34,'2018_09_13_200535_alter_table_content',13),
	(35,'2018_09_13_212303_alter_table_content2',13),
	(36,'2018_09_15_084728_move_featured_from_menu',14),
	(37,'2018_09_15_090139_alter_featured_from_product',14),
	(38,'2018_09_15_213721_alter_product_from_partner',15),
	(39,'2018_09_16_210402_alter_comments_table',15),
	(40,'2018_09_16_211254_alter_announcement_table',15),
	(41,'2018_09_18_134255_alter_socialmedia',16),
	(42,'2018_09_18_144518_alter_area',16),
	(43,'2018_09_18_145133_alter_companies_table',16),
	(44,'2018_09_19_203717_alter_companies',17),
	(45,'2018_09_19_211142_alter_companies2',17),
	(46,'2018_09_19_215049_alter_menus',17),
	(47,'2018_09_20_104344_alter_companies',18),
	(48,'2018_09_21_212344_drop_photo_from_socialmedia',19),
	(49,'2018_09_24_113919_alter_article_change_nullable_video',20),
	(50,'2018_09_24_141939_alter_comments_drop_is_featured',21),
	(51,'2018_09_27_125633_alter_tables_articles',22),
	(52,'2018_09_27_130541_alter_tables_comments',22),
	(53,'2018_09_27_131026_alter_tables_companies',22),
	(54,'2018_09_27_133041_create_email_templates_table',22),
	(55,'2018_09_27_140333_create_analytics_table',22),
	(56,'2018_10_02_143733_alter_areas_table',23);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table offices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `offices`;

CREATE TABLE `offices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table partners
# ------------------------------------------------------------

DROP TABLE IF EXISTS `partners`;

CREATE TABLE `partners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `partners_product_id_foreign` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `partners` WRITE;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;

INSERT INTO `partners` (`id`, `product_id`, `name`, `image`, `description`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'1,3','Cisco','cisco_1537554857.png','-','2018-09-21 17:34:17','2018-09-21 17:34:17',NULL),
	(2,'1,2','Huawei','huawei_1537554919.png','-','2018-09-21 17:35:19','2018-09-21 17:35:19',NULL),
	(3,'1','Lantronix','lantronix_1537554977.png','-','2018-09-21 17:36:17','2018-09-21 17:36:17',NULL),
	(4,'1','Honeywell','Honeywell_1537556216.png','-','2018-09-21 17:56:56','2018-09-21 17:56:56',NULL),
	(5,'1','Scheneider Electric','Scheneider-Electric_1537556244.png','-','2018-09-21 17:57:24','2018-09-21 17:57:24',NULL),
	(6,'1','Dasan','Dasan_1537556269.png','-','2018-09-21 17:57:49','2018-09-21 17:57:49',NULL),
	(7,'1','Bosch','bosch_1537556359.png','-','2018-09-21 17:59:19','2018-09-21 17:59:19',NULL),
	(8,'1','Pelco','pelco_1537556384.png','-','2018-09-21 17:59:44','2018-09-21 17:59:44',NULL),
	(9,'1','Felica','felica_1537556401.png','-','2018-09-21 18:00:01','2018-09-21 18:00:01',NULL),
	(10,'1','Woorizen','woorizen_1537556439.png','-','2018-09-21 18:00:39','2018-09-21 18:00:39',NULL),
	(11,'1','TOA','toa_1537556455.png','-','2018-09-21 18:00:55','2018-09-21 18:00:55',NULL),
	(12,'1','Azbil Corporation','azbil_1537556480.png','-','2018-09-21 18:01:20','2018-09-21 18:01:20',NULL),
	(13,'1','LS','ls_1537556509.png','LS','2018-09-21 18:01:49','2018-09-21 18:01:49',NULL),
	(14,'2','ti-phone','ti-phone_1537556595.png','-','2018-09-21 18:03:15','2018-09-21 18:03:15',NULL),
	(15,'2','vmware Airwatch','airwatch-1_1537556633.png','-','2018-09-21 18:03:53','2018-09-21 18:03:53',NULL),
	(16,'2,3','Samsung','samsung_1537556669.png','-','2018-09-21 18:04:29','2018-09-21 18:04:29',NULL),
	(17,'2','Apple Computers','apple_1537556802.png','-','2018-09-21 18:06:42','2018-09-21 18:06:42',NULL),
	(18,'2,3','ZTE','zte_1537556826.png','-','2018-09-21 18:07:06','2018-09-21 18:07:06',NULL),
	(19,'3','Riverbed','riverbed_1537556929.png','-','2018-09-21 18:08:49','2018-09-21 18:08:49',NULL),
	(20,'3','Paloalto Networks','paloalto_1537556959.png','-','2018-09-21 18:09:19','2018-09-21 18:09:19',NULL),
	(21,'3','T-Systems','t-system_1537556977.png','-','2018-09-21 18:09:37','2018-09-21 18:09:37',NULL),
	(22,'3','Thales','thales_1537556997.png','-','2018-09-21 18:09:57','2018-09-21 18:09:57',NULL),
	(23,'3','Hewlett-Packard Company','hp_1537557034.png','-','2018-09-21 18:10:34','2018-09-21 18:10:34',NULL),
	(24,'3','Dell','dell_1537557054.png','-','2018-09-21 18:10:54','2018-09-21 18:10:54',NULL),
	(25,'3','IBM','ibm_1537557072.png','-','2018-09-21 18:11:12','2018-09-21 18:11:12',NULL),
	(26,'3','TP-LINK','tp-link_1537557102.png','-','2018-09-21 18:11:42','2018-09-21 18:11:42',NULL),
	(27,'3','Symantec','symantec_1537557123.png','-','2018-09-21 18:12:03','2018-09-21 18:12:03',NULL);

/*!40000 ALTER TABLE `partners` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `overview_id` int(10) unsigned DEFAULT NULL,
  `why_us_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `cased_id` int(10) unsigned DEFAULT NULL,
  `excerpt` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'false',
  PRIMARY KEY (`id`),
  KEY `products_overview_id_foreign` (`overview_id`),
  KEY `products_why_us_id_foreign` (`why_us_id`),
  KEY `products_product_id_foreign` (`product_id`),
  KEY `products_cased_id_foreign` (`cased_id`),
  CONSTRAINT `products_cased_id_foreign` FOREIGN KEY (`cased_id`) REFERENCES `contents` (`id`),
  CONSTRAINT `products_overview_id_foreign` FOREIGN KEY (`overview_id`) REFERENCES `contents` (`id`),
  CONSTRAINT `products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `contents` (`id`),
  CONSTRAINT `products_why_us_id_foreign` FOREIGN KEY (`why_us_id`) REFERENCES `contents` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`, `overview_id`, `why_us_id`, `product_id`, `cased_id`, `excerpt`, `image`, `featured`)
VALUES
	(1,'IoT Services','2018-09-20 12:30:44','2018-09-20 12:30:44',NULL,1,NULL,NULL,NULL,'Testing','iot_1537446644.jpg','true'),
	(2,'Mobility Services','2018-09-20 12:31:22','2018-09-20 12:31:22',NULL,2,NULL,NULL,NULL,'Testing 2','mobility_1537446682.jpg','true'),
	(3,'CPE Manage Services','2018-09-20 12:31:50','2018-09-20 12:31:50',NULL,3,NULL,NULL,NULL,'Testing 3','cpe_1537446710.jpg','true'),
	(4,'IoT Smart Building','2018-09-20 12:45:00','2018-09-20 12:45:00',NULL,6,7,8,9,'This is Excerpt','iot-smart-building-x_1537447500.jpg','false'),
	(5,'PRIME','2018-09-21 16:37:39','2018-09-21 16:37:39',NULL,13,14,15,16,'-','mobility-prime-x_1537551459.jpg','false'),
	(6,'Seat Management','2018-09-21 17:17:55','2018-09-21 17:22:41',NULL,17,18,19,20,'-','cpe-seat-management-x_1537553875.jpg','false'),
	(7,'Government Industries','2018-10-04 12:26:36','2018-10-04 12:26:36',NULL,21,NULL,NULL,NULL,'This is an example','gov_1538655996.jpg','true'),
	(8,'Healthcare Industries','2018-10-04 12:27:14','2018-10-04 12:27:14',NULL,22,NULL,NULL,NULL,'This is a subtitle','healthcare_1538656034.jpg','true'),
	(9,'IoT Gateway','2018-10-04 12:38:22','2018-10-04 12:38:22',NULL,23,24,26,25,'-','iot-iot-gateway-x_1538656702.jpg','false'),
	(10,'IT Infrastructure Management','2018-10-04 12:39:32','2018-10-04 12:39:32',NULL,27,30,29,28,'-','iot-it-infra-x_1538656772.jpg','false'),
	(11,'Smart Lighting','2018-10-04 12:40:19','2018-10-04 12:40:19',NULL,31,34,33,32,'-','iot-smart-lighting-x_1538656819.jpg','false'),
	(12,'Smart Water','2018-10-04 12:41:14','2018-10-04 12:41:14',NULL,35,38,37,36,'-','iot-smart-water-x_1538656874.jpg','false'),
	(13,'Smart Living','2018-10-04 16:50:27','2018-10-04 16:53:41',NULL,39,41,42,40,'-','iot-smart-living-x_1538672021.jpg','false'),
	(14,'Handsets','2018-10-04 16:51:36','2018-10-04 16:53:19',NULL,43,45,44,46,'-','mobility-handset-x_1538671999.jpg','false'),
	(15,'ICT Security Services','2018-10-04 16:52:31','2018-10-04 16:53:07',NULL,47,48,49,50,'-','cpe-ict-security-system-x_1538671987.jpg','false'),
	(16,'IT Hardware','2018-10-04 16:55:34','2018-10-04 16:55:34',NULL,51,53,54,52,'-','cpe-it-hardware-x_1538672134.jpg','false'),
	(17,'Managed CPE Services','2018-10-04 16:56:17','2018-10-04 16:56:17',NULL,55,57,58,56,'-','cpe-managed-cpe-services-x_1538672177.jpg','false'),
	(18,'Education Industries','2018-10-05 07:07:21','2018-10-05 07:07:21',NULL,59,NULL,NULL,NULL,'-','edu_1538723241.png','true'),
	(19,'Finance Industries','2018-10-05 07:08:17','2018-10-05 07:08:17',NULL,60,NULL,NULL,NULL,'-','finance_1538723297.png','true'),
	(20,'Banking Industries','2018-10-05 07:08:35','2018-10-05 07:08:35',NULL,61,NULL,NULL,NULL,'-','banking_1538723315.png','true'),
	(21,'Retail Industries','2018-10-05 07:11:28','2018-10-05 07:11:28',NULL,62,NULL,NULL,NULL,'-','retail_1538723488.png','true'),
	(22,'Transport & Logistic Industries','2018-10-05 07:12:24','2018-10-05 07:12:24',NULL,63,NULL,NULL,NULL,'-','transport_1538723544.png','true'),
	(23,'Media Industries','2018-10-05 07:12:57','2018-10-05 07:12:57',NULL,64,NULL,NULL,NULL,'-','media_1538723577.png','true');

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table socialmedia
# ------------------------------------------------------------

DROP TABLE IF EXISTS `socialmedia`;

CREATE TABLE `socialmedia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `socialmedia` WRITE;
/*!40000 ALTER TABLE `socialmedia` DISABLE KEYS */;

INSERT INTO `socialmedia` (`id`, `name`, `link`, `description`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'facebook','https://web.facebook.com/pinstheiotcompany',NULL,'2018-09-20 11:58:37','2018-09-20 11:58:37',NULL),
	(2,'twitter','https://twitter.com/pinsindonesia',NULL,'2018-09-20 11:58:37','2018-09-20 11:58:37',NULL);

/*!40000 ALTER TABLE `socialmedia` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `group_name` int(11) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;

INSERT INTO `teams` (`id`, `name`, `title`, `photo`, `created_at`, `updated_at`, `deleted_at`, `group_name`, `details`)
VALUES
	(1,'Otong Iip','President Commissioner','otong_1537555440.jpg','2018-09-21 17:44:00','2018-09-21 17:44:00',NULL,1,'Otong Iip was born in Kuningan , 1965. Appointed as President Commissioner of PT PINS Indonesia since November , 2016. Currently, he serve as President Director of TelkomMetra. Various position has previously served as Senior Manager Finance Regional Division II (2006-2007), Deputy Senior General Manager Finance Centre (2007-2009), Finance Director of PT Telekomunikasi Indonesia International (TELIN) (2009-2010), Senior General Manager of Finance-Billing & Collection Centre Telkom (2010-2013), and President Director of PT Finnet Indonesia (2013-2015). Otong Iip completed his graduate studies at the University of Borobudur with Master’s Degree Management in 2002. He also follows a number of active training, including Directorship Development at NUS Singapore and International Leadership INSEAD Batch-1 in Jakarta-France-Abu Dhabi (2016). He has received awards of Satya Lancana from the President of the Republic of Indonesia (2013).'),
	(2,'Mohammad Firdaus','Direktur Utama','mohammad_1537555547.jpg','2018-09-21 17:45:47','2018-09-21 17:45:47',NULL,2,'Mohammad Firdaus was born in Ujung Pandang 1967. Appointed as President Director at PT. PINS Indonesia on 22 February 2017. He served several positions in other company, among others, Deputy Executive General Manager  Telkom West Division (2013-2013), and Deputy Executive General Manager Home Service Consumer Service Division (2013-2015 ) and as Executive General Manager of Telkom Regional 7 (2015-2017).\n\nHe graduated from Hasanuddin University of Makassar with Electrical Engineering degree, and Master Marketing Management from University of Muhammadiyah, Malang. He was selected as Top Band Position 1 from PT Telkom (2015). In addition, Mr Firdaus also achieved The Most Inspiring Role Model Division / Center from PT Telkom in 2016. He also won the highest award from the President of Republic of Indonesia, Satyalencana Pembangunan in 2016.'),
	(3,'Adbi Mulyanta Ginting','Commissioner','abdi_1537555635.jpg','2018-09-21 17:47:15','2018-09-21 17:47:15',NULL,1,'Abdi Mulyanta Ginting was born in Tanjung Pinang, 1966.  He graduated from Electro Engineering of Bandung Technology Institute (ITB) in 1991 as well as gained title Master of Science in Telecommunication(MST) from University of Pittsburgh, USA, in 1997. He gained various successes in PT Telkom inter alia implementation of TelkomNet in 1995 as the project Leader, developed Paperless Office system called POINT in 1998 and in 1999 – 2000 he was successful in the implementation of Y2K TELKOM project in Information System field.\n\nIn 2005, as the GM of Information System Unit Regional V East Java, he was responsible for the successful implementation of Call Data Collection (CDC) Divre 5 East Java and since 2007 he was as the Deputy Senior General Manager IS Center, PT TELKOM and in 2009, he was responsible for the successful implementation of I-Siska as the sustitute of the existing system. Since March 22 2018, he was entrusted to have position as Board of Commisssioner of PT. PINS Indonesia.'),
	(4,'Aulia E Marinto','Commissioner','aulia_1537555683.jpeg','2018-09-21 17:48:03','2018-09-21 17:48:46',NULL,1,'Abdi Mulyanta Ginting was born in Tanjung Pinang, 1966.  He graduated from Electro Engineering of Bandung Technology Institute (ITB) in 1991 as well as gained title Master of Science in Telecommunication(MST) from University of Pittsburgh, USA, in 1997. He gained various successes in PT Telkom inter alia implementation of TelkomNet in 1995 as the project Leader, developed Paperless Office system called POINT in 1998 and in 1999 – 2000 he was successful in the implementation of Y2K TELKOM project in Information System field.\n\nIn 2005, as the GM of Information System Unit Regional V East Java, he was responsible for the successful implementation of Call Data Collection (CDC) Divre 5 East Java and since 2007 he was as the Deputy Senior General Manager IS Center, PT TELKOM and in 2009, he was responsible for the successful implementation of I-Siska as the sustitute of the existing system. Since March 22 2018, he was entrusted to have position as Board of Commisssioner of PT. PINS Indonesia.'),
	(5,'Irnanda Laksana','Commissioner','irnanda_1537555849.jpg','2018-09-21 17:50:49','2018-09-21 17:50:49',NULL,1,'Irnanda Laksanawan was born in Surabaya on 1962. He was appointed as the Board of Commissioners of PT.PINS Indonesia on December 1, 2017. Now he is as National Research Council of Indonesia since 2012. He ever occupied several posititions such as Board of Commissioners of PT Pertamina (Persero), Deputy of Business Sector Strategic Industries & Manufacture, State Owned Enterprise of Indonesia (2010 – 2012), and as Expert of Ministry of State Owned Enterprise Business Sector Human Resources and Technology (2012 – 2013).\n\nIrnanda Laksanawan gained Bachelor of Mechanical Engineernig from ITS Surabaya on 1986, and he gained the title Master of Engineering Science from Business School University of Birmingham, United Kingdom on 1990, and he gained the title Doctor (Phd) of Business Management from University of South Australia 2007. Iranda was also active in taking part in organizaton such as Chairman of England alumnus in Indonesia (2005 – 2013), Chairman of Alumnus ITS (IKA ITS) (2011-2015), adn still active as Board of Indonesia Technology Audit (2012 – Now).'),
	(6,'Benny Artono','Sales Director','benny_1537555895.jpg','2018-09-21 17:51:35','2018-09-21 17:51:35',NULL,2,'Benny Artono born in wonosobo on 1966. Served as Director Distribution Business since april 8 2013 , then Director ICT Solution PINS in march 2015, and now served as Sales Director PINS Indonesia. Previously, he served as head of regional Telecommunication Medan ( 2006 ) , Deputy Executive Division General Manager for Regional VII Telkom in 2008, Deputy EGM Division Consumer Service east II  in 2010 , Deputy EGM Division Consumer Service east I  ( 2011-2012 ), and as Deputy EGM Division Access Telkom ( 2012-2013 ). He was graduated for s2 on business administration at the university of bradford UK in 2001'),
	(7,'Imam Santoso','Operation Director','imam_1537555929.jpg','2018-09-21 17:52:09','2018-09-21 17:52:09',NULL,2,'Imam Santoso was born in Banyuwangi on 1966. He was appointed as the Operation Director of PT.PINS Indonesia on March 22, 2018. He ever occupied several posititions inter alia the President Director of PT. Sigma Metrasys Solution (2011-2013), Director for SI Development and Business in PT. Sigma Cipta Caraka (2013-2015), and as the Executive General Manager of Service and Solution Division of PT. Telkom from (2015 -2018).\n\nImam Santoso was graduated from Hasanudin University Makassar with title Graduate in Telecommunication Engineering, and he gained the title Master of Science in Communication Technology, Business & Policy from Stratchlyde University, UK.'),
	(8,'Notje Rosanti','Finance & Business Support Director','notje_1537555973.jpg','2018-09-21 17:52:53','2018-09-21 17:52:53',NULL,2,'Notje Rosanti was born in Bandung on 1969. She was appointed as the Director of Finance and Business Support of PT.PINS Indonesia on March 22, 2018. She ever occupied several posititions inter alia as the Assistant to the Vice-President of Subsidiary Performance of PT. Telkom (2015-2018), and as the Assistant to the Vice-President of Cash and Risk Management of PT. Telkom (2013-2015).\nThe graduation of Padjajaran Univeristy with title Bachelor of Economics, and Master of Management from STMB Telkom, was participate in several training such as, SS IoT (Internet of Things) (2017), and Financial Strategies for Value Creation on 2017.');

/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'Admin','admin@pins.co.id','$2y$10$Udnv067Ab1B.rYr9i3qNAO/0Y7C6cWfkB2IM7tts9Yz1rp1FXk35.','TMbZd8uTE6RgQIoagiXVyEzeqz2boVqgOTN1JL0aMfN4nNBoxrVNEAI5KNNi','2018-08-25 06:00:05','2018-08-25 06:00:05',NULL),
	(2,'SuperAdmin','superadmin@gmail.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','6tspOwjxTvoiUMgVsbpQqBDPMV3EHgMS4VhnjCo3VH0vXedIR6DbwmGlowRy','2018-01-01 00:00:00',NULL,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
