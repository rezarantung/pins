<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('sliders', 'Api\FrontendController@showSliders')->middleware('api');
Route::get('menu', 'Api\FrontendController@showMenu')->middleware('api');
Route::get('menu/{id}', 'Api\FrontendController@showMenuDetails')->middleware('api');
Route::get('footers', 'Api\FrontendController@showFooter')->middleware('api');
Route::get('products', 'Api\FrontendController@showProduct')->middleware('api');
Route::get('products/{id}', 'Api\FrontendController@showProductDetails')->middleware('api');
Route::get('news/{query}', 'Api\FrontendController@showNews')->middleware('api');
Route::get('comments/{id}', 'Api\FrontendController@showComments')->middleware('api');
Route::get('area', 'Api\FrontendController@showArea')->middleware('api');
Route::post('comments', 'Api\FrontendController@addComment')->middleware('api');
Route::post('contacts', 'Api\FrontendController@addEnquiry')->middleware('api');
Route::post('analytics', 'Api\FrontendController@addAnalytics')->middleware('api');
