<?php

Route::group(['prefix' => 'admin'], function () {
	Auth::routes();

	//dasboard
	Route::get('/', 'DashboardController@index');
	Route::get('/dashboard/visitor', 'DashboardController@visitor');
	Route::get('/dashboard/article', 'DashboardController@article');
	Route::get('/dashboard/article_list', 'DashboardController@article_list');

	//user
	Route::get('/user', 'UserController@index');
	Route::get('/user/list', 'UserController@list');
	Route::post('/user/store', 'UserController@store');
	Route::delete('/user/destroy', 'UserController@destroy');
	Route::get('/user/detail', 'UserController@detail');
	Route::put('/user/update', 'UserController@update');
	Route::put('/user/change_password', 'UserController@change_password');

	//enquiry
	Route::get('/enquiry', 'EnquiryController@index');
	Route::get('/enquiry/list', 'EnquiryController@list');
	Route::post('/enquiry/store', 'EnquiryController@store');
	Route::delete('/enquiry/destroy', 'EnquiryController@destroy');
	Route::get('/enquiry/detail', 'EnquiryController@detail');
	Route::put('/enquiry/update', 'EnquiryController@update');

	//team
	Route::get('/team', 'TeamController@index');
	Route::get('/team/list', 'TeamController@list');
	Route::post('/team/store', 'TeamController@store');
	Route::delete('/team/destroy', 'TeamController@destroy');
	Route::get('/team/detail', 'TeamController@detail');
	Route::post('/team/update', 'TeamController@update');

	//area
	Route::get('/area', 'AreaController@index');
	Route::get('/area/list', 'AreaController@list');
	Route::post('/area/store', 'AreaController@store');
	Route::delete('/area/destroy', 'AreaController@destroy');
	Route::get('/area/detail', 'AreaController@detail');
	Route::put('/area/update', 'AreaController@update');

	//product
	Route::get('/product', 'ProductController@index');
	Route::get('/product/list', 'ProductController@list');
	Route::get('/product/content_list', 'ProductController@content_list');
	Route::post('/product/store', 'ProductController@store');
	Route::delete('/product/destroy', 'ProductController@destroy');
	Route::get('/product/detail', 'ProductController@detail');
	Route::post('/product/update', 'ProductController@update');

	//socialmedia
	Route::get('/socialmedia', 'SocialmediaController@index');
	Route::get('/socialmedia/list', 'SocialmediaController@list');
	Route::post('/socialmedia/store', 'SocialmediaController@store');
	Route::delete('/socialmedia/destroy', 'SocialmediaController@destroy');
	Route::get('/socialmedia/detail', 'SocialmediaController@detail');
	Route::post('/socialmedia/update', 'SocialmediaController@update');

	//company
	Route::get('/company', 'CompanyController@index');
	Route::get('/company/list', 'CompanyController@list');
	Route::post('/company/store', 'CompanyController@store');
	Route::delete('/company/destroy', 'CompanyController@destroy');
	Route::get('/company/detail', 'CompanyController@detail');
	Route::post('/company/update', 'CompanyController@update');

	//content
	Route::get('/content', 'ContentController@index');
	Route::get('/content/list', 'ContentController@list');
	Route::post('/content/store', 'ContentController@store');
	Route::delete('/content/destroy', 'ContentController@destroy');
	Route::get('/content/detail', 'ContentController@detail');
	Route::put('/content/update', 'ContentController@update');

	//comment
	Route::get('/comment', 'CommentController@index');
	Route::get('/comment/list', 'CommentController@list');
	Route::post('/comment/store', 'CommentController@store');
	Route::delete('/comment/destroy', 'CommentController@destroy');
	Route::get('/comment/detail', 'CommentController@detail');
	Route::put('/comment/update', 'CommentController@update');

	//partner
	Route::get('/partner', 'PartnerController@index');
	Route::get('/partner/list', 'PartnerController@list');
	Route::get('/partner/product_list', 'PartnerController@product_list');
	Route::post('/partner/store', 'PartnerController@store');
	Route::delete('/partner/destroy', 'PartnerController@destroy');
	Route::get('/partner/detail', 'PartnerController@detail');
	Route::post('/partner/update', 'PartnerController@update');

	//article
	Route::get('/article', 'ArticleController@index');
	Route::get('/article/list', 'ArticleController@list');
	Route::post('/article/store', 'ArticleController@store');
	Route::delete('/article/destroy', 'ArticleController@destroy');
	Route::get('/article/detail', 'ArticleController@detail');
	Route::post('/article/update', 'ArticleController@update');

	//content
	Route::get('/content', 'ContentController@index');
	Route::get('/content/list', 'ContentController@list');
	Route::post('/content/store', 'ContentController@store');
	Route::delete('/content/destroy', 'ContentController@destroy');
	Route::get('/content/detail', 'ContentController@detail');
	Route::post('/content/update', 'ContentController@update');

	//menu
	Route::get('/menu', 'MenuController@index');
	Route::get('/menu/list', 'MenuController@list');
	Route::get('/menu/parent_list', 'MenuController@parent_list');
	Route::get('/menu/content_list', 'MenuController@content_list');
	Route::post('/menu/store', 'MenuController@store');
	Route::delete('/menu/destroy', 'MenuController@destroy');
	Route::get('/menu/detail', 'MenuController@detail');
	Route::post('/menu/update', 'MenuController@update');

	//email template
	Route::get('/emailtemplate', 'EmailtemplateController@index');
	Route::get('/emailtemplate/list', 'EmailtemplateController@list');
	Route::get('/emailtemplate/detail', 'EmailtemplateController@detail');
	Route::post('/emailtemplate/update', 'EmailtemplateController@update');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/n/{id}', 'NewsController@show');
